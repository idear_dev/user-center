/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.console.inteceptor;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.StringUtil;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.response.Response;
import com.idear.user.center.console.bo.ManagerInfo;
import com.idear.user.center.console.business.ManagerBusiness;
import com.idear.user.center.console.context.AuthContext;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.core.po.Manager;
import com.idear.user.center.service.ManagerService;
import com.idear.user.center.service.facade.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Set;

/**
 * 验证Token有效性拦截器
 * @author CcShan
 * @date 2018/1/29
 */
@Component("validateTokenInterceptor")
public class ValidateTokenInterceptor implements HandlerInterceptor{

    @Autowired
    private ManagerBusiness managerBusiness;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        String token = httpServletRequest.getHeader(SystemHeader.TOKEN);
        if (StringUtil.isNotEmpty(token)){
            ManagerInfo managerInfo = managerBusiness.checkToken(token);
            if (managerInfo == null){
                throw new BusinessException(ConsoleMessage.TOKEN_IS_ILLEGAL);
            }
            String urlPattern = httpServletRequest.getServletPath();
            validatePermission(urlPattern,managerInfo);
            AuthContext.setManagerInfo(managerInfo);
        }else {
            throw new BusinessException(ConsoleMessage.TOKEN_IS_EMPTY);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }

    /**
     * 验证资源访问权限
     *
     * @param urlPattern
     * @param managerInfo
     */
    private void validatePermission(String urlPattern, ManagerInfo managerInfo) {
        Set<String> resourceSet = managerInfo.getResourceSet();
        if (resourceSet == null || resourceSet.isEmpty()) {
            throw new BusinessException(ConsoleMessage.PERMISSION_DENIED);
        } else if (!resourceSet.contains(urlPattern)) {
            throw new BusinessException(ConsoleMessage.PERMISSION_INSUFFICIENCY);
        }
    }
}
