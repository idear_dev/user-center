/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * @author ShanCc
 * @date 2018/10/12
 */
public class AddBannerForm {

    /**
     * 位置信息
     */
    @NotNull(message = "location_not_null")
    private Integer location;

    /**
     * 跳转地址
     */
    private String linkUrl;

    /**
     * 图片地址
     */
    @NotEmpty(message = "image_url_not_empty")
    private String imageUrl;

    /**
     * 标题信息
     */
    @NotEmpty(message = "title_not_empty")
    private String title;

    /**
     * 排序位置
     */
    @NotNull(message = "sort_not_null")
    private Integer sort;

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
