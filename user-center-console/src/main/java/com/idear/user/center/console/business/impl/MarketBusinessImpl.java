/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.MarketBusiness;
import com.idear.user.center.console.form.AddMarketForm;
import com.idear.user.center.console.form.UpdateMarketForm;
import com.idear.user.center.service.dto.MarketDto;
import com.idear.user.center.service.facade.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/10/24
 */
@Component
public class MarketBusinessImpl implements MarketBusiness {

    @Autowired
    private MarketService marketService;

    /**
     * 获取所有应用市场信息
     *
     * @return
     */
    @Override
    public Response getMarketList() {
        Response<List<MarketDto>> response = marketService.listMarket();
        return response;
    }

    /**
     * 添加应用市场信息
     *
     * @param form
     * @return
     */
    @Override
    public Response add(AddMarketForm form) {
        MarketDto marketDto = new MarketDto();
        marketDto.setName(form.getName());
        marketDto.setLogoUrl(form.getLogoUrl());
        marketDto.setPackageName(form.getPackageName());
        Response<Integer> response = marketService.addMarket(marketDto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新应用市场信息
     *
     * @param form
     * @return
     */
    @Override
    public Response update(UpdateMarketForm form) {
        MarketDto marketDto = new MarketDto();
        marketDto.setId(form.getId());
        marketDto.setName(form.getName());
        marketDto.setLogoUrl(form.getLogoUrl());
        marketDto.setPackageName(form.getPackageName());
        Response response = marketService.updateMarket(marketDto);
        return ResponseUtil.getSimpleResponse(response);
    }
}
