package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class EditManagerForm {

    @NotNull(message = "id_not_null")
    private Integer id;

    @NotEmpty(message = "role_ids_not_empty")
    private String roleIds;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }
}
