package com.idear.user.center.console.vo;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class PageResultBaseVO {

    private Integer totalNum;

    private Integer totalPage;

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
