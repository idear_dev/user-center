package com.idear.user.center.console.vo;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class RoleAllListVO {

    private List<Map<String, Object>> roleList;

    public List<Map<String, Object>> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Map<String, Object>> roleList) {
        this.roleList = roleList;
    }
}
