package com.idear.user.center.console.business.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.Md5Util;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.user.center.console.bo.ManagerInfo;
import com.idear.user.center.console.business.ManagerBusiness;
import com.idear.user.center.console.component.MenuComponent;
import com.idear.user.center.console.constant.CacheConstant;
import com.idear.user.center.console.context.AuthContext;
import com.idear.user.center.console.converter.ManagerConverter;
import com.idear.user.center.console.form.GetManagerListForm;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.console.vo.GetManagerInfoVO;
import com.idear.user.center.core.enums.DelFlag;
import com.idear.user.center.core.enums.ManagerStatus;
import com.idear.user.center.core.po.Manager;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.core.po.Resource;
import com.idear.user.center.core.po.Role;
import com.idear.user.center.service.ManagerService;
import com.idear.user.center.service.MenuService;
import com.idear.user.center.service.ResourceService;
import com.idear.user.center.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 *
 * @author CcShan
 * @date 2017/9/22
 */
@Component
public class ManagerBusinessImpl implements ManagerBusiness {

    @Autowired
    private ManagerService managerService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ManagerConverter managerConverter;

    @Autowired
    private MenuComponent menuComponent;

    @Override
    public ManagerInfo checkToken(String token) {
        String value = redisTemplate.opsForValue().get(CacheConstant.MANAGER_TOKEN_INDEX+token);
        if (StringUtil.isEmpty(value)){
            return null;
        }
        return JsonUtil.parseObject(value,ManagerInfo.class);
    }

    @Override
    public Response login(String email, String password) {
        Manager manager = managerService.getManagerByEmail(email);
        if (manager == null || !manager.getPassword().equals(Md5Util.md5(password))){
            throw new BusinessException(ConsoleMessage.PASSWORD_ERROR);
        }
        ManagerInfo managerInfo = initManagerInfo(manager);
        String token = Md5Util.md5(manager.getEmail()+System.currentTimeMillis());
        redisTemplate.opsForValue().set(CacheConstant.MANAGER_TOKEN_INDEX+token, JsonUtil.toJsonString(managerInfo));
        Map<String,Object> resultMap = new HashMap<>(2);
        resultMap.put("token",token);
        return new Response(resultMap);
    }

    /**
     * token信息
     *
     * @param token
     * @return
     */
    @Override
    public Response logout(String token) {
        redisTemplate.delete(CacheConstant.MANAGER_TOKEN_INDEX+token);
        return Response.SUCCESS;
    }

    /**
     * 初始化管理员信息
     *
     * @param manager
     * @return
     */
    private ManagerInfo initManagerInfo(Manager manager) {
        ManagerInfo managerInfo = new ManagerInfo();
        String roleIds = manager.getRoleIds();
        managerInfo.setManager(manager);
        List<Role> roleList = getRoleList(roleIds);
        managerInfo.setRoleList(roleList);
        List<Menu> menuList = getMenuList(roleList);
        managerInfo.setMenuList(menuList);
        List<Resource> resourceList = getResourceList(menuList);
        managerInfo.setResourceList(resourceList);
        managerInfo.setResourceSet(getResourceSet(resourceList));
        return managerInfo;
    }

    /**
     * 根据角色id获取角色列表
     *
     * @param roleIds
     * @return
     */
    private List<Role> getRoleList(String roleIds) {
        if (StringUtil.isEmpty(roleIds)) {
            return null;
        } else {
            String[] roleIdArray = roleIds.split(",");
            List<Integer> roleIdList = new ArrayList<>();
            for (String roleId : roleIdArray) {
                roleIdList.add(Integer.valueOf(roleId));
            }
            return roleService.getRoleListById(roleIdList);
        }
    }

    /**
     * 根据角色列表获取用户可操作的菜单列表
     *
     * @param roleList
     * @return
     */
    private List<Menu> getMenuList(List<Role> roleList) {
        if (roleList == null || roleList.isEmpty()) {
            return null;
        }
        List<Integer> menuIdList = new ArrayList<>();
        for (Role role : roleList) {
            String menuIds = role.getMenuIds();
            if (StringUtil.isNotEmpty(menuIds)) {
                String[] menuIdArray = menuIds.split(",");
                for (String menuId : menuIdArray) {
                    menuIdList.add(Integer.valueOf(menuId));
                }
            }
        }
        if (menuIdList.isEmpty()) {
            return null;
        } else {
            return menuService.getMenuListById(menuIdList);
        }
    }

    /**
     * 根据菜单列表获取用户可用的资源列表
     *
     * @param menuList
     * @return
     */
    private List<Resource> getResourceList(List<Menu> menuList) {
        if (menuList == null || menuList.isEmpty()) {
            return null;
        }
        Iterator<Menu> iterator = menuList.iterator();
        List<Integer> resourceIdList = new ArrayList<>();
        while (iterator.hasNext()) {
            Menu menu = iterator.next();
            String resourceIds = menu.getResourceIds();
            if (StringUtil.isNotEmpty(resourceIds)) {
                String[] resourceIdArray = resourceIds.split(",");
                for (String resourceId : resourceIdArray) {
                    resourceIdList.add(Integer.valueOf(resourceId));
                }
            }
        }
        if (resourceIdList.isEmpty()) {
            return null;
        } else {
            return resourceService.getResourceListById(resourceIdList);
        }
    }

    /**
     * 获取资源集合
     *
     * @param resourceList
     * @return
     */
    private Set<String> getResourceSet(List<Resource> resourceList) {
        if (resourceList == null || resourceList.isEmpty()) {
            return null;
        }
        Set<String> resourceSet = new HashSet<>();
        for (Resource resource : resourceList) {
            resourceSet.add(resource.getUrlPattern());
        }
        return resourceSet;
    }

    /**
     * 获取管理员信息
     *
     * @return
     */
    @Override
    public Response getManagerInfo() {
        Manager manager = AuthContext.getManager();
        List<Menu> menuList = AuthContext.getMenuList();
        GetManagerInfoVO getManagerInfoVO = new GetManagerInfoVO();
        getManagerInfoVO.setName(manager.getName());
        getManagerInfoVO.setEmail(manager.getEmail());
        getManagerInfoVO.setMenu(menuComponent.getMenuInfo(menuList));
        return new Response(getManagerInfoVO);
    }

    /**
     * 创建管理员
     *
     * @param email
     * @param roleIds
     * @return
     */
    @Override
    public Response createManager(String email, String roleIds) {
        Manager manager = managerService.getAllManagerByEmail(email);
        if (manager == null) {
            manager = new Manager();
            Date date = new Date();
            manager.setEmail(email);
            manager.setStatus(ManagerStatus.OPENED.getStatus());
            manager.setRoleIds(roleIds);
            manager.setName("");
            manager.setCreateTime(date);
            manager.setUpdateTime(date);
            managerService.addManager(manager);
        } else if(DelFlag.DELETED.getCode().equals(manager.getDelFlag())) {
            Date date = new Date();
            manager.setStatus(ManagerStatus.OPENED.getStatus());
            manager.setId(manager.getId());
            manager.setDelFlag(DelFlag.NORMAL.getCode());
            manager.setRoleIds(roleIds);
            manager.setName("");
            manager.setCreateTime(date);
            manager.setUpdateTime(date);
            managerService.updateManager(manager);
        } else {
            throw new BusinessException(ConsoleMessage.MANAGER_IS_EXIST);
        }
        return Response.SUCCESS;
    }

    /**
     * 获取管理员列表
     *
     * @param getManagerListForm
     * @return
     */
    @Override
    public Response getManagerList(GetManagerListForm getManagerListForm) {
        String name = getManagerListForm.getName();
        Integer roleId = getManagerListForm.getRoleId();
        Integer pageNo = getManagerListForm.getPageNo();
        Integer pageSize = getManagerListForm.getPageSize();
        if (pageSize > 100) {
            pageSize = 100;
        }
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        PageList<Manager> result = managerService.getManagerList(name, roleId, pageBounds);
        List<Role> roleList = roleService.pageableRoleList(1, 10000);
        return new Response(managerConverter.convertToQueryManagerListVO(result, roleList));
    }

    /**
     * 编辑管理员
     *
     * @param id
     * @param roleIds
     * @return
     */
    @Override
    public Response editManager(Integer id, String roleIds) {
        Manager manager = new Manager();
        manager.setId(id);
        manager.setRoleIds(roleIds);
        manager.setUpdateTime(new Date());
        managerService.updateManager(manager);
        return Response.SUCCESS;
    }

    @Override
    public Response deleteManager(Integer id) {
        Manager manager = managerService.getManagerById(id);
        if(manager == null) {
            throw new BusinessException(ConsoleMessage.MANAGER_IS_NOT_EXIST);
        }
        manager.setId(id);
        manager.setDelFlag(DelFlag.DELETED.getCode());
        managerService.updateManager(manager);
        return Response.SUCCESS;
    }

    @Override
    public Response closeManager(Integer id) {
        Manager manager = managerService.getManagerById(id);
        if(manager == null) {
            throw new BusinessException(ConsoleMessage.MANAGER_IS_NOT_EXIST);
        }
        manager.setStatus(ManagerStatus.CLOSED.getStatus());
        manager.setUpdateTime(new Date());
        managerService.updateManager(manager);
        return Response.SUCCESS;
    }

    @Override
    public Response openManager(Integer id) {
        Manager manager = new Manager();
        manager.setId(id);
        manager.setStatus(ManagerStatus.OPENED.getStatus());
        manager.setUpdateTime(new Date());
        managerService.updateManager(manager);
        return Response.SUCCESS;
    }
}
