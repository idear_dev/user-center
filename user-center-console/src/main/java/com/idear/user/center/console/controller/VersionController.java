/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.VersionBusiness;
import com.idear.user.center.console.form.AddClientVersionForm;
import com.idear.user.center.console.form.DeleteClientVersionForm;
import com.idear.user.center.console.form.PageQueryBaseForm;
import com.idear.user.center.console.form.UpdateClientVersionForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author ShanCc
 * @date 2018/10/23
 */
@Controller
@RequestMapping(value = "version", method = RequestMethod.POST)
public class VersionController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Resource
    private VersionBusiness versionBusiness;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getClientVersionList(@RequestBody final PageQueryBaseForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer pageNo = form.getPageNo();
                Integer pageSize = form.getPageSize();
                return versionBusiness.getVersionList(pageNo,pageSize);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addClientVersion(@RequestBody final AddClientVersionForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return versionBusiness.add(form);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteClientVersion(@RequestBody final DeleteClientVersionForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer questionId = form.getId();
                return versionBusiness.delete(questionId);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateClientVersion(@RequestBody final UpdateClientVersionForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return versionBusiness.update(form);
            }
        }, bindingResult);
    }
}
