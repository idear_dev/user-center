package com.idear.user.center.console.vo;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class QueryManagerListVO {

    private List<Map<String, Object>> managerList;

    private Integer totalCount;

    private Integer totalPage;

    public List<Map<String, Object>> getManagerList() {
        return managerList;
    }

    public void setManagerList(List<Map<String, Object>> managerList) {
        this.managerList = managerList;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
