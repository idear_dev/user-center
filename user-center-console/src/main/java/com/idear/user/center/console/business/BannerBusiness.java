/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddBannerForm;
import com.idear.user.center.console.form.UpdateBannerForm;

/**
 * @author ShanCc
 * @date 2018/10/11
 */
public interface BannerBusiness {

    /**
     * 获取banner location 信息
     * @return
     */
    Response getLocationList();

    /**
     * 添加banner信息
     * @param form
     * @return
     */
    Response add(AddBannerForm form);

    /**
     * 删除广告位
     * @param bannerId
     * @return
     */
    Response delete(Integer bannerId);

    /**
     * 更新banner信息
     * @param form
     * @return
     */
    Response update(UpdateBannerForm form);

    /**
     * 查询所有Banner信息
     * @return
     */
    Response getBannerList();
}
