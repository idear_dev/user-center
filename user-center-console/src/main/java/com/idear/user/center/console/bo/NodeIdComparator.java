package com.idear.user.center.console.bo;

import java.util.Comparator;

/**
 * Created by CcShan on 2017/9/22.
 */
public class NodeIdComparator implements Comparator {

    /**
     * 按照节点编号比较
     */
    @Override
    public int compare(Object o1, Object o2) {
        int j1 = ((Node) o1).sort;
        int j2 = ((Node) o2).sort;
        return (j1 < j2 ? -1 : (j1 == j2 ? 0 : 1));
    }
}