package com.idear.user.center.console.business;


import com.idear.framework.response.Response;
import com.idear.user.center.console.form.CreateMenuForm;
import com.idear.user.center.console.form.EditMenuForm;
import com.idear.user.center.console.form.QueryMenuForm;

/**
 * @author cc
 * @date 2018-09-21
 */
public interface MenuBusiness {

    /**
     * 添加菜单
     *
     * @param form
     * @return
     */
    Response createMenu(CreateMenuForm form);

    /**
     * 编辑菜单
     *
     * @param form
     * @return
     */
    Response editMenu(EditMenuForm form);

    /**
     * 删除菜单
     * @param id
     * @return
     */
    Response deleteMenu(Integer id);

    /**
     * 查询菜单列表
     *
     * @param form
     * @return
     */
    Response queryMenuList(QueryMenuForm form);
}
