package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.MenuBusiness;
import com.idear.user.center.console.form.CreateMenuForm;
import com.idear.user.center.console.form.DeleteMenuForm;
import com.idear.user.center.console.form.EditMenuForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @author cc
 * @date 2019-09-21
 **/
@Controller
@RequestMapping(value = "menu", method = RequestMethod.POST)
public class MenuController {

    @Autowired
    private MenuBusiness menuBusiness;

    @Autowired
    private ControllerTemplate controllerTemplate;

    /**
     * 新建菜单
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "create")
    @ResponseBody
    public String createMenu(@Valid @RequestBody final CreateMenuForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return menuBusiness.createMenu(form);
            }
        },bindingResult);
    }

    /**
     * 编辑菜单信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "edit")
    @ResponseBody
    public String editRole(@Valid @RequestBody final EditMenuForm form,BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return menuBusiness.editMenu(form);
            }
        },bindingResult);
    }

    /**
     * 删除菜单信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(@Valid @RequestBody final DeleteMenuForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return menuBusiness.deleteMenu(form.getMenuId());
            }
        },bindingResult);
    }
}
