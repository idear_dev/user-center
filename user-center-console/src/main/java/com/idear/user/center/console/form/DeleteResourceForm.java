package com.idear.user.center.console.form;

import javax.validation.constraints.NotNull;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class DeleteResourceForm {

    @NotNull(message = "resource_id_not_null")
    private Integer resourceId;

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }
}
