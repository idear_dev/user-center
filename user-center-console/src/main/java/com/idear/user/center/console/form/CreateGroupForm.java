package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class CreateGroupForm {

    @NotEmpty(message = "group_name_not_null")
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
