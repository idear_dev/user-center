/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.message;

import com.idear.common.message.Message;

/**
 * @author ShanCc
 * @date 2018/9/21
 */
public enum  ConsoleMessage implements Message {

    /**
     * console错误码区间(2000~3000]
     */
    TOKEN_IS_EMPTY("2001","token信息为空"),
    TOKEN_IS_ILLEGAL("2002","请重新登录"),
    EMAIL_NOT_RIGHT("2003","邮箱地址不正确"),
    CHANNEL_NOT_EXIST("2204","渠道信息不存在"),
    VERSION_NOT_EXIT("2205","版本信息不存在"),
    UPLOAD_READ_FILE_FAILED("2206","上传文件失败"),
    UPLOAD_FILE_NOT_SUPPORT("2207","上传文件不支持"),
    MANAGER_IS_NOT_EXIST("2208","管理员信息不存在"),
    MANAGER_IS_EXIST("2209","会员信息不存在"),
    NO_PARENT_MENU_EXISTS("",""),
    NO_RESOURCE_EXISTS("",""),
    MENU_IS_EXISTS("",""),
    RESOURCE_IS_EXISTS("",""),
    ROLE_CONTAINS_GROUP("",""),
    ROLE_IS_EXIST("",""),
    GROUP_NAME_IS_EXIST("",""),
    PASSWORD_ERROR("2101","用户名或密码不正确"),
    PERMISSION_DENIED("2102","暂无权限访问"),
    PERMISSION_INSUFFICIENCY("2103","暂无权限访问"),
    BANNER_LOCATION_NOT_EXIST("2104","banner位置信息不存在");

    private String code;

    private String msg;

    ConsoleMessage(String code,String msg){
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getRespCode() {
        return this.code;
    }

    @Override
    public String getRespMsg() {
        return this.msg;
    }
}
