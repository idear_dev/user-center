/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.common.util.DateUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.WithdrawBusiness;
import com.idear.user.center.console.form.AuditWithdrawForm;
import com.idear.user.center.console.form.QueryWithdrawForm;
import com.idear.user.center.service.dto.WithdrawDetailListDto;
import com.idear.user.center.service.facade.WithdrawService;
import com.idear.user.center.service.param.WithdrawListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/6
 */
@Component
public class WithdrawBusinessImpl implements WithdrawBusiness {

    @Autowired
    private WithdrawService withdrawService;

    /**
     * 查询提现申请状态
     *
     * @return
     */
    @Override
    public Response getWithdrawStatus() {
        Response<List<Map<String,Object>>> response = withdrawService.listStatus();
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 查询任务记录列表
     *
     * @param form
     * @return
     */
    @Override
    public Response getWithdrawList(QueryWithdrawForm form) {
        WithdrawListParam param = new WithdrawListParam();
        param.setPageNo(form.getPageNo());
        param.setPageSize(form.getPageSize());
        param.setMobileNo(form.getMobileNo());
        param.setStatus(form.getStatus());
        if (StringUtil.isNotEmpty(form.getBeginTime())){
            param.setBeginTime(DateUtil.parseDate(form.getBeginTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            param.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        Response<WithdrawDetailListDto> response = withdrawService.getWithdrawDetailList(param);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 审核任务记录
     *
     * @param form
     * @return
     */
    @Override
    public Response auditWithdraw(AuditWithdrawForm form) {
        return null;
    }
}
