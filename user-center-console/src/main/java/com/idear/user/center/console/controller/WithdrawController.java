/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.WithdrawBusiness;
import com.idear.user.center.console.form.AuditTaskRecordForm;
import com.idear.user.center.console.form.AuditWithdrawForm;
import com.idear.user.center.console.form.QueryTaskRecordForm;
import com.idear.user.center.console.form.QueryWithdrawForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ShanCc
 * @date 2018/10/23
 */
@Controller
@RequestMapping(value = "withdraw", method = RequestMethod.POST)
public class WithdrawController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private WithdrawBusiness withdrawBusiness;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getWithdrawList(@RequestBody final QueryWithdrawForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return withdrawBusiness.getWithdrawList(form);
            }
        },bindingResult);
    }

    @RequestMapping(value = "/status", method = RequestMethod.POST)
    @ResponseBody
    public String getTaskRecordList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return withdrawBusiness.getWithdrawStatus();
            }
        });
    }

    @RequestMapping(value = "/audit", method = RequestMethod.POST)
    @ResponseBody
    public String auditTaskRecord(@RequestBody final AuditWithdrawForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return withdrawBusiness.auditWithdraw(form);
            }
        }, bindingResult);
    }
}
