package com.idear.user.center.console.form;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class EditMenuForm {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 父节点Id
     */
    private Integer pid;
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 资源指向
     */
    private String href;
    /**
     * 资源排序号
     */
    private Integer sort;
    /**
     * 资源ids
     */
    private String ResourceIds;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getResourceIds() {
        return ResourceIds;
    }

    public void setResourceIds(String resourceIds) {
        ResourceIds = resourceIds;
    }
}
