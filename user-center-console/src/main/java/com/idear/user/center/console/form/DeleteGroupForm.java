package com.idear.user.center.console.form;


import javax.validation.constraints.NotNull;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class DeleteGroupForm {

    @NotNull(message = "group_id_not_null")
    private Integer groupId;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}
