package com.idear.user.center.console.vo;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class PageResourceListVO extends PageResultBaseVO{

    private List<ResourceVO> resourceList;

    public List<ResourceVO> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<ResourceVO> resourceList) {
        this.resourceList = resourceList;
    }
}
