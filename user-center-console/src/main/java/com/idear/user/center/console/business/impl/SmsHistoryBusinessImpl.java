/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.common.util.DateUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.sms.service.dto.HistoryListDto;
import com.idear.sms.service.facade.SmsService;
import com.idear.sms.service.param.QueryHistoryParam;
import com.idear.user.center.console.business.SmsHistoryBusiness;
import com.idear.user.center.console.form.QuerySmsHistoryForm;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
@Component
public class SmsHistoryBusinessImpl implements SmsHistoryBusiness {

    @Resource
    private SmsService smsService;

    /**
     * 获取短信记录信息
     *
     * @param form
     * @return
     */
    @Override
    public Response getSmsHistoryList(QuerySmsHistoryForm form) {
        QueryHistoryParam param = new QueryHistoryParam();
        if (StringUtil.isNotEmpty(form.getBeginTime())){
            param.setBeginTime(DateUtil.parseDate(form.getBeginTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            param.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        param.setMobileNo(form.getMobileNo());
        param.setTemplateCode(form.getTemplateCode());
        param.setPageNo(form.getPageNo());
        param.setPageSize(form.getPageSize());
        Response<HistoryListDto> response = smsService.queryHistory(param);
        return ResponseUtil.getSimpleResponse(response);
    }
}
