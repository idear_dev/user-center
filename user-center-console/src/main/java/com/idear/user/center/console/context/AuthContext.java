package com.idear.user.center.console.context;


import com.idear.user.center.console.bo.ManagerInfo;
import com.idear.user.center.core.po.Manager;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.core.po.Role;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author CcShan
 * @date 2017/9/26
 */
public class AuthContext {

    private static final ThreadLocal<ManagerInfo> TL_MANAGER_INFO = new ThreadLocal();

    public static Manager getManager() {
        return TL_MANAGER_INFO.get().getManager();
    }

    public static ManagerInfo getManagerInfo() {
        return TL_MANAGER_INFO.get();
    }

    public static Set<Integer> getGroupSet() {
        List<Role> roleList = TL_MANAGER_INFO.get().getRoleList();
        Set<Integer> groupSet = new HashSet<>();
        for (Role role : roleList) {
            groupSet.add(role.getGroupId());
        }
        return groupSet;
    }

    public static List<Menu> getMenuList() {
        return TL_MANAGER_INFO.get().getMenuList();
    }

    public static void setManagerInfo(ManagerInfo managerInfo) {
        TL_MANAGER_INFO.set(managerInfo);
    }

    public static void clearThreadLocals() {
        TL_MANAGER_INFO.remove();
    }
}
