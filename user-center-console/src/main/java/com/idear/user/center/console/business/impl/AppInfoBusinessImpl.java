/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.AppInfoBusiness;
import com.idear.user.center.console.form.AddAppInfoForm;
import com.idear.user.center.console.form.QueryAppInfoForm;
import com.idear.user.center.console.form.UpdateAppInfoForm;
import com.idear.user.center.service.dto.AppInfoDto;
import com.idear.user.center.service.dto.AppInfoListDto;
import com.idear.user.center.service.facade.AppInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
@Component
public class AppInfoBusinessImpl implements AppInfoBusiness {

    @Autowired
    private AppInfoService appInfoService;

    /**
     * 获取所有应用信息
     *
     * @param form
     * @return
     */
    @Override
    public Response getAppInfoList(QueryAppInfoForm form) {
        AppInfoDto param = new AppInfoDto();
        param.setAppName(form.getAppName());
        Response<AppInfoListDto> response = appInfoService.listAppInfo(param,form.getPageNo(),form.getPageSize());
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 添加应用信息
     *
     * @param form
     * @return
     */
    @Override
    public Response add(AddAppInfoForm form) {
        AppInfoDto param = new AppInfoDto();
        param.setAppName(form.getAppName());
        param.setAppDescription(form.getAppDescription());
        param.setPhoneNumber(form.getPhoneNumber());
        param.setPackageName(form.getPackageName());
        param.setCompany(form.getCompany());
        param.setAppLogoUrl(form.getAppLogoUrl());
        Response<Integer> response = appInfoService.addAppInfo(param);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新应用信息
     *
     * @param form
     * @return
     */
    @Override
    public Response update(UpdateAppInfoForm form) {
        AppInfoDto param = new AppInfoDto();
        param.setId(form.getId());
        param.setAppName(form.getAppName());
        param.setAppDescription(form.getAppDescription());
        param.setPhoneNumber(form.getPhoneNumber());
        param.setPackageName(form.getPackageName());
        param.setCompany(form.getCompany());
        param.setAppLogoUrl(form.getAppLogoUrl());
        Response response = appInfoService.updateAppInfo(param);
        return ResponseUtil.getSimpleResponse(response);
    }
}
