/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AuditWithdrawForm;
import com.idear.user.center.console.form.QueryWithdrawForm;

/**
 * @author ShanCc
 * @date 2018/11/6
 */
public interface WithdrawBusiness {

    /**
     * 查询提现申请状态
     * @return
     */
    Response getWithdrawStatus();

    /**
     * 查询任务记录列表
     * @param form
     * @return
     */
    Response getWithdrawList(QueryWithdrawForm form);

    /**
     * 审核任务记录
     * @param form
     * @return
     */
    Response auditWithdraw(AuditWithdrawForm form);
}
