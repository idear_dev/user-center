/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.VersionBusiness;
import com.idear.user.center.console.form.AddClientVersionForm;
import com.idear.user.center.console.form.UpdateClientVersionForm;
import com.idear.user.center.service.dto.ClientVersionDto;
import com.idear.user.center.service.dto.ClientVersionListDto;
import com.idear.user.center.service.facade.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
@Component
public class VersionBusinessImpl implements VersionBusiness {

    @Autowired
    private ConfigService configService;

    /**
     * 分页获取版本列表
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response getVersionList(Integer pageNo, Integer pageSize) {
        Response<ClientVersionListDto> response = configService.listClientVersion(pageNo,pageSize);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 增加版本信息
     *
     * @param form
     * @return
     */
    @Override
    public Response add(AddClientVersionForm form) {
        ClientVersionDto dto = new ClientVersionDto();
        dto.setChannel(form.getChannel());
        dto.setDescription(form.getDescription());
        dto.setDownloadUrl(form.getDownloadUrl());
        dto.setForceUpdate(form.getForceUpdate());
        dto.setVersion(form.getVersion());
        Response<Integer> response = configService.addClientVersion(dto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 删除版本信息
     *
     * @param versionId
     * @return
     */
    @Override
    public Response delete(Integer versionId) {
        Response response = configService.deleteClientVersion(versionId);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新版本信息
     *
     * @param form
     * @return
     */
    @Override
    public Response update(UpdateClientVersionForm form) {
        ClientVersionDto dto = new ClientVersionDto();
        dto.setId(form.getId());
        dto.setChannel(form.getChannel());
        dto.setDescription(form.getDescription());
        dto.setDownloadUrl(form.getDownloadUrl());
        dto.setForceUpdate(form.getForceUpdate());
        dto.setVersion(form.getVersion());
        Response response = configService.updateClientVersion(dto);
        return ResponseUtil.getSimpleResponse(response);
    }
}
