package com.idear.user.center.console.vo;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class GetMenuListVO {

    private List<Map<String, Object>> menuList;

    public List<Map<String, Object>> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Map<String, Object>> menuList) {
        this.menuList = menuList;
    }
}
