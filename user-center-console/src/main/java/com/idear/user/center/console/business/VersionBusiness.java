/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddClientVersionForm;
import com.idear.user.center.console.form.UpdateClientVersionForm;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public interface VersionBusiness {

    /**
     * 分页获取版本列表
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response getVersionList(Integer pageNo, Integer pageSize);

    /**
     * 增加版本信息
     * @param form
     * @return
     */
    Response add(AddClientVersionForm form);

    /**
     * 删除版本信息
     * @param versionId
     * @return
     */
    Response delete(Integer versionId);

    /**
     * 更新版本信息
     * @param form
     * @return
     */
    Response update(UpdateClientVersionForm form);
}
