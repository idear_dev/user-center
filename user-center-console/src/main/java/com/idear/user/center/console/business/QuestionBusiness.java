/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddQuestionForm;
import com.idear.user.center.console.form.UpdateQuestionForm;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public interface QuestionBusiness {

    /**
     * 获取QA列表
     * @return
     */
    Response getQuestionList();

    /**
     * 添加QA
     * @param form
     * @return
     */
    Response add(AddQuestionForm form);

    /**
     * 删除QA
     * @param questionId
     * @return
     */
    Response delete(Integer questionId);

    /**
     * 更新QA
     * @param form
     * @return
     */
    Response update(UpdateQuestionForm form);
}
