package com.idear.user.center.console.converter;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.util.StringUtil;
import com.idear.user.center.console.vo.ManagerListVO;
import com.idear.user.center.console.vo.QueryManagerListVO;
import com.idear.user.center.core.po.Manager;
import com.idear.user.center.core.po.Role;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CcShan on 2017/9/26.
 */
@Component
public class ManagerConverter {

    public QueryManagerListVO convertToQueryManagerListVO(PageList<Manager> result, List<Role> roleList) {
        QueryManagerListVO queryManagerListVO = new QueryManagerListVO();
        queryManagerListVO.setTotalCount(result.getPaginator().getTotalCount());
        queryManagerListVO.setTotalPage(result.getPaginator().getTotalPages());
        List<Map<String, Object>> managerList = new ArrayList<>();
        for (Manager manager : result) {
            Map<String, Object> map = new HashMap<>();
            map.put("email", manager.getEmail());
            map.put("id", manager.getId());
            map.put("name", manager.getName());
            map.put("status", manager.getStatus());
            map.put("createTime", manager.getCreateTime());
            String roleIds = manager.getRoleIds();
            map.put("roleNames", getRoleNames(roleIds, roleList));
            map.put("roleIds", roleIds);
            managerList.add(map);
        }
        queryManagerListVO.setManagerList(managerList);
        return queryManagerListVO;
    }

    /**
     * roleIds转换成roleNames
     *
     * @param roleIds
     * @param roleList
     * @return
     */
    private String getRoleNames(String roleIds, List<Role> roleList) {
        if (StringUtil.isNotEmpty(roleIds)) {
            String[] roleIdArray = roleIds.split(",");
            StringBuilder roleNames = new StringBuilder();
            for (String roleIdStr : roleIdArray) {
                for (Role role : roleList) {
                    if (Integer.valueOf(roleIdStr).equals(role.getId())) {
                        roleNames.append(role.getName()).append(",");
                    }
                }
            }
            if(roleNames.length() > 0) {
                return roleNames.substring(0, roleNames.length() - 1).toString();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public ManagerListVO convert2ListVO(List<Manager> managerList) {
        ManagerListVO managerListVO = new ManagerListVO();
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (Manager manager : managerList) {
            Map<String, Object> managerMap = new HashMap<>();
            managerMap.put("email", manager.getEmail());
            managerMap.put("name", manager.getName());
            resultList.add(managerMap);
        }
        managerListVO.setManagerList(resultList);
        return managerListVO;
    }
}
