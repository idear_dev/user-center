package com.idear.user.center.console.vo;


import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class GetRoleListVO {

    private List<Map<String, Object>> roleList;

    private Integer totalCount;

    private Integer totalPage;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<Map<String, Object>> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Map<String, Object>> roleList) {
        this.roleList = roleList;
    }
}
