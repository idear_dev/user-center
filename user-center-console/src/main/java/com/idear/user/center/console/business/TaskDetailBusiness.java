/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddTaskDetailForm;
import com.idear.user.center.console.form.UpdateTaskDetailForm;

/**
 * @author ShanCc
 * @date 2018/11/5
 */
public interface TaskDetailBusiness {

    /**
     * 查询任务详情列表
     * @param planId
     * @return
     */
    Response getTaskDetailList(Integer planId);

    /**
     * 添加任务详情
     * @param form
     * @return
     */
    Response addTaskDetail(AddTaskDetailForm form);

    /**
     * 更新任务信息
     * @param form
     * @return
     */
    Response updateTaskDetail(UpdateTaskDetailForm form);
}
