package com.idear.user.center.console.controller;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.ValidateUtil;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.response.Response;
import com.idear.user.center.console.business.ManagerBusiness;
import com.idear.user.center.console.form.*;
import com.idear.user.center.console.message.ConsoleMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * @author cc
 * @date 2018-09-21
 */
@Controller
@RequestMapping(value = "user", method = RequestMethod.POST)
public class UserController {

    @Autowired
    private ManagerBusiness managerBusiness;

    /**
     * 获取用户信息
     *
     * @return
     */
    @RequestMapping(value = "get")
    @ResponseBody
    public String getManagerInfo() {
        return JsonUtil.toPrettyJson(managerBusiness.getManagerInfo());
    }

    /**
     * 创建用户信息
     *
     * @param createManagerForm
     * @return
     */
    @RequestMapping(value = "create")
    @ResponseBody
    public String createManager(@RequestBody CreateManagerForm createManagerForm) {
        ValidateUtil.validate(createManagerForm);
        String email = createManagerForm.getEmail();
        if (!email.endsWith("@rumiker.com")) {
            throw new BusinessException(ConsoleMessage.EMAIL_NOT_RIGHT);
        }
        String roleIds = createManagerForm.getRoleIds();
        return JsonUtil.toPrettyJson(managerBusiness.createManager(email, roleIds));
    }

    /**
     * 查询用户列表
     *
     * @param getManagerListForm
     * @return
     */
    @RequestMapping(value = "list")
    @ResponseBody
    public String getManagerList(@RequestBody GetManagerListForm getManagerListForm) {
        ValidateUtil.validate(getManagerListForm);
        return JsonUtil.toPrettyJson(managerBusiness.getManagerList(getManagerListForm));
    }

    /**
     * 编辑用户信息
     *
     * @param editManagerForm
     * @return
     */
    @RequestMapping(value = "edit")
    @ResponseBody
    public String editManager(@RequestBody EditManagerForm editManagerForm) {
        ValidateUtil.validate(editManagerForm);
        Integer id = editManagerForm.getId();
        String roleIds = editManagerForm.getRoleIds();
        return JsonUtil.toPrettyJson(managerBusiness.editManager(id, roleIds));
    }

    /**
     * 编辑用户信息
     *
     * @param loginForm
     * @return
     */
    @RequestMapping(value = "login")
    @ResponseBody
    public String logout(@RequestBody LoginForm loginForm) {
        ValidateUtil.validate(loginForm);
        String email = loginForm.getEmail();
        String password = loginForm.getPassword();
        return JsonUtil.toPrettyJson(managerBusiness.login(email,password));
    }

    /**
     * 退出系统
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "logout")
    @ResponseBody
    public String logout(HttpServletRequest request) {
        String token = request.getHeader(SystemHeader.TOKEN);
        return JsonUtil.toPrettyJson(managerBusiness.logout(token));
    }

    /**
     * 删除用户信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "delete")
    @ResponseBody
    public String deleteManager(@RequestBody ManagerIdForm form) {
        ValidateUtil.validate(form);
        return JsonUtil.toPrettyJson(managerBusiness.deleteManager(form.getUserId()));
    }

    /**
     * 打开用户信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "open")
    @ResponseBody
    public String openManager(@RequestBody ManagerIdForm form) {
        ValidateUtil.validate(form);
        return JsonUtil.toPrettyJson(managerBusiness.openManager(form.getUserId()));
    }

    /**
     * 关闭用户信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "close")
    @ResponseBody
    public String closeManager(@RequestBody ManagerIdForm form) {
        ValidateUtil.validate(form);
        return JsonUtil.toPrettyJson(managerBusiness.closeManager(form.getUserId()));
    }
}
