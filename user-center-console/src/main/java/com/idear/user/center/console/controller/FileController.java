/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.action.Action;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.FileBusiness;
import com.idear.user.center.service.dto.MemberInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ShanCc
 * @date 2018/10/11
 */
@Controller
@RequestMapping(value = "file", method = RequestMethod.POST)
public class FileController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private FileBusiness fileBusiness;

    /**
     * 图片上传
     *
     * @return
     */
    @RequestMapping(value = "image/upload", method = RequestMethod.POST)
    @ResponseBody
    public String uploadImage(@RequestParam("uploadFile") final MultipartFile multipartFile) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                if (multipartFile == null){
                    throw new BusinessException(BaseMessage.ERROR_PARAM);
                }
                return fileBusiness.uploadImage(multipartFile);
            }
        });
    }

    /**
     * 上传Apk文件
     *
     * @return
     */
    @RequestMapping(value = "apk/upload", method = RequestMethod.POST)
    @ResponseBody
    public String uploadApk(@RequestParam("uploadFile") final MultipartFile multipartFile) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                if (multipartFile == null){
                    throw new BusinessException(BaseMessage.ERROR_PARAM);
                }
                return fileBusiness.uploadApk(multipartFile);
            }
        });
    }
}
