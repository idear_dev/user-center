package com.idear.user.center.console.bo;

/**
 * Created by CcShan on 2017/9/22.
 */
public class Node implements Comparable {
    /**
     * 节点编号
     */
    public Integer id;

    /**
     * 节点序号
     */
    public Integer sort;

    /**
     * 节点内容
     */
    public String name;

    /**
     * 跳转地址
     */
    public String href;
    /**
     * 父节点编号
     */
    public Integer parentId;
    /**
     * 孩子节点列表
     */
    public Children children = new Children();

    // 先序遍历，拼接JSON字符串
    public String toString() {
        String result = "{"
                + "id : '" + id + "'"
                + ", name : '" + name + "'"
                + ", href : '" + href + "'"
                + ", sort : '" + sort + "'"
                + ", parentId : '" + parentId + "'";

        if (children != null && children.getSize() != 0) {
            result += ", children : " + children.toString();
        }
        return result + "}";
    }

    // 兄弟节点横向排序
    public void sortChildren() {
        if (children != null && children.getSize() != 0) {
            children.sortChildren();
        }
    }

    // 添加孩子节点
    public void addChild(Node node) {
        this.children.addChild(node);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}