package com.idear.user.center.console.form;


/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class QueryResourceForm extends PageQueryBaseForm{
    /**
     * 名称查询
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
