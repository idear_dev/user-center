/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddSmsTemplateForm;
import com.idear.user.center.console.form.UpdateSmsTemplateForm;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
public interface SmsTemplateBusiness {

    /**
     * 添加短信模板信息
     * @param form
     * @return
     */
    Response add(AddSmsTemplateForm form);

    /**
     * 删除短信模版
     * @param templateId
     * @return
     */
    Response delete(Integer templateId);

    /**
     * 更新模版信息
     * @param form
     * @return
     */
    Response update(UpdateSmsTemplateForm form);

    /**
     * 查询所有模板信息
     * @return
     */
    Response getTemplateList();
}
