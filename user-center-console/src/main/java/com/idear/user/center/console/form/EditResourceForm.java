package com.idear.user.center.console.form;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class EditResourceForm {
    /**
     * 资源id
     */
    private int id;
    /**
     * 资源名称
     */
    private String name;
    /**
     * 资源url
     */
    private String urlPattern;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }
}
