package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class CreateRoleForm {

    private Integer groupId;

    @NotEmpty(message = "menu_ids_not_empty")
    private String menuIds;

    @NotEmpty(message = "role_name_not_empty")
    private String roleName;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String menuIds) {
        this.menuIds = menuIds;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
