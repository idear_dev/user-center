package com.idear.user.center.console.business;


import com.idear.framework.response.Response;
import com.idear.user.center.console.form.CreateResourceForm;
import com.idear.user.center.console.form.EditResourceForm;
import com.idear.user.center.console.form.QueryResourceForm;

/**
 * @author cc
 * @date 2018-09-22
 */
public interface ResourceBusiness {
    /**
     * 添加资源
     * @param form
     * @return
     */
    Response createResource(CreateResourceForm form);

    /**
     * 编辑资源
     * @param form
     * @return
     */
    Response editResource(EditResourceForm form);

    /**
     * 删除资源
     * @param id
     * @return
     */
    Response deleteResource(Integer id);

    /**
     * 查询资源列表
     * @param form
     * @return
     */
    Response queryResourceList(QueryResourceForm form);

    /**
     * 查询所有资源列表
     * @return
     */
    Response queryAllResourceList();
}
