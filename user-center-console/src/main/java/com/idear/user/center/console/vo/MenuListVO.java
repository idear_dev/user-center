package com.idear.user.center.console.vo;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class MenuListVO {

    private Object menu;

    public Object getMenu() {
        return menu;
    }

    public void setMenu(Object menu) {
        this.menu = menu;
    }
}
