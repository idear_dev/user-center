/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class AddAppInfoForm {

    @NotEmpty(message = "app_name_not_empty")
    private String appName;

    @NotEmpty(message = "app_description_not_empty")
    private String appDescription;

    @NotEmpty(message = "app_logo_url_not_empty")
    private String appLogoUrl;

    @NotEmpty(message = "package_name_not_empty")
    private String packageName;

    @NotEmpty(message = "company_not_empty")
    private String company;

    @NotEmpty(message = "phone_number_not_empty")
    private String phoneNumber;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppDescription() {
        return appDescription;
    }

    public void setAppDescription(String appDescription) {
        this.appDescription = appDescription;
    }

    public String getAppLogoUrl() {
        return appLogoUrl;
    }

    public void setAppLogoUrl(String appLogoUrl) {
        this.appLogoUrl = appLogoUrl;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
