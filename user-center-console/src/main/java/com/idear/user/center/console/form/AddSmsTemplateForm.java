/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
public class AddSmsTemplateForm {

    /**
     * 模板编号
     */
    @NotEmpty(message = "code_not_empty")
    private String code;

    /**
     * 模板内容
     */
    @NotEmpty(message = "content_not_empty")
    private String content;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
