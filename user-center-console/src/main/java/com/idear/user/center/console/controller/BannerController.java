/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.BannerBusiness;
import com.idear.user.center.console.form.AddBannerForm;
import com.idear.user.center.console.form.DeleteBannerForm;
import com.idear.user.center.console.form.UpdateBannerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ShanCc
 * @date 2018/10/11
 */
@Controller
@RequestMapping(value = "banner", method = RequestMethod.POST)
public class BannerController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private BannerBusiness bannerBusiness;

    @RequestMapping(value = "/location/list", method = RequestMethod.POST)
    @ResponseBody
    public String getLocationList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return bannerBusiness.getLocationList();
            }
        });
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getBannerList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return bannerBusiness.getBannerList();
            }
        });
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addBanner(@RequestBody final AddBannerForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return bannerBusiness.add(form);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteBanner(@RequestBody final DeleteBannerForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer bannerId = form.getId();
                return bannerBusiness.delete(bannerId);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateBanner(@RequestBody final UpdateBannerForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return bannerBusiness.update(form);
            }
        }, bindingResult);
    }
}
