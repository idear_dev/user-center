/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ShanCc
 * @date 2018/10/24
 */
public class AddMarketForm {

    /**
     * 应用市场名称
     */
    @NotEmpty(message = "name_not_empty")
    private String name;

    /**
     * 应用市场Logo
     */
    @NotEmpty(message = "logo_url_not_empty")
    private String logoUrl;

    /**
     * 应用市场Logo
     */
    @NotEmpty(message = "package_name_not_empty")
    private String packageName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
