/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.TaskDetailBusiness;
import com.idear.user.center.console.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ShanCc
 * @date 2018/10/23
 */
@Controller
@RequestMapping(value = "task/detail", method = RequestMethod.POST)
public class TaskDetailController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private TaskDetailBusiness taskDetailBusiness;


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getTaskDetailList(@RequestBody final QueryTaskDetailForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer planId = form.getPlanId();
                return taskDetailBusiness.getTaskDetailList(planId);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addTaskDetail(@RequestBody final AddTaskDetailForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return taskDetailBusiness.addTaskDetail(form);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateTaskDetail(@RequestBody final UpdateTaskDetailForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return taskDetailBusiness.updateTaskDetail(form);
            }
        }, bindingResult);
    }
}
