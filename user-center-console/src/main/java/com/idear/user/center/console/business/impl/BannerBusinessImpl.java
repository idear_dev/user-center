/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.BannerBusiness;
import com.idear.user.center.console.form.AddBannerForm;
import com.idear.user.center.console.form.UpdateBannerForm;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.core.enums.BannerLocation;
import com.idear.user.center.service.dto.BannerDto;
import com.idear.user.center.service.facade.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/10/11
 */
@Component
public class BannerBusinessImpl implements BannerBusiness {

    @Autowired
    private BannerService bannerService;

    /**
     * 获取banner location 信息
     *
     * @return
     */
    @Override
    public Response getLocationList() {
        Response<List<Map<String,Object>>> response = bannerService.listLocation();
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 添加banner信息
     *
     * @param form
     * @return
     */
    @Override
    public Response add(AddBannerForm form) {
        BannerLocation location = BannerLocation.getLocation(form.getLocation());
        if (location == null){
            throw new BusinessException(ConsoleMessage.BANNER_LOCATION_NOT_EXIST);
        }
        BannerDto bannerDto = new BannerDto();
        bannerDto.setImageUrl(form.getImageUrl());
        bannerDto.setLinkUrl(form.getLinkUrl());
        bannerDto.setTitle(form.getTitle());
        bannerDto.setLocation(form.getLocation());
        bannerDto.setSort(form.getSort());
        Response<Integer> response = bannerService.addBanner(bannerDto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 删除广告问
     *
     * @param bannerId
     * @return
     */
    @Override
    public Response delete(Integer bannerId) {
        Response<Integer> response = bannerService.deleteBanner(bannerId);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新banner信息
     *
     * @param form
     * @return
     */
    @Override
    public Response update(UpdateBannerForm form) {
        BannerDto bannerDto = new BannerDto();
        bannerDto.setId(form.getId());
        bannerDto.setImageUrl(form.getImageUrl());
        bannerDto.setLinkUrl(form.getLinkUrl());
        bannerDto.setTitle(form.getTitle());
        bannerDto.setLocation(form.getLocation());
        bannerDto.setSort(form.getSort());
        bannerDto.setStatus(form.getStatus());
        Response<Integer> response = bannerService.updateBanner(bannerDto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 查询所有Banner信息
     *
     * @return
     */
    @Override
    public Response getBannerList() {
        Response<List<BannerDto>> response = bannerService.listBanner();
        return response;
    }
}
