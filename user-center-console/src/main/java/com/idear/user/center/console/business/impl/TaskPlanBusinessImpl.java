/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.common.util.DateUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.PageQueryUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.TaskPlanBusiness;
import com.idear.user.center.console.form.AddTaskPlanForm;
import com.idear.user.center.console.form.QueryPlanForm;
import com.idear.user.center.console.form.UpdateTaskPlanForm;
import com.idear.user.center.service.dto.TaskPlanDto;
import com.idear.user.center.service.dto.TaskPlanListDto;
import com.idear.user.center.service.dto.Tutorial;
import com.idear.user.center.service.facade.TaskPlanService;
import com.idear.user.center.service.param.TaskPlanListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
@Component
public class TaskPlanBusinessImpl implements TaskPlanBusiness {

    @Autowired
    private TaskPlanService taskPlanService;

    /**
     * 查询任务计划
     *
     * @param form
     * @return
     */
    @Override
    public Response getTaskPlanList(QueryPlanForm form) {
        TaskPlanListParam param = new TaskPlanListParam();
        param.setPageNo(form.getPageNo() == null ? PageQueryUtil.DEFAULT_PAGE_NO : form.getPageNo());
        param.setPageSize(form.getPageSize() == null ? PageQueryUtil.DEFAULT_PAGE_SIZE : form.getPageSize());
        param.setName(form.getName());
        if (StringUtil.isNotEmpty(form.getBeginTime())){
            param.setBeginTime(DateUtil.parseDate(form.getBeginTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            param.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        Response<TaskPlanListDto> response = taskPlanService.queryTaskPlan(param);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 添加任务计划
     *
     * @param form
     * @return
     */
    @Override
    public Response addTaskPlan(AddTaskPlanForm form) {
        TaskPlanDto dto = new TaskPlanDto();
        dto.setAppId(form.getAppId());
        dto.setMarketId(form.getMarketId());
        dto.setName(form.getName());
        dto.setDescription(form.getDescription());
        if (StringUtil.isNotEmpty(form.getStartTime())){
            dto.setStartTime(DateUtil.parseDate(form.getStartTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            dto.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        dto.setTotalNum(form.getTotalNum());
        dto.setType(form.getType());
        dto.setTypeInfo(form.getTypeInfo());
        List<String> label = form.getLabelList();
        if (label !=null && !label.isEmpty()) {
            dto.setLabelList(label);
        }
        List<Tutorial> tutorial = form.getTutorialList();
        if (tutorial != null && !tutorial.isEmpty()) {
            dto.setTutorialList(tutorial);
        }
        Response<Integer> response = taskPlanService.addTaskPlan(dto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新任务计划
     *
     * @param form
     * @return
     */
    @Override
    public Response updateTaskPlan(UpdateTaskPlanForm form) {
        TaskPlanDto dto = new TaskPlanDto();
        dto.setId(form.getId());
        dto.setAppId(form.getAppId());
        dto.setMarketId(form.getMarketId());
        dto.setName(form.getName());
        dto.setDescription(form.getDescription());
        if (StringUtil.isNotEmpty(form.getStartTime())){
            dto.setStartTime(DateUtil.parseDate(form.getStartTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            dto.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        dto.setTotalNum(form.getTotalNum());
        dto.setType(form.getType());
        dto.setTypeInfo(form.getTypeInfo());
        dto.setStatus(form.getStatus());
        List<String> label = form.getLabelList();
        if (label !=null && !label.isEmpty()) {
            dto.setLabelList(label);
        }
        List<Tutorial> tutorial = form.getTutorialList();
        if (tutorial != null && !tutorial.isEmpty()) {
            dto.setTutorialList(tutorial);
        }
        Response<Integer> response = taskPlanService.updateTaskPlan(dto);
        return ResponseUtil.getSimpleResponse(response);
    }
}
