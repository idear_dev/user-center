package com.idear.user.center.console.business.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.framework.response.Response;
import com.idear.user.center.console.business.ResourceBusiness;
import com.idear.user.center.console.converter.ResourceConverter;
import com.idear.user.center.console.form.CreateResourceForm;
import com.idear.user.center.console.form.EditResourceForm;
import com.idear.user.center.console.form.QueryResourceForm;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.console.vo.PageResourceListVO;
import com.idear.user.center.console.vo.ResourceListVO;
import com.idear.user.center.core.enums.DelFlag;
import com.idear.user.center.core.po.Resource;
import com.idear.user.center.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * create by wenqx on 2017/11/20
 **/
@Component
public class ResourceBusinessImpl implements ResourceBusiness {

    @Autowired
    private ResourceService resourceService;

    @Override
    public Response createResource(CreateResourceForm form) {
        Resource addResource = ResourceConverter.convertAddResourceForm2Resource(form);
        Resource resource = resourceService.queryAllResourceByName(form.getName());
        if(resource == null) {
            resourceService.addResource(addResource);
        } else if (DelFlag.DELETED.getCode().equals(resource.getDelFlag())) {
            addResource.setId(resource.getId());
            addResource.setDelFlag(DelFlag.NORMAL.getCode());
            resourceService.editResource(addResource);
        } else {
            throw new BusinessException(ConsoleMessage.RESOURCE_IS_EXISTS);
        }

        return Response.SUCCESS;
    }


    @Override
    public Response editResource(EditResourceForm form) {
        Resource resource = ResourceConverter.convertEditResourceForm2Resource(form);
        resourceService.editResource(resource);
        return Response.SUCCESS;
    }

    @Override
    public Response deleteResource(Integer id) {
        resourceService.deleteResource(id);
        return Response.SUCCESS;
    }

    @Override
    public Response queryResourceList(QueryResourceForm form) {
        Resource resource = ResourceConverter.convertQueryResourceForm2Resource(form);
        PageList pageList = resourceService.queryResourceList(resource,form.getPageNo(),form.getPageSize());
        PageResourceListVO pageResourceListVO = ResourceConverter.convertPageList2GetResourceListVO(pageList);
        return new Response(pageResourceListVO);
    }

    @Override
    public Response queryAllResourceList() {
        ResourceListVO resourceListVO = ResourceConverter.convertList2GetResourceListVO(resourceService.queryAllResourceList());
        return new Response(resourceListVO);
    }
}
