/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.QuestionBusiness;
import com.idear.user.center.console.form.AddQuestionForm;
import com.idear.user.center.console.form.DeleteQuestionForm;
import com.idear.user.center.console.form.UpdateQuestionForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author ShanCc
 * @date 2018/10/23
 */
@Controller
@RequestMapping(value = "question", method = RequestMethod.POST)
public class QuestionController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Resource
    private QuestionBusiness questionBusiness;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getQuestionList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return questionBusiness.getQuestionList();
            }
        });
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addQuestion(@RequestBody final AddQuestionForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return questionBusiness.add(form);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteQuestion(@RequestBody final DeleteQuestionForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer questionId = form.getId();
                return questionBusiness.delete(questionId);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateQuestion(@RequestBody final UpdateQuestionForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return questionBusiness.update(form);
            }
        }, bindingResult);
    }
}
