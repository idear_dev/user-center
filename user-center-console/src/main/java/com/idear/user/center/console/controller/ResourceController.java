package com.idear.user.center.console.controller;

import com.idear.common.util.JsonUtil;
import com.idear.common.util.ValidateUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.ResourceBusiness;
import com.idear.user.center.console.form.CreateResourceForm;
import com.idear.user.center.console.form.DeleteResourceForm;
import com.idear.user.center.console.form.EditResourceForm;
import com.idear.user.center.console.form.QueryResourceForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @author cc
 * @date 2018-09-21
 **/
@Controller
@RequestMapping(value = "resource", method = RequestMethod.POST)
public class ResourceController {

    @Autowired
    private ResourceBusiness resourceBusiness;

    @Autowired
    private ControllerTemplate controllerTemplate;

    /**
     * 新建菜单
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "create")
    @ResponseBody
    public String createResource(@Valid @RequestBody final CreateResourceForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return resourceBusiness.createResource(form);
            }
        },bindingResult);
    }

    /**
     * 查看菜单信息(分页）
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "list")
    @ResponseBody
    public String queryResourceList(@Valid @RequestBody final QueryResourceForm form,BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return resourceBusiness.queryResourceList(form);
            }
        },bindingResult);
    }

    /**
     * 查看所有菜单信息
     * @return
     */
    @RequestMapping(value = "all/list")
    @ResponseBody
    public String queryResourceAllList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return resourceBusiness.queryAllResourceList();
            }
        });
    }


    /**
     * 编辑菜单信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "edit")
    @ResponseBody
    public String editResource(@Valid @RequestBody final EditResourceForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return resourceBusiness.editResource(form);
            }
        },bindingResult);
    }

    /**
     * 删除菜单信息
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(@Valid @RequestBody final DeleteResourceForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return resourceBusiness.deleteResource(form.getResourceId());
            }
        },bindingResult);
    }
}
