package com.idear.user.center.console.vo;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class GetManagerInfoVO {

    private Object menu;

    private String email;

    private String name;

    public Object getMenu() {
        return menu;
    }

    public void setMenu(Object menu) {
        this.menu = menu;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
