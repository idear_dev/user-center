package com.idear.user.center.console.business.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.framework.response.Response;
import com.idear.user.center.console.business.RoleBusiness;
import com.idear.user.center.console.component.MenuComponent;
import com.idear.user.center.console.converter.RoleConverter;
import com.idear.user.center.console.form.CreateRoleForm;
import com.idear.user.center.console.form.EditRoleForm;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.console.vo.MenuListVO;
import com.idear.user.center.core.enums.RoleGroupStatus;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.core.po.Role;
import com.idear.user.center.core.po.RoleGroup;
import com.idear.user.center.service.MenuService;
import com.idear.user.center.service.RoleGroupService;
import com.idear.user.center.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author cc
 * @date 2018-09-21
 */
@Component
public class RoleBusinessImpl implements RoleBusiness {

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleGroupService roleGroupService;

    @Autowired
    private RoleConverter roleConverter;

    @Autowired
    private MenuComponent menuComponent;

    /**
     * 获取角色列表分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response pageableRoleList(Integer pageNo, Integer pageSize) {
        if (pageSize > 100) {
            pageSize = 100;
        }
        PageList<Role> roleList = roleService.pageableRoleList(pageNo, pageSize);
        List<RoleGroup> roleGroupList = roleGroupService.getRoleGroupByStatus(null);
        List<Menu> menuList = menuService.getMenuList();
        return new Response(roleConverter.convertToGetRoleListVO(roleList, roleGroupList, menuList));
    }

    /**
     * 创建分组信息
     *
     * @param groupName
     * @param groupCode
     * @return
     */
    @Override
    public Response createGroup(String groupName, String groupCode) {
        RoleGroup roleGroup = roleGroupService.getRoleGroupByName(groupName);
        if (roleGroup == null) {
            roleGroup = new RoleGroup();
            Date date = new Date();
            roleGroup.setName(groupName);
            roleGroup.setCode(groupCode);
            roleGroup.setStatus(RoleGroupStatus.OPENED.getStatus());
            roleGroup.setCreateTime(date);
            roleGroup.setUpdateTime(date);
            roleGroupService.addRoleGroup(roleGroup);
        } else if (RoleGroupStatus.CLOSED.getStatus().equals(roleGroup.getStatus())) {
            roleGroupService.updateRoleGroupStatus(roleGroup.getId(), RoleGroupStatus.OPENED);
        }
        return Response.SUCCESS;
    }

    /**
     * 删除分组
     *
     * @param groupId
     * @return
     */
    @Override
    public Response deleteGroup(Integer groupId) {
        if (groupId != null) {
            List<Role> list = roleService.getRoleListByGroupId(groupId);
            if(!CollectionUtils.isEmpty(list)) {
                throw new BusinessException(ConsoleMessage.ROLE_CONTAINS_GROUP);
            }
            roleGroupService.updateRoleGroupStatus(groupId, RoleGroupStatus.CLOSED);
        }
        return Response.SUCCESS;
    }

    /**
     * 创建角色
     *
     * @param createRoleForm
     * @return
     */
    @Override
    public Response createRole(CreateRoleForm createRoleForm) {
        try {
            Role role = new Role();
            Date date = new Date();
            role.setName(createRoleForm.getRoleName());
            role.setGroupId(createRoleForm.getGroupId());
            role.setMenuIds(createRoleForm.getMenuIds());
            role.setCreateTime(date);
            role.setUpdateTime(date);
            roleService.createRole(role);
        } catch (DuplicateKeyException e) {
            throw new BusinessException(ConsoleMessage.ROLE_IS_EXIST);
        }

        return Response.SUCCESS;
    }

    /**
     * 获取角色
     *
     * @param roleId
     * @return
     */
    @Override
    public Response getRole(Integer roleId) {
        List<Integer> roleIdList = new ArrayList<>();
        roleIdList.add(roleId);
        List<Role> roleList = roleService.getRoleListById(roleIdList);
        if (roleIdList != null && !roleIdList.isEmpty()) {
            return new Response(roleList.get(0));
        } else {
            return Response.SUCCESS;
        }

    }

    /**
     * 编辑分组
     *
     * @param groupId
     * @param groupName
     * @return
     */
    @Override
    public Response editGroup(Integer groupId, String groupName) {
        try {
            roleGroupService.updateRoleGroupName(groupId, groupName);
        } catch (DuplicateKeyException e) {
            throw new BusinessException(ConsoleMessage.GROUP_NAME_IS_EXIST);
        }
        return Response.SUCCESS;
    }

    /**
     * 编辑角色
     *
     * @param editRoleForm
     * @return
     */
    @Override
    public Response editRole(EditRoleForm editRoleForm) {
        Role role = new Role();
        role.setId(editRoleForm.getRoleId());
        role.setMenuIds(editRoleForm.getMenuIds());
        role.setGroupId(editRoleForm.getGroupId());
        role.setName(editRoleForm.getRoleName());
        role.setUpdateTime(new Date());
        roleService.updateRole(role);
        return Response.SUCCESS;
    }

    /**
     * 获取所有分组列表
     *
     * @return
     */
    @Override
    public Response getGroupList() {
        List<RoleGroup> roleGroupList = roleGroupService.getRoleGroupByStatus(RoleGroupStatus.OPENED);
        return new Response(roleGroupList);
    }

    /**
     * 获取所有菜单列表
     *
     * @return
     */
    @Override
    public Response getMenuList() {
        List<Menu> menuList = menuService.getMenuList();
        menuComponent.getMenuInfo(menuList);
        MenuListVO menuListVO = new MenuListVO();
        menuListVO.setMenu(menuComponent.getMenuInfo(menuList).getJSONArray("children"));
        return new Response(menuListVO);
    }

    /**
     * 获取所有角色列表，不分页
     * @return
     */
    @Override
    public Response getRoleList() {
        List<Role> roleList = roleService.getRoleList();
        return new Response(roleConverter.convertToRoleAllListVO(roleList));
    }
}
