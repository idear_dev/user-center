/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.DateUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.TaskRecordBusiness;
import com.idear.user.center.console.form.AuditTaskRecordForm;
import com.idear.user.center.console.form.QueryTaskRecordForm;
import com.idear.user.center.console.vo.TaskRecordListVO;
import com.idear.user.center.service.dto.TaskRecordDto;
import com.idear.user.center.service.dto.TaskRecordListDto;
import com.idear.user.center.service.facade.TaskRecordService;
import com.idear.user.center.service.param.TaskRecordListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author ShanCc
 * @date 2018/11/5
 */
@Component
public class TaskRecordBusinessImpl implements TaskRecordBusiness {

    @Autowired
    private TaskRecordService taskRecordService;

    /**
     * 查询任务记录状态集合
     *
     * @return
     */
    @Override
    public Response getTaskRecordStatus() {
        Response<List<Map<String,Object>>> response = taskRecordService.listStatus();
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 查询任务记录列表
     *
     * @param form
     * @return
     */
    @Override
    public Response getTaskRecordList(QueryTaskRecordForm form) {
        TaskRecordListParam param = new TaskRecordListParam();
        param.setPageNo(form.getPageNo());
        param.setPageSize(form.getPageSize());
        if (form.getStatus() != null){
            List<Integer> statusList = new ArrayList<>();
            statusList.add(form.getStatus());
            param.setStatusList(statusList);
        }
        if (StringUtil.isNotEmpty(form.getBeginTime())){
            param.setBeginTime(DateUtil.parseDate(form.getBeginTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            param.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        Response<TaskRecordListDto> response = taskRecordService.getRecordList(param);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            TaskRecordListDto taskRecordListDto = response.getRespBody();
            if (taskRecordListDto == null || taskRecordListDto.getTaskRecordList().isEmpty()) {
                return Response.SUCCESS;
            } else {
                return new Response<>(convert2TaskRecordListVO(taskRecordListDto));
            }
        }
    }

    public static TaskRecordListVO convert2TaskRecordListVO(TaskRecordListDto taskRecordListDto) {
        TaskRecordListVO taskRecordListVO = new TaskRecordListVO();
        taskRecordListVO.setTotalNum(taskRecordListDto.getTotalNum());
        taskRecordListVO.setTotalPage(taskRecordListDto.getTotalPage());
        List<TaskRecordDto> dtoList = taskRecordListDto.getTaskRecordList();
        List<Map<String, Object>> taskRecordList = new ArrayList<>();
        for (TaskRecordDto taskRecordDto : dtoList) {
            taskRecordList.add(convert2TaskDetailMap(taskRecordDto));
        }
        taskRecordListVO.setRecordList(taskRecordList);
        return taskRecordListVO;
    }

    public static Map<String,Object> convert2TaskDetailMap(TaskRecordDto taskRecordDto){
        Map<String, Object> temp = new HashMap<>(32);
        JSONObject appInfo = JsonUtil.parseJsonObject(taskRecordDto.getAppInfo());
        JSONObject taskInfo = JsonUtil.parseJsonObject(taskRecordDto.getTaskInfo());
        JSONObject planInfo = JsonUtil.parseJsonObject(taskRecordDto.getPlanInfo());
        planInfo.put("label",planInfo.getJSONArray("label"));
        planInfo.put("tutorial",planInfo.getJSONArray("tutorial"));
        if (StringUtil.isNotEmpty(taskRecordDto.getMarketInfo())){
            JSONObject marketInfo = JsonUtil.parseJsonObject(taskRecordDto.getMarketInfo());
            temp.put("marketInfo",marketInfo);
        }
        if (StringUtil.isNotEmpty(taskRecordDto.getAuditInfo())){
            JSONArray auditInfo = JsonUtil.parseJsonArray(taskRecordDto.getAuditInfo());
            temp.put("auditInfo",auditInfo);
        }

        Date submitTime = taskRecordDto.getSubmitTime();
        if (submitTime != null){
            temp.put("submitTime", DateUtil.getFullCNPatternNow(submitTime));
        }else {
            temp.put("submitTime", null);
        }
        String auditRemark = taskRecordDto.getAuditRemark();
        if (StringUtil.isNotEmpty(taskRecordDto.getAuditRemark())){
            temp.put("auditRemark", auditRemark);
        }else {
            temp.put("auditRemark", null);
        }
        Date auditTime = taskRecordDto.getAuditTime();
        if (auditTime != null){
            temp.put("auditTime", DateUtil.getFullCNPatternNow(auditTime));
        }else {
            temp.put("auditTime", null);
        }
        temp.put("recordId", taskRecordDto.getRecordId());
        temp.put("appInfo",appInfo);
        temp.put("taskInfo",taskInfo);
        temp.put("planInfo",planInfo);
        temp.put("status", taskRecordDto.getStatus());
        temp.put("createTime",taskRecordDto.getCreateTime());
        temp.put("updateTime",taskRecordDto.getUpdateTime());
        return temp;
    }

    /**
     * 审核任务记录
     *
     * @param form
     * @return
     */
    @Override
    public Response auditTaskRecord(AuditTaskRecordForm form) {
        Integer recordId = form.getId();
        Boolean pass = form.getPass();
        String remark = form.getRemark();
        Response response = taskRecordService.audit(recordId,pass,remark);
        return ResponseUtil.getSimpleResponse(response);
    }
}
