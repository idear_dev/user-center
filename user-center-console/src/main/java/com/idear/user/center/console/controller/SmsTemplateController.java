/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.SmsTemplateBusiness;
import com.idear.user.center.console.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ShanCc
 * @date 2018/10/23
 */
@Controller
@RequestMapping(value = "sms/template", method = RequestMethod.POST)
public class SmsTemplateController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private SmsTemplateBusiness smsTemplateBusiness;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getTemplateList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return smsTemplateBusiness.getTemplateList();
            }
        });
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteTemplate(@RequestBody final DeleteSmsTemplateForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer templateId = form.getId();
                return smsTemplateBusiness.delete(templateId);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addTemplate(@RequestBody final AddSmsTemplateForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return smsTemplateBusiness.add(form);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateTemplate(@RequestBody final UpdateSmsTemplateForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return smsTemplateBusiness.update(form);
            }
        }, bindingResult);
    }
}
