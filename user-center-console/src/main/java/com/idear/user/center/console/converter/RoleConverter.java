package com.idear.user.center.console.converter;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.util.StringUtil;
import com.idear.user.center.console.vo.GetRoleListVO;
import com.idear.user.center.console.vo.RoleAllListVO;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.core.po.Role;
import com.idear.user.center.core.po.RoleGroup;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CcShan on 2017/9/19.
 */
@Component
public class RoleConverter {

    public GetRoleListVO convertToGetRoleListVO(PageList<Role> roleList, List<RoleGroup> roleGroupList, List<Menu> menuList) {
        List<Map<String, Object>> list = new ArrayList<>();
        GetRoleListVO getRoleListVO = new GetRoleListVO();
        getRoleListVO.setRoleList(list);
        if (roleList != null || !roleList.isEmpty()) {
            for (Role role : roleList) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("roleId", role.getId());
                temp.put("roleName", role.getName());
                temp.put("menuIds",role.getMenuIds());
                temp.put("menuNames",getMenuNames(role.getMenuIds(), menuList));
                for (RoleGroup roleGroup : roleGroupList) {
                    if (roleGroup.getId().equals(role.getGroupId())) {
                        temp.put("groupId",roleGroup.getId());
                        temp.put("groupName", roleGroup.getName());
                    }
                }
                list.add(temp);
            }
        }
        getRoleListVO.setTotalCount(roleList.getPaginator().getTotalCount());
        getRoleListVO.setTotalPage(roleList.getPaginator().getTotalPages());
        return getRoleListVO;
    }

    public RoleAllListVO convertToRoleAllListVO(List<Role> roleList) {
        List<Map<String, Object>> list = new ArrayList<>();
        RoleAllListVO roleAllListVO = new RoleAllListVO();
        if (roleList != null || !roleList.isEmpty()) {
            for (Role role : roleList) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("roleId", role.getId());
                temp.put("roleName", role.getName());
                list.add(temp);
            }
        }
        roleAllListVO.setRoleList(list);
        return roleAllListVO;
    }

    private String getMenuNames(String menuIds, List<Menu> menuList) {
        if (StringUtil.isNotEmpty(menuIds)) {
            String[] roleIdArray = menuIds.split(",");
            StringBuilder menuNames = new StringBuilder();
            for (String roleIdStr : roleIdArray) {
                for (Menu menu : menuList) {
                    if (Integer.valueOf(roleIdStr).equals(menu.getId())) {
                        menuNames.append(menu.getName()).append(",");
                    }
                }
            }
            return menuNames.substring(0, menuNames.length() - 1).toString();
        } else {
            return null;
        }
    }

}
