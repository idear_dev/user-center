package com.idear.user.center.console.bo;


import com.idear.user.center.core.po.Manager;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.core.po.Resource;
import com.idear.user.center.core.po.Role;

import java.util.List;
import java.util.Set;

/**
 *
 * @author ShanCc
 * @date 2017/9/21
 */
public class ManagerInfo {

    private Manager manager;

    private List<Role> roleList;

    private List<Menu> menuList;

    private List<Resource> resourceList;

    private Set<String> resourceSet;

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public List<Resource> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<Resource> resourceList) {
        this.resourceList = resourceList;
    }

    public Set<String> getResourceSet() {
        return resourceSet;
    }

    public void setResourceSet(Set<String> resourceSet) {
        this.resourceSet = resourceSet;
    }
}
