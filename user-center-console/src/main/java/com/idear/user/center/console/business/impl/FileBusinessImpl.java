/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.FileUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.user.center.console.business.FileBusiness;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.service.facade.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author ShanCc
 * @date 2018/10/12
 */
@Component
public class FileBusinessImpl implements FileBusiness {

    @Autowired
    private FileService fileService;

    /**
     * 上传图片信息
     *
     * @param multipartFile
     * @return
     */
    @Override
    public Response uploadImage(MultipartFile multipartFile) {
        try {
            InputStream inputStream = multipartFile.getInputStream();
            String originalFilename = multipartFile.getOriginalFilename();
            String fileType = FileUtil.getFileType(originalFilename);
            String path = "image/"+ UUID.randomUUID().toString()+"."+fileType;
            if (!FileUtil.isCorrectImageType(fileType)){
                throw new BusinessException(ConsoleMessage.UPLOAD_FILE_NOT_SUPPORT);
            }
            Response response = fileService.uploadFile(inputStream,path);
            if (response == null) {
                throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
            } else if (!response.success()) {
                throw new BusinessException(response.getRespCode(), response.getRespMsg());
            } else {
                Map<String,Object> resultMap = new HashMap<>(2);
                resultMap.put("imageUrl",response.getRespBody());
                return new Response(resultMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(ConsoleMessage.UPLOAD_READ_FILE_FAILED);
        }
    }

    /**
     * 上传APK文件
     *
     * @param multipartFile
     * @return
     */
    @Override
    public Response uploadApk(MultipartFile multipartFile) {
        try {
            InputStream inputStream = multipartFile.getInputStream();
            String originalFilename = multipartFile.getOriginalFilename();
            String fileType = FileUtil.getFileType(originalFilename);
            String path = "apk/"+ UUID.randomUUID().toString()+"."+fileType;
            if (StringUtil.isEmpty(fileType) || fileType.endsWith(APK_FILE_TYPE)){
                throw new BusinessException(ConsoleMessage.UPLOAD_FILE_NOT_SUPPORT);
            }
            Response response = fileService.uploadFile(inputStream,path);
            if (response == null) {
                throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
            } else if (!response.success()) {
                throw new BusinessException(response.getRespCode(), response.getRespMsg());
            } else {
                Map<String,Object> resultMap = new HashMap<>(2);
                resultMap.put("downloadUrl",response.getRespBody());
                return new Response(resultMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(ConsoleMessage.UPLOAD_READ_FILE_FAILED);
        }
    }

    private static final String APK_FILE_TYPE = "apk";
}
