/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ShanCc
 * @date 2018/10/12
 */
public interface FileBusiness {

    /**
     * 上传图片信息
     * @param multipartFile
     * @return
     */
    Response uploadImage(MultipartFile multipartFile);

    /**
     * 上传APK文件
     * @param multipartFile
     * @return
     */
    Response uploadApk(MultipartFile multipartFile);
}
