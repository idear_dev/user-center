package com.idear.user.center.console.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.framework.response.Response;
import com.idear.user.center.console.business.MenuBusiness;
import com.idear.user.center.console.converter.MenuConverter;
import com.idear.user.center.console.form.CreateMenuForm;
import com.idear.user.center.console.form.EditMenuForm;
import com.idear.user.center.console.form.QueryMenuForm;
import com.idear.user.center.console.message.ConsoleMessage;
import com.idear.user.center.core.enums.DelFlag;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.core.po.Resource;
import com.idear.user.center.service.MenuService;
import com.idear.user.center.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * create by wenqx on 2017/11/20
 **/
@Component
public class MenuBusinessImpl implements MenuBusiness {

    @Autowired
    private MenuService menuService;

    @Autowired
    private ResourceService resourceService;

    @Override
    public Response createMenu(CreateMenuForm form) {
        //判断是否存在父id
        if(!isExistParentMenu(form.getPid())) {
            throw new BusinessException(ConsoleMessage.NO_PARENT_MENU_EXISTS);
        }
        //判断资源是否存在
        if(!isExistResource(form.getResourceIds())) {
            throw new BusinessException(ConsoleMessage.NO_RESOURCE_EXISTS);
        }
        Menu addMenu = MenuConverter.convertAddMenuForm2Menu(form);
        Menu menu = menuService.queryAllMenuByName(form.getName());
        if(menu == null) {
            menuService.addMenu(addMenu);
        } else if (DelFlag.DELETED.getCode().equals(menu.getDelFlag())) {
            addMenu.setId(menu.getId());
            addMenu.setDelFlag(DelFlag.NORMAL.getCode());
            menuService.editMenu(addMenu);
        } else {
            throw new BusinessException(ConsoleMessage.MENU_IS_EXISTS);
        }
        return Response.SUCCESS;
    }

    @Override
    public Response editMenu(EditMenuForm form) {
        //判断是否存在父id
        if(!isExistParentMenu(form.getPid())) {
            throw new BusinessException(ConsoleMessage.NO_PARENT_MENU_EXISTS);
        }
        //判断资源是否存在
        if(!isExistResource(form.getResourceIds())) {
            throw new BusinessException(ConsoleMessage.NO_RESOURCE_EXISTS);
        }
        Menu menu = MenuConverter.convertEditMenuForm2Menu(form);
        menuService.editMenu(menu);
        return Response.SUCCESS;
    }

    @Override
    public Response deleteMenu(Integer id) {
        menuService.deleteMenu(id);
        return Response.SUCCESS;
    }

    @Override
    public Response queryMenuList(QueryMenuForm form) {
        Menu menu = MenuConverter.convertQueryMenuForm2Menu(form);
        return new Response(menuService.queryMenuList(menu));
    }

    public boolean isExistParentMenu(Integer pid) {
        boolean result = true;
        List<Integer> menuIdList = new ArrayList<>();
        if(pid != null || pid != 0) {
            menuIdList.add(pid);
            List<Menu> menuList = menuService.getMenuListById(menuIdList);
            if(menuList == null || menuList.size() == 0) {
                result = false;
            }
        }
        return result;
    }

    public boolean isExistResource(String resourceIds) {
        boolean result = true;
        if(resourceIds == null) {
            return result;
        }
        String[] resourceArray = resourceIds.split(",");
        List<Integer> resourceIdList = new ArrayList<>();

        for (int i=0;i< resourceArray.length;i++) {
            resourceIdList.add(Integer.parseInt(resourceArray[i]));
        }
        List<Resource> resourceList = resourceService.getResourceListById(resourceIdList);
        if(resourceList == null || resourceList.size() == 0) {
            return false;
        }
        return result;
    }
}
