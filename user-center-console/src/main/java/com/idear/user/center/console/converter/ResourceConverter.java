package com.idear.user.center.console.converter;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.console.form.CreateResourceForm;
import com.idear.user.center.console.form.EditResourceForm;
import com.idear.user.center.console.form.QueryResourceForm;
import com.idear.user.center.console.vo.PageResourceListVO;
import com.idear.user.center.console.vo.ResourceListVO;
import com.idear.user.center.console.vo.ResourceVO;
import com.idear.user.center.core.po.Resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * create by wenqx on 2017/11/20
 **/
public class ResourceConverter {

    public static Resource convertAddResourceForm2Resource(CreateResourceForm form) {
        Date operateDate = new Date();
        Resource resource = new Resource();
        resource.setName(form.getName());
        resource.setUrlPattern(form.getUrlPattern());
        resource.setCreateTime(operateDate);
        resource.setUpdateTime(operateDate);
        return resource;
    }

    public static Resource convertEditResourceForm2Resource(EditResourceForm form) {
        Date operateDate = new Date();
        Resource resource = new Resource();
        resource.setId(form.getId());
        resource.setName(form.getName());
        resource.setUrlPattern(form.getUrlPattern());
        resource.setUpdateTime(operateDate);
        return resource;
    }


    public static Resource convertQueryResourceForm2Resource(QueryResourceForm form) {
        Resource resource = new Resource();
        resource.setName(form.getName());
        return resource;
    }

    public static PageResourceListVO convertPageList2GetResourceListVO(PageList<Resource> pageList) {
        if(pageList == null) {
            return null;
        }
        PageResourceListVO pageResourceListVO = new PageResourceListVO();
        List<ResourceVO> resourceVOList = new ArrayList<>();

        for(Resource resource:pageList) {
            ResourceVO resourceVO = convertResource2ResourceVO(resource);
            resourceVOList.add(resourceVO);
        }
        pageResourceListVO.setResourceList(resourceVOList);
        pageResourceListVO.setTotalNum(pageList.getPaginator().getTotalCount());
        pageResourceListVO.setTotalPage(pageList.getPaginator().getTotalPages());
        return pageResourceListVO;
    }

    public static ResourceVO convertResource2ResourceVO(Resource resource) {
        if(resource == null) {
            return null;
        }
        ResourceVO resourceVO = new ResourceVO();
        resourceVO.setId(resource.getId());
        resourceVO.setName(resource.getName());
        resourceVO.setUrlPattern(resource.getUrlPattern());
        resourceVO.setCreateTime(resource.getCreateTime());
        resourceVO.setUpdateTime(resource.getUpdateTime());
        return resourceVO;
    }


    public static ResourceListVO convertList2GetResourceListVO(List<Resource> list) {
        if(list == null) {
            return null;
        }
        ResourceListVO resourceListVO = new ResourceListVO();
        List<ResourceVO> resourceVOList = new ArrayList<>();

        for(Resource resource:list) {
            ResourceVO resourceVO = convertResource2ResourceVO(resource);
            resourceVOList.add(resourceVO);
        }
        resourceListVO.setResourceList(resourceVOList);
        return resourceListVO;
    }
}
