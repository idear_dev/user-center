/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.common.util.DateUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.TaskDetailBusiness;
import com.idear.user.center.console.form.AddTaskDetailForm;
import com.idear.user.center.console.form.UpdateTaskDetailForm;
import com.idear.user.center.service.dto.TaskPlanDetailDto;
import com.idear.user.center.service.facade.TaskDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/5
 */
@Component
public class TaskDetailBusinessImpl implements TaskDetailBusiness {

    @Autowired
    private TaskDetailService taskDetailService;


    /**
     * 查询任务详情列表
     *
     * @param planId
     * @return
     */
    @Override
    public Response getTaskDetailList(Integer planId) {
        Response<List<TaskPlanDetailDto>> response = taskDetailService.queryTaskDetail(planId);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 添加任务详情
     *
     * @param form
     * @return
     */
    @Override
    public Response addTaskDetail(AddTaskDetailForm form) {
        TaskPlanDetailDto detailDto = new TaskPlanDetailDto();
        detailDto.setPlanId(form.getPlanId());
        detailDto.setChannel(form.getChannel());
        detailDto.setAmount(form.getAmount());
        detailDto.setTotalNum(form.getTotalNum());
        if (StringUtil.isNotEmpty(form.getStartTime())){
            detailDto.setStartTime(DateUtil.parseDate(form.getStartTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            detailDto.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getShowTime())){
            detailDto.setShowTime(DateUtil.parseDate(form.getShowTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        Response<Integer> response = taskDetailService.addTaskDetail(detailDto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新任务信息
     *
     * @param form
     * @return
     */
    @Override
    public Response updateTaskDetail(UpdateTaskDetailForm form) {
        TaskPlanDetailDto detailDto = new TaskPlanDetailDto();
        detailDto.setId(form.getId());
        detailDto.setChannel(form.getChannel());
        detailDto.setAmount(form.getAmount());
        detailDto.setTotalNum(form.getTotalNum());
        if (StringUtil.isNotEmpty(form.getStartTime())){
            detailDto.setStartTime(DateUtil.parseDate(form.getStartTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getEndTime())){
            detailDto.setEndTime(DateUtil.parseDate(form.getEndTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        if (StringUtil.isNotEmpty(form.getShowTime())){
            detailDto.setShowTime(DateUtil.parseDate(form.getShowTime(),DateUtil.FULL_CHINESE_PATTERN));
        }
        detailDto.setStatus(form.getStatus());
        Response response = taskDetailService.updateTaskDetail(detailDto);
        return ResponseUtil.getSimpleResponse(response);
    }
}
