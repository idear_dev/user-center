/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.AppInfoBusiness;
import com.idear.user.center.console.form.AddAppInfoForm;
import com.idear.user.center.console.form.QueryAppInfoForm;
import com.idear.user.center.console.form.UpdateAppInfoForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
@Controller
@RequestMapping(value = "app", method = RequestMethod.POST)
public class AppInfoController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private AppInfoBusiness appInfoBusiness;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public String getAppInfoList(@RequestBody final QueryAppInfoForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return appInfoBusiness.getAppInfoList(form);
            }
        },bindingResult);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addBanner(@RequestBody final AddAppInfoForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return appInfoBusiness.add(form);
            }
        }, bindingResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateBanner(@RequestBody final UpdateAppInfoForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return appInfoBusiness.update(form);
            }
        }, bindingResult);
    }
}
