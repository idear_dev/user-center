/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.sms.service.dto.TemplateDto;
import com.idear.sms.service.facade.SmsTemplateService;
import com.idear.user.center.console.business.SmsTemplateBusiness;
import com.idear.user.center.console.form.AddSmsTemplateForm;
import com.idear.user.center.console.form.UpdateSmsTemplateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
@Component
public class SmsTemplateBusinessImpl implements SmsTemplateBusiness {

    @Autowired
    private SmsTemplateService smsTemplateService;

    /**
     * 添加短信模板信息
     *
     * @param form
     * @return
     */
    @Override
    public Response add(AddSmsTemplateForm form) {
        TemplateDto dto = new TemplateDto();
        dto.setContent(form.getContent());
        dto.setCode(form.getCode());
        Response<Integer> response = smsTemplateService.addTemplate(dto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 删除短信模版
     *
     * @param templateId
     * @return
     */
    @Override
    public Response delete(Integer templateId) {
        Response response = smsTemplateService.deleteTemplate(templateId);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新模版信息
     *
     * @param form
     * @return
     */
    @Override
    public Response update(UpdateSmsTemplateForm form) {
        TemplateDto dto = new TemplateDto();
        dto.setContent(form.getContent());
        dto.setCode(form.getCode());
        dto.setId(form.getId());
        Response response = smsTemplateService.updateTemplate(dto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 查询所有模板信息
     *
     * @return
     */
    @Override
    public Response getTemplateList() {
        Response response = smsTemplateService.listTemplate();
        return ResponseUtil.getSimpleResponse(response);
    }
}
