/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business.impl;

import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.console.business.QuestionBusiness;
import com.idear.user.center.console.form.AddQuestionForm;
import com.idear.user.center.console.form.UpdateQuestionForm;
import com.idear.user.center.core.enums.ConfigStatus;
import com.idear.user.center.service.dto.QuestionDto;
import com.idear.user.center.service.facade.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
@Component
public class QuestionBusinessImpl implements QuestionBusiness {

    @Autowired
    private ConfigService configService;

    /**
     * 获取QA列表
     *
     * @return
     */
    @Override
    public Response getQuestionList() {
        Response<List<QuestionDto>> response = configService.listQuestion(ConfigStatus.ENABLE.getCode());
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 添加QA
     *
     * @param form
     * @return
     */
    @Override
    public Response add(AddQuestionForm form) {
        QuestionDto questionDto = new QuestionDto();
        questionDto.setQuestion(form.getQuestion());
        questionDto.setAnswer(form.getAnswer());
        Response<Integer> response = configService.addQuestion(questionDto);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 删除QA
     *
     * @param questionId
     * @return
     */
    @Override
    public Response delete(Integer questionId) {
        Response response = configService.deleteQuestion(questionId);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 更新QA
     *
     * @param form
     * @return
     */
    @Override
    public Response update(UpdateQuestionForm form) {
        QuestionDto questionDto = new QuestionDto();
        questionDto.setId(form.getId());
        questionDto.setQuestion(form.getQuestion());
        questionDto.setAnswer(form.getAnswer());
        Response response = configService.updateQuestion(questionDto);
        return ResponseUtil.getSimpleResponse(response);
    }
}
