package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class CreateMenuForm {
    /**
     * 父节点Id
     */
    private Integer pid;
    /**
     * 菜单名称
     */
    @NotBlank(message = "name_not_blank")
    private String name;
    /**
     * 资源指向
     */
    private String href;
    /**
     * 资源排序号
     */
    private Integer sort;
    /**
     * 资源id
     */
    private String resourceIds;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }


    public String getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }
}
