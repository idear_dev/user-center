package com.idear.user.center.console.component;

import com.alibaba.fastjson.JSONObject;
import com.idear.common.util.JsonUtil;
import com.idear.user.center.console.bo.Node;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author cc
 * @date 2018-09-24
 */
@Component
public class MenuComponent {

    @Autowired
    private MenuService menuService;

    public JSONObject getMenuInfo(List<Menu> menuList) {
        // 节点列表（散列表，用于临时存储节点对象）
        HashMap nodeList = new HashMap();
        // 根据结果集构造节点列表（存入散列表）
        for (Menu menu : menuList) {
            Menu temp = menu;
            while (true) {
                Node node = new Node();
                node.name = temp.getName();
                node.sort = temp.getSort();
                node.id = temp.getId();
                node.parentId = temp.getPid();
                node.href = temp.getHref();
                nodeList.put(node.id, node);
                if (temp.getPid().equals(0)) {
                    break;
                } else {
                    List<Integer> menuIdList = new ArrayList<>();
                    menuIdList.add(temp.getPid());
                    List<Menu> menus = menuService.getMenuListById(menuIdList);
                    temp = menus.get(0);
                }
            }
        }
        // 构造无序的多叉树
        Node root = new Node();
        Set entrySet = nodeList.entrySet();
        for (Iterator it = entrySet.iterator(); it.hasNext(); ) {
            Node node = (Node) ((Map.Entry) it.next()).getValue();
            if (node.parentId == null || node.parentId.equals(0)) {
                root.addChild(node);
            } else {
                ((Node) nodeList.get(node.parentId)).addChild(node);
            }
        }
        // 对多叉树进行横向排序
        root.sortChildren();
        return JsonUtil.parseJsonObject(root.toString());
    }
}
