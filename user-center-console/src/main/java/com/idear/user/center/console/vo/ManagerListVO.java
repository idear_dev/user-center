package com.idear.user.center.console.vo;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/11/7
 */
public class ManagerListVO {

    private List<Map<String, Object>> managerList;

    public List<Map<String, Object>> getManagerList() {
        return managerList;
    }

    public void setManagerList(List<Map<String, Object>> managerList) {
        this.managerList = managerList;
    }
}
