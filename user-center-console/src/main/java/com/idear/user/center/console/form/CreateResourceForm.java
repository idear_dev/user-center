package com.idear.user.center.console.form;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public class CreateResourceForm {

    /**
     * 资源名称
     */
    @NotBlank(message = "name_not_blank")
    private String name;
    /**
     * 资源url
     */
    private String urlPattern;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }
}
