package com.idear.user.center.console.controller;

import com.idear.common.util.JsonUtil;
import com.idear.common.util.ValidateUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.console.business.RoleBusiness;
import com.idear.user.center.console.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @author cc
 * @date 2018-09-21
 */
@Controller
@RequestMapping(value = "role", method = RequestMethod.POST)
public class RoleController {

    @Autowired
    private RoleBusiness roleBusiness;

    @Autowired
    private ControllerTemplate controllerTemplate;

    /**
     * 获取角色列表
     *
     * @param getRoleListForm
     * @return
     */
    @RequestMapping(value = "list")
    @ResponseBody
    public String getRoleList(@Valid @RequestBody final GetRoleListForm getRoleListForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer pageNo = getRoleListForm.getPageNo();
                Integer pageSize = getRoleListForm.getPageSize();
                return roleBusiness.pageableRoleList(pageNo, pageSize);
            }
        }, bindingResult);
    }

    /**
     * 新建角色
     *
     * @param createRoleForm
     * @return
     */
    @RequestMapping(value = "create")
    @ResponseBody
    public String createRole(@Valid @RequestBody final CreateRoleForm createRoleForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return roleBusiness.createRole(createRoleForm);
            }
        }, bindingResult);
    }

    /**
     * 查看角色信息
     *
     * @param getRoleForm
     * @return
     */
    @RequestMapping(value = "get")
    @ResponseBody
    public String getRole(@Valid @RequestBody final GetRoleForm getRoleForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer roleId = getRoleForm.getRoleId();
                return roleBusiness.getRole(roleId);
            }
        }, bindingResult);
    }

    /**
     * 编辑角色信息
     *
     * @param editRoleForm
     * @return
     */
    @RequestMapping(value = "edit")
    @ResponseBody
    public String editRole(@Valid @RequestBody final EditRoleForm editRoleForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return roleBusiness.editRole(editRoleForm);
            }
        }, bindingResult);
    }

    /**
     * 编辑分组信息
     *
     * @param editGroupForm
     * @return
     */
    @RequestMapping(value = "group/edit")
    @ResponseBody
    public String editGroup(@Valid @RequestBody final EditGroupForm editGroupForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer groupId = editGroupForm.getGroupId();
                String groupName = editGroupForm.getGroupName();
                return roleBusiness.editGroup(groupId, groupName);
            }
        }, bindingResult);
    }

    /**
     * 创建分组信息
     *
     * @param createGroupForm
     * @return
     */
    @RequestMapping(value = "group/create")
    @ResponseBody
    public String createGroup(@Valid @RequestBody final CreateGroupForm createGroupForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String groupName = createGroupForm.getGroupName();
                return roleBusiness.createGroup(groupName, groupName);
            }
        }, bindingResult);
    }

    /**
     * 删除分组
     *
     * @param deleteGroupForm
     * @return
     */
    @RequestMapping(value = "group/delete")
    @ResponseBody
    public String deleteGroup(@Valid @RequestBody final DeleteGroupForm deleteGroupForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer groupId = deleteGroupForm.getGroupId();
                return roleBusiness.deleteGroup(groupId);
            }
        }, bindingResult);
    }

    /**
     * 获取分组列表
     *
     * @return
     */
    @RequestMapping(value = "group/list")
    @ResponseBody
    public String getGroupList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return roleBusiness.getGroupList();
            }
        });
    }

    /**
     * 获取菜单列表
     *
     * @return
     */
    @RequestMapping(value = "menu/list")
    @ResponseBody
    public String getMenuList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return roleBusiness.getMenuList();
            }
        });
    }


    /**
     * 获取角色列表
     *
     * @return
     */
    @RequestMapping(value = "all/list")
    @ResponseBody
    public String getRoleList() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return roleBusiness.getRoleList();
            }
        });
    }
}
