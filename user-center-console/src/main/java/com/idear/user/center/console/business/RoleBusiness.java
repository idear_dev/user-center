package com.idear.user.center.console.business;


import com.idear.framework.response.Response;
import com.idear.user.center.console.form.CreateRoleForm;
import com.idear.user.center.console.form.EditRoleForm;

/**
 * @author cc
 * @date 2018-09-21
 */
public interface RoleBusiness {

    Response pageableRoleList(Integer pageNo, Integer pageSize);

    Response createGroup(String groupName, String groupCode);

    Response deleteGroup(Integer groupId);

    Response createRole(CreateRoleForm createRoleForm);

    Response getRole(Integer roleId);

    Response editGroup(Integer groupId, String groupName);

    Response editRole(EditRoleForm editRoleForm);

    Response getGroupList();

    Response getMenuList();

    Response getRoleList();
}
