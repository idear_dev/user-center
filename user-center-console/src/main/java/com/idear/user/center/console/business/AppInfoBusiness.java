/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddAppInfoForm;
import com.idear.user.center.console.form.QueryAppInfoForm;
import com.idear.user.center.console.form.UpdateAppInfoForm;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public interface AppInfoBusiness {

    /**
     * 获取所有应用信息
     * @param form
     * @return
     */
    Response getAppInfoList(QueryAppInfoForm form);

    /**
     * 添加应用信息
     * @param form
     * @return
     */
    Response add(AddAppInfoForm form);

    /**
     * 更新应用信息
     * @param form
     * @return
     */
    Response update(UpdateAppInfoForm form);
}
