package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.bo.ManagerInfo;
import com.idear.user.center.console.form.GetManagerListForm;

/**
 * @author cc
 * @date 2018-09-21
 */
public interface ManagerBusiness {

    /**
     * 检查token有效性
     * @param token
     * @return
     */
    ManagerInfo checkToken(String token);

    /**
     * 登录
     * @param email
     * @param password
     * @return
     */
    Response login(String email,String password);

    /**
     * token信息
     * @param token
     * @return
     */
    Response logout(String token);

    /**
     * 获取管理员信息
     * @return
     */
    Response getManagerInfo();

    /**
     * 创建管理员
     * @param email
     * @param roleIds
     * @return
     */
    Response createManager(String email, String roleIds);

    /**
     * 获取管理员列表
     * @param getManagerListForm
     * @return
     */
    Response getManagerList(GetManagerListForm getManagerListForm);

    /**
     * 编辑管理员
     * @param id
     * @param roleIds
     * @return
     */
    Response editManager(Integer id, String roleIds);

    /**
     * 删除管理员
     * @param id
     * @return
     */
    Response deleteManager(Integer id);

    /**
     * 关闭用户
     * @param id
     * @return
     */
    Response closeManager(Integer id);

    /**
     * 开启用户
     * @param id
     * @return
     */
    Response openManager(Integer id);

}
