package com.idear.user.center.console.converter;

import com.idear.user.center.console.form.CreateMenuForm;
import com.idear.user.center.console.form.EditMenuForm;
import com.idear.user.center.console.form.QueryMenuForm;
import com.idear.user.center.core.enums.DelFlag;
import com.idear.user.center.core.po.Menu;

import java.util.Date;

/**
 * create by wenqx on 2017/11/20
 **/
public class MenuConverter {

    public static Menu convertAddMenuForm2Menu(CreateMenuForm form) {
        Date operateDate = new Date();
        Menu menu = new Menu();
        menu.setDelFlag(DelFlag.NORMAL.getCode());
        menu.setHref(form.getHref());
        menu.setPid(form.getPid());
        menu.setName(form.getName());
        menu.setSort(form.getSort());
        menu.setResourceIds(form.getResourceIds());
        menu.setCreateTime(operateDate);
        menu.setUpdateTime(operateDate);
        return menu;
    }

    public static Menu convertEditMenuForm2Menu(EditMenuForm form) {
        Date operateDate = new Date();
        Menu menu = new Menu();
        menu.setId(form.getId());
        menu.setPid(form.getPid());
        menu.setName(form.getName());
        menu.setResourceIds(form.getResourceIds());
        menu.setHref(form.getHref());
        menu.setSort(form.getSort());
        menu.setPid(form.getPid());
        menu.setUpdateTime(operateDate);
        return menu;
    }

    public static Menu convertQueryMenuForm2Menu(QueryMenuForm form) {
        Menu menu = new Menu();
        menu.setName(form.getName());
        return menu;
    }
}
