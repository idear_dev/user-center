/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.QuerySmsHistoryForm;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
public interface SmsHistoryBusiness {

    /**
     * 获取短信记录信息
     * @param form
     * @return
     */
    Response getSmsHistoryList(QuerySmsHistoryForm form);
}
