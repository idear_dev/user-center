/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.console.business;

import com.idear.framework.response.Response;
import com.idear.user.center.console.form.AddTaskPlanForm;
import com.idear.user.center.console.form.QueryPlanForm;
import com.idear.user.center.console.form.UpdateTaskPlanForm;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
public interface TaskPlanBusiness {

    /**
     * 查询任务计划
     * @param form
     * @return
     */
    Response getTaskPlanList(QueryPlanForm form);

    /**
     * 添加任务计划
     * @param form
     * @return
     */
    Response addTaskPlan(AddTaskPlanForm form);

    /**
     * 更新任务计划
     * @param form
     * @return
     */
    Response updateTaskPlan(UpdateTaskPlanForm form);
}
