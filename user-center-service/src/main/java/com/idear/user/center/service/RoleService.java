package com.idear.user.center.service;


import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.Role;

import java.util.List;

/**
 * @author cc
 * @date 2018-09-21
 */
public interface RoleService {

    PageList<Role> pageableRoleList(Integer pageNo, Integer pageSize);

    List<Role> getRoleListById(List<Integer> roleIdList);

    void createRole(Role role);

    void updateRole(Role role);

    List<Role> getRoleListByGroupId(Integer groupId);

    List<Role> getRoleList();
}
