/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.PageQueryUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.enums.ConfigStatus;
import com.idear.user.center.core.enums.QuestionStatus;
import com.idear.user.center.core.mapper.ClientVersionConfigMapper;
import com.idear.user.center.core.mapper.QuestionMapper;
import com.idear.user.center.core.po.ClientVersionConfig;
import com.idear.user.center.core.po.Question;
import com.idear.user.center.service.dto.ClientVersionDto;
import com.idear.user.center.service.dto.ClientVersionListDto;
import com.idear.user.center.service.dto.QuestionDto;
import com.idear.user.center.service.facade.ConfigService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author CcShan
 * @date 2018/8/28
 */
@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Resource
    private ClientVersionConfigMapper clientVersionConfigMapper;

    @Resource
    private QuestionMapper questionMapper;

    /**
     * 查询比当前版本还要新的客户端版本信息
     *
     * @param channel
     * @param version
     * @return
     */
    @Override
    public Response<List<ClientVersionDto>> listNewClientVersion(final String channel, final Integer version) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                List<ClientVersionDto> dtoList = new ArrayList<>();
                Map<String,String> param = new HashMap<>(16);
                param.put("channel",channel);
                List<ClientVersionConfig> configList = clientVersionConfigMapper.getConfigListByChannel(param);
                if (configList != null && !configList.isEmpty()) {
                    Iterator<ClientVersionConfig> iterator = configList.iterator();
                    while (iterator.hasNext()) {
                        ClientVersionConfig config = iterator.next();
                        if (config.getVersion() >= version) {
                            dtoList.add(convert2Dto(config));
                        }
                    }
                }
                return new Response<>(dtoList);
            }
        });
    }

    /**
     * 分页查询所有客户端版本信息
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<ClientVersionListDto> listClientVersion(Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = PageQueryUtil.DEFAULT_PAGE_NO;
        }
        if (pageSize == null || pageSize > PageQueryUtil.DEFAULT_MAX_PAGE_SIZE) {
            pageSize = PageQueryUtil.DEFAULT_PAGE_SIZE;
        }
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        PageList<ClientVersionConfig> pageList = clientVersionConfigMapper.getConfigList(pageBounds);
        if (pageList != null || !pageList.isEmpty()) {
            ClientVersionListDto clientVersionListDto = new ClientVersionListDto();
            Integer totalNum = pageList.getPaginator().getTotalCount();
            Integer totalPage = pageList.getPaginator().getTotalPages();
            clientVersionListDto.setTotalNum(totalNum);
            clientVersionListDto.setTotalPage(totalPage);
            clientVersionListDto.setClientVersionList(convert2DtoList(pageList));
            return new Response(clientVersionListDto);
        } else {
            return Response.SUCCESS;
        }
    }

    /**
     * 添加版本信息
     *
     * @param clientVersionDto
     * @return
     */
    @Override
    public Response<Integer> addClientVersion(final ClientVersionDto clientVersionDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                ClientVersionConfig config = new ClientVersionConfig();
                config.setChannel(clientVersionDto.getChannel());
                config.setDescription(clientVersionDto.getDescription());
                config.setDownloadUrl(clientVersionDto.getDownloadUrl());
                config.setIsForceUpdate(Boolean.TRUE.equals(clientVersionDto.getForceUpdate()) ? 1 : 0);
                config.setStatus(ConfigStatus.ENABLE.getCode());
                config.setVersion(clientVersionDto.getVersion());
                clientVersionConfigMapper.saveConfig(config);
                return new Response(config.getId());
            }
        });
    }

    /**
     * 更新版本信息
     *
     * @param clientVersionDto
     * @return
     */
    @Override
    public Response updateClientVersion(final ClientVersionDto clientVersionDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer versionId = clientVersionDto.getId();
                if (versionId == null) {
                    throw new BusinessException(ServiceMessage.VERSION_ID_IS_NULL);
                }
                ClientVersionConfig config = clientVersionConfigMapper.getConfig(versionId);
                if (config == null) {
                    throw new BusinessException(ServiceMessage.VERSION_INFO_NOT_EXIST);
                }
                if (StringUtil.isNotEmpty(clientVersionDto.getChannel())) {
                    config.setChannel(clientVersionDto.getChannel());
                }
                if (StringUtil.isNotEmpty(clientVersionDto.getDescription())) {
                    config.setDescription(clientVersionDto.getDescription());
                }
                if (StringUtil.isNotEmpty(clientVersionDto.getDownloadUrl())) {
                    config.setDownloadUrl(clientVersionDto.getDownloadUrl());
                }
                if (clientVersionDto.getForceUpdate() != null) {
                    config.setIsForceUpdate(Boolean.TRUE.equals(clientVersionDto.getForceUpdate()) ? 1 : 0);
                }
                if (clientVersionDto.getVersion() != null){
                    config.setVersion(clientVersionDto.getVersion());
                }
                if (clientVersionDto.getStatus() != null){
                    config.setStatus(clientVersionDto.getStatus());
                }
                clientVersionConfigMapper.updateConfig(config);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 更新版本信息
     *
     * @param versionId
     * @return
     */
    @Override
    public Response deleteClientVersion(final Integer versionId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                if (versionId == null) {
                    throw new BusinessException(ServiceMessage.VERSION_ID_IS_NULL);
                }
                ClientVersionConfig config = clientVersionConfigMapper.getConfig(versionId);
                if (config == null) {
                    throw new BusinessException(ServiceMessage.VERSION_INFO_NOT_EXIST);
                }
                config.setStatus(ConfigStatus.DISABLE.getCode());
                clientVersionConfigMapper.updateConfig(config);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 查看所有的QA信息
     *
     * @param status
     * @return
     */
    @Override
    public Response<List<QuestionDto>> listQuestion(final Integer status) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Question param = new Question();
                param.setStatus(status);
                List<Question> questionList = questionMapper.getQuestionListByStatus(param);
                List<QuestionDto> dtoList = new ArrayList<>();
                for (Question question : questionList) {
                    QuestionDto questionDto = new QuestionDto();
                    questionDto.setQuestion(question.getQuestion());
                    questionDto.setId(question.getId());
                    questionDto.setAnswer(question.getAnswer());
                    questionDto.setCreateTime(question.getCreateTime());
                    questionDto.setUpdateTime(question.getUpdateTime());
                    dtoList.add(questionDto);
                }
                return new Response(dtoList);
            }
        });
    }

    /**
     * 添加QA信息
     *
     * @param questionDto
     * @return
     */
    @Override
    public Response<Integer> addQuestion(final QuestionDto questionDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Question question = new Question();
                question.setStatus(QuestionStatus.ENABLE.getCode());
                question.setQuestion(questionDto.getQuestion());
                question.setAnswer(questionDto.getAnswer());
                questionMapper.saveQuestion(question);
                return new Response(question.getId());
            }
        });
    }

    /**
     * 更新QA信息
     *
     * @param questionDto
     * @return
     */
    @Override
    public Response updateQuestion(final QuestionDto questionDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer questionId = questionDto.getId();
                if (questionId == null){
                    throw new BusinessException(ServiceMessage.QUESTION_ID_IS_NULL);
                }
                Question question = questionMapper.getQuestion(questionId);
                if (question == null){
                    throw new BusinessException(ServiceMessage.QUESTION_INFO_NOT_EXIST);
                }
                if (StringUtil.isNotEmpty(questionDto.getQuestion())){
                    question.setQuestion(questionDto.getQuestion());
                }
                if (StringUtil.isNotEmpty(questionDto.getAnswer())){
                    question.setAnswer(questionDto.getAnswer());
                }
                questionMapper.updateQuestion(question);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 删除QA信息
     *
     * @param questionId
     * @return
     */
    @Override
    public Response deleteQuestion(final Integer questionId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                if (questionId == null){
                    throw new BusinessException(ServiceMessage.QUESTION_ID_IS_NULL);
                }
                Question question = questionMapper.getQuestion(questionId);
                if (question == null){
                    throw new BusinessException(ServiceMessage.QUESTION_INFO_NOT_EXIST);
                }
                question.setStatus(QuestionStatus.DISABLE.getCode());
                questionMapper.updateQuestion(question);
                return Response.SUCCESS;
            }
        });
    }

    private List<ClientVersionDto> convert2DtoList(List<ClientVersionConfig> configList) {
        List<ClientVersionDto> dtoList = new ArrayList<>();
        for (ClientVersionConfig config : configList) {
            dtoList.add(convert2Dto(config));
        }
        return dtoList;
    }

    private ClientVersionDto convert2Dto(ClientVersionConfig clientVersionConfig) {
        ClientVersionDto clientVersionDto = new ClientVersionDto();
        clientVersionDto.setChannel(clientVersionConfig.getChannel());
        clientVersionDto.setDescription(clientVersionConfig.getDescription());
        clientVersionDto.setDownloadUrl(clientVersionConfig.getDownloadUrl());
        clientVersionDto.setId(clientVersionConfig.getId());
        clientVersionDto.setForceUpdate(clientVersionConfig.getIsForceUpdate() == 1 ? true : false);
        clientVersionDto.setStatus(clientVersionConfig.getStatus());
        clientVersionDto.setVersion(clientVersionConfig.getVersion());
        clientVersionDto.setCreateTime(clientVersionConfig.getCreateTime());
        clientVersionDto.setUpdateTime(clientVersionConfig.getUpdateTime());
        return clientVersionDto;
    }
}
