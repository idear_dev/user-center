package com.idear.user.center.service;


import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.Manager;

import java.util.List;


/**
 *
 * @author CcShan
 * @date 2017/9/21
 */
public interface ManagerService {

    /**
     * 根据邮箱获取管理员信息
     * @param email
     * @return
     */
    Manager getManagerByEmail(String email);

    /**
     * 更新管理员信息
     * @param manager
     * @return
     */
    void updateManager(Manager manager);

    /**
     * 添加管理员
     * @param manager
     */
    void addManager(Manager manager);

    /**
     * 获取管理员列表
     * @param name
     * @param roleId
     * @param pageBounds
     * @return
     */
    PageList<Manager> getManagerList(String name, Integer roleId, PageBounds pageBounds);

    /**
     * 根据角色列表查询用户信息
     * @param roleIdList
     * @return
     */
    List<Manager> getManagerListByRoleIds(List<Integer> roleIdList);

    /**
     * 查询所有的 包括删除的
     * @param email
     * @return
     */
    Manager getAllManagerByEmail(String email);

    /**
     * 根据Id查询管理员信息
     * @param id
     * @return
     */
    Manager getManagerById(Integer id);
}
