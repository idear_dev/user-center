/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.PageQueryUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.mapper.AppInfoMapper;
import com.idear.user.center.core.po.AppInfo;
import com.idear.user.center.service.dto.AppInfoDto;
import com.idear.user.center.service.dto.AppInfoListDto;
import com.idear.user.center.service.facade.AppInfoService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
@Service
public class AppInfoServiceImpl implements AppInfoService {

    @Resource
    private BusinessTemplate businessTemplate;

    @Resource
    private AppInfoMapper appInfoMapper;

    /**
     * 模糊查询App信息
     *
     * @param appInfoDto
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<AppInfoListDto> listAppInfo(AppInfoDto appInfoDto, Integer pageNo, Integer pageSize) {
        AppInfo param = new AppInfo();
        param.setAppName(appInfoDto.getAppName());
        if (pageNo == null) {
            pageNo = PageQueryUtil.DEFAULT_PAGE_NO;
        }
        if (pageSize == null || pageSize > PageQueryUtil.DEFAULT_MAX_PAGE_SIZE) {
            pageSize = PageQueryUtil.DEFAULT_PAGE_SIZE;
        }
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        PageList<AppInfo> pageList = appInfoMapper.getAppInfoList(param, pageBounds);
        if (pageList != null || !pageList.isEmpty()) {
            AppInfoListDto appInfoListDto = new AppInfoListDto();
            Integer totalNum = pageList.getPaginator().getTotalCount();
            Integer totalPage = pageList.getPaginator().getTotalPages();
            appInfoListDto.setTotalNum(totalNum);
            appInfoListDto.setTotalPage(totalPage);
            appInfoListDto.setAppInfoList(convert2DtoList(pageList));
            return new Response(appInfoListDto);
        } else {
            return Response.SUCCESS;
        }
    }

    /**
     * 添加App信息
     *
     * @param appInfoDto
     * @return
     */
    @Override
    public Response<Integer> addAppInfo(final AppInfoDto appInfoDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                AppInfo appInfo = new AppInfo();
                appInfo.setAppName(appInfoDto.getAppName());
                appInfo.setAppDescription(appInfoDto.getAppDescription());
                appInfo.setAppLogoUrl(appInfoDto.getAppLogoUrl());
                appInfo.setCompany(appInfoDto.getCompany());
                appInfo.setPackageName(appInfoDto.getPackageName());
                appInfo.setPhoneNumber(appInfoDto.getPhoneNumber());
                appInfoMapper.saveAppInfo(appInfo);
                return new Response(appInfo.getId());
            }
        });
    }

    /**
     * 更新banner信息
     *
     * @param appInfoDto
     * @return
     */
    @Override
    public Response updateAppInfo(final AppInfoDto appInfoDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer appId = appInfoDto.getId();
                if (appId == null) {
                    throw new BusinessException(ServiceMessage.APP_ID_IS_NULL);
                }
                AppInfo appInfo = appInfoMapper.getAppInfoById(appInfoDto.getId());
                if (appInfo == null) {
                    throw new BusinessException(ServiceMessage.APP_INFO_NOT_EXIST);
                }
                if (StringUtil.isNotEmpty(appInfoDto.getAppDescription())) {
                    appInfo.setAppDescription(appInfoDto.getAppDescription());
                }
                if (StringUtil.isNotEmpty(appInfoDto.getAppName())) {
                    appInfo.setAppName(appInfoDto.getAppName());
                }
                if (StringUtil.isNotEmpty(appInfoDto.getAppLogoUrl())) {
                    appInfo.setAppLogoUrl(appInfoDto.getAppLogoUrl());
                }
                if (StringUtil.isNotEmpty(appInfoDto.getCompany())) {
                    appInfo.setCompany(appInfoDto.getCompany());
                }
                if (StringUtil.isNotEmpty(appInfoDto.getPackageName())) {
                    appInfo.setPackageName(appInfoDto.getPackageName());
                }
                if (StringUtil.isNotEmpty(appInfoDto.getPhoneNumber())) {
                    appInfo.setPhoneNumber(appInfoDto.getPhoneNumber());
                }
                appInfoMapper.updateAppInfo(appInfo);
                return Response.SUCCESS;
            }
        });
    }

    private AppInfoDto convert2Dto(AppInfo appInfo) {
        AppInfoDto appInfoDto = new AppInfoDto();
        appInfoDto.setId(appInfo.getId());
        appInfoDto.setAppName(appInfo.getAppName());
        appInfoDto.setAppDescription(appInfo.getAppDescription());
        appInfoDto.setAppLogoUrl(appInfo.getAppLogoUrl());
        appInfoDto.setCompany(appInfo.getCompany());
        appInfoDto.setPackageName(appInfo.getPackageName());
        appInfoDto.setPhoneNumber(appInfo.getPhoneNumber());
        appInfoDto.setCreateTime(appInfo.getCreateTime());
        appInfoDto.setUpdateTime(appInfo.getUpdateTime());
        return appInfoDto;
    }

    private List<AppInfoDto> convert2DtoList(List<AppInfo> bannerList) {
        List<AppInfoDto> dtoList = new ArrayList<>();
        if (bannerList != null && !bannerList.isEmpty()) {
            Iterator<AppInfo> iterator = bannerList.iterator();
            while (iterator.hasNext()) {
                AppInfo appInfo = iterator.next();
                dtoList.add(convert2Dto(appInfo));
            }
        }
        return dtoList;
    }
}
