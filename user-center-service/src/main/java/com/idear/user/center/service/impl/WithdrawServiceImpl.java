/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.DateUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.PageQueryUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.dao.AccountRecordDao;
import com.idear.user.center.core.enums.*;
import com.idear.user.center.core.mapper.*;
import com.idear.user.center.core.po.*;
import com.idear.user.center.service.dto.WithdrawDetailDto;
import com.idear.user.center.service.dto.WithdrawDetailListDto;
import com.idear.user.center.service.facade.WithdrawService;
import com.idear.user.center.service.message.ServiceMessage;
import com.idear.user.center.service.param.WithdrawListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created By CcShan on 2018/1/3.
 * @author Administrator
 */
@Service
public class WithdrawServiceImpl implements WithdrawService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Autowired
    private AccountRecordDao accountRecordDao;

    @Resource
    private AccountMapper accountMapper;

    @Resource
    private MemberMapper memberMapper;

    @Resource
    private AccountRecordMapper accountRecordMapper;

    @Resource
    private AccountWithdrawRecordMapper accountWithdrawRecordMapper;

    @Resource
    private WechatMapper wechatMapper;

    /**
     * 最少提现金额
     */
    private static final Integer LOWER_APPLY_AMOUNT = 20000;

    /**
     * 获取提现记录明细
     *
     * @param serialNo
     * @param memberId
     * @return
     */
    @Override
    public Response<WithdrawDetailDto> getWithdrawDetail(Integer memberId, String serialNo) {
        AccountRecord accountRecord = accountRecordDao.queryAccountRecord(memberId, serialNo);
        AccountWithdrawRecord accountWithdrawRecord = accountWithdrawRecordMapper.queryRecord(serialNo);
        if (accountRecord == null || accountWithdrawRecord == null) {
            throw new BusinessException(ServiceMessage.QUERY_WITHDRAW_DETAIL_FAILED);
        } else {
            WithdrawDetailDto withdrawDetailDto = convert2WithdrawDetailDto(accountWithdrawRecord);
            return new Response<>(withdrawDetailDto);
        }
    }

    /**
     * 提现申请
     *
     * @param memberId
     * @param amount
     * @return
     */
    @Override
    public Response<String> apply(final Integer memberId, final Integer amount) {
        return businessTemplate.execute(new Action() {
            @Override
            @Transactional(rollbackFor = Exception.class)
            public Response execute() {
                Wechat wechat = wechatMapper.getWechatByMemberId(memberId);
                if (wechat == null){
                    throw new BusinessException(ServiceMessage.WECHAT_NOT_BIND);
                }
                if (amount < LOWER_APPLY_AMOUNT){
                    throw new BusinessException(ServiceMessage.APPLY_AMOUNT_IS_ILLEGAL);
                }
                Integer canWithdrawAmount = accountRecordDao.sumCanWithdrawAmount(memberId);
                if (amount > canWithdrawAmount){
                    throw new BusinessException(ServiceMessage.APPLY_AMOUNT_IS_OVER);
                }
                Member member = memberMapper.getMemberById(memberId);
                Account account = accountMapper.getAccountByMemberId(memberId);
                String serialNo = outAccountRecord(account,amount);
                Date nowDate = new Date();
                AccountWithdrawRecord accountWithdrawRecord = new AccountWithdrawRecord();
                accountWithdrawRecord.setAccountInfo(JsonUtil.toJsonString(account));
                accountWithdrawRecord.setAmount(amount);
                accountWithdrawRecord.setMobileNo(member.getMobileNo());
                accountWithdrawRecord.setMemberId(memberId);
                accountWithdrawRecord.setName(member.getNickname());
                accountWithdrawRecord.setRemark("提现申请");
                accountWithdrawRecord.setSerialNo(serialNo);
                accountWithdrawRecord.setWithdrawType(WithdrawType.WECHAT.getCode());
                accountWithdrawRecord.setStatus(WithdrawStatus.AUDITING.getCode());
                accountWithdrawRecord.setCreateTime(nowDate);
                accountWithdrawRecord.setUpdateTime(nowDate);
                accountWithdrawRecordMapper.saveRecord(accountWithdrawRecord);
                return new Response(serialNo);
            }
        });
    }

    /**
     * 获取提现申请状态信息
     *
     * @return
     */
    @Override
    public Response<List<Map<String, Object>>> listStatus() {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                WithdrawStatus[] statusArray = WithdrawStatus.values();
                List<Map<String, Object>> result = new ArrayList<>();
                for (WithdrawStatus status : statusArray) {
                    Map<String, Object> temp = new HashMap<>(2);
                    temp.put("code", status.getCode());
                    temp.put("desc", status.getDesc());
                    result.add(temp);
                }
                return new Response<>(result);
            }
        });
    }

    /**
     * 查询提现记录列表
     *
     * @param param
     * @return
     */
    @Override
    public Response<WithdrawDetailListDto> getWithdrawDetailList(final WithdrawListParam param) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer pageNo = param.getPageNo();
                if (pageNo == null) {
                    pageNo = PageQueryUtil.DEFAULT_PAGE_NO;
                }
                Integer pageSize = param.getPageSize();
                if (pageSize == null) {
                    pageSize = PageQueryUtil.DEFAULT_PAGE_SIZE;
                }
                if (pageSize > PageQueryUtil.DEFAULT_MAX_PAGE_SIZE) {
                    pageSize = PageQueryUtil.DEFAULT_MAX_PAGE_SIZE;
                }
                PageBounds pageBounds = new PageBounds(pageNo, pageSize);
                Map<String, Object> queryParam = new HashMap<>(16);
                queryParam.put("beginTime", param.getBeginTime());
                queryParam.put("endTime", param.getEndTime());
                queryParam.put("status", param.getStatus());
                queryParam.put("mobileNo", param.getMobileNo());
                PageList<AccountWithdrawRecord> recordList = accountWithdrawRecordMapper.getRecordList(queryParam, pageBounds);
                return new Response(convert2RecordListDto(recordList));
            }
        });
    }


    /**
     * 提现记录
     * @param account
     * @param amount
     */
    private String outAccountRecord(Account account,Integer amount){
        Date nowDate = new Date();
        Integer accountId = account.getId();
        Integer memberId = account.getMemberId();
        AccountRecord accountRecord = new AccountRecord();
        accountRecord.setAccountId(accountId);
        accountRecord.setMemberId(memberId);
        accountRecord.setAmount(amount);
        String serialNo = DateUtil.getCurrentDate(DateUtil.FULL_PATTERN)+System.currentTimeMillis();
        accountRecord.setSerialNo(serialNo);
        accountRecord.setInOutFlag(AccountRecordFlag.OUT.getCode());
        accountRecord.setRecordType(AccountRecordType.WITHDRAW.getCode());
        accountRecord.setRemark("提现");
        accountRecord.setRecordTime(nowDate);
        accountRecord.setCreateTime(nowDate);
        accountRecord.setUpdateTime(nowDate);
        accountRecord.setStatus(AccountRecordStatus.OUT_ING.getCode());
        accountRecordMapper.saveAccountRecord(accountRecord);
        return serialNo;
    }

    /**
     * 类型转换
     * @param withdrawRecord
     * @return
     */
    private WithdrawDetailDto convert2WithdrawDetailDto(AccountWithdrawRecord withdrawRecord) {
        WithdrawDetailDto withdrawDetailDto = new WithdrawDetailDto();
        withdrawDetailDto.setSerialNo(withdrawRecord.getSerialNo());
        withdrawDetailDto.setAmount(withdrawRecord.getAmount());
        withdrawDetailDto.setWithdrawType(withdrawRecord.getWithdrawType());
        withdrawDetailDto.setAccountInfo(withdrawRecord.getAccountInfo());
        withdrawDetailDto.setMobileNo(withdrawRecord.getMobileNo());
        withdrawDetailDto.setName(withdrawRecord.getName());
        withdrawDetailDto.setRemark(withdrawRecord.getRemark());
        withdrawDetailDto.setApplyTime(withdrawRecord.getCreateTime());
        withdrawDetailDto.setStatus(withdrawRecord.getStatus());
        withdrawDetailDto.setUpdateTime(withdrawRecord.getUpdateTime());
        return withdrawDetailDto;
    }

    /**
     * 类型转换
     *
     * @param recordList
     * @return
     */
    private WithdrawDetailListDto convert2RecordListDto(PageList<AccountWithdrawRecord> recordList) {
        WithdrawDetailListDto withdrawDetailListDto = new WithdrawDetailListDto();
        Integer totalNum = recordList.getPaginator().getTotalCount();
        Integer totalPage = recordList.getPaginator().getTotalPages();
        withdrawDetailListDto.setTotalNum(totalNum);
        withdrawDetailListDto.setTotalPage(totalPage);
        List<WithdrawDetailDto> withdrawDetailDtoList = new ArrayList<>();
        if (!recordList.isEmpty()) {
            for (AccountWithdrawRecord accountWithdrawRecord : recordList) {
                WithdrawDetailDto withdrawDetailDto = convert2WithdrawDetailDto(accountWithdrawRecord);
                withdrawDetailDtoList.add(withdrawDetailDto);
            }
        }
        withdrawDetailListDto.setWithdrawDetailList(withdrawDetailDtoList);
        return withdrawDetailListDto;
    }

}
