/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
@Component
public class MemberConfig {

    /**
     * token失效延迟时间
     */
    @Value("${token.delay.time}")
    private String tokenDelayTime;

    public Long getTokenDelayTime() {
        return Long.valueOf(tokenDelayTime);
    }

    public void setTokenDelayTime(String tokenDelayTime) {
        this.tokenDelayTime = tokenDelayTime;
    }
}
