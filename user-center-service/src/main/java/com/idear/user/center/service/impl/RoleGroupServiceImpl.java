package com.idear.user.center.service.impl;

import com.idear.user.center.core.enums.RoleGroupStatus;
import com.idear.user.center.core.mapper.RoleGroupMapper;
import com.idear.user.center.core.po.RoleGroup;
import com.idear.user.center.service.RoleGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2017/9/26
 */
@Service
public class RoleGroupServiceImpl implements RoleGroupService {

    @Autowired
    private RoleGroupMapper roleGroupMapper;

    @Override
    public void addRoleGroup(RoleGroup roleGroup) {
        roleGroupMapper.addRoleGroup(roleGroup);
    }

    @Override
    public RoleGroup getRoleGroupByName(String groupName) {
        return roleGroupMapper.getRoleGroupByName(groupName);
    }

    @Override
    public void updateRoleGroupStatus(Integer roleGroupId, RoleGroupStatus roleGroupStatus) {
        Integer status = roleGroupStatus.getStatus();
        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        map.put("id", roleGroupId);
        map.put("updateTime", new Date());
        roleGroupMapper.updateGroupStatus(map);
    }

    @Override
    public void updateRoleGroupName(Integer roleGroupId, String roleGroupName) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", roleGroupName);
        map.put("id", roleGroupId);
        map.put("updateTime", new Date());
        roleGroupMapper.updateGroupName(map);
    }

    @Override
    public List<RoleGroup> getRoleGroupByStatus(RoleGroupStatus roleGroupStatus) {
        Map<String, Object> map = new HashMap<>();
        if (roleGroupStatus != null) {
            map.put("status", roleGroupStatus.getStatus());
        } else {
            map.put("status", null);
        }
        return roleGroupMapper.getRoleGroupByStatus(map);
    }

}
