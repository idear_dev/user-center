/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.enums.TaskStatus;
import com.idear.user.center.core.mapper.TaskDetailMapper;
import com.idear.user.center.core.po.TaskDetail;
import com.idear.user.center.service.dto.TaskPlanDetailDto;
import com.idear.user.center.service.facade.TaskDetailService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/5
 */
@Service
public class TaskDetailServiceImpl implements TaskDetailService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Resource
    private TaskDetailMapper taskDetailMapper;

    /**
     * 查询任务详情列表
     *
     * @param planId
     * @return
     */
    @Override
    public Response<List<TaskPlanDetailDto>> queryTaskDetail(final Integer planId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                List<TaskDetail> detailList = taskDetailMapper.getTaskDetailByPlanId(planId);
                if (detailList == null || detailList.isEmpty()) {
                    return Response.SUCCESS;
                }
                return new Response(convert2DtoList(detailList));
            }
        });
    }

    /**
     * 新增任务详情
     *
     * @param dto
     * @return
     */
    @Override
    public Response<Integer> addTaskDetail(final TaskPlanDetailDto dto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                TaskDetail taskDetail = new TaskDetail();
                taskDetail.setPlanId(dto.getPlanId());
                taskDetail.setAmount(dto.getAmount());
                taskDetail.setChannel(dto.getChannel());
                taskDetail.setStartTime(dto.getStartTime());
                taskDetail.setEndTime(dto.getEndTime());
                taskDetail.setShowTime(dto.getShowTime());
                taskDetail.setReceivedNum(dto.getReceivedNum());
                taskDetail.setTotalNum(dto.getTotalNum());
                taskDetail.setStatus(TaskStatus.ON_LINE.getCode());
                taskDetail.setReceivedNum(0);
                Date nowDate = new Date();
                taskDetail.setCreateTime(nowDate);
                taskDetail.setUpdateTime(nowDate);
                taskDetailMapper.saveTaskDetail(taskDetail);
                return new Response(taskDetail.getId());
            }
        });
    }

    /**
     * 更新任务详情
     *
     * @param dto
     * @return
     */
    @Override
    public Response updateTaskDetail(final TaskPlanDetailDto dto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer detailId = dto.getId();
                if (detailId == null) {
                    throw new BusinessException(ServiceMessage.TASK_DETAIL_ID_IS_NULL);
                }
                TaskDetail taskDetail = taskDetailMapper.getTaskDetail(dto.getId());
                if (taskDetail == null) {
                    throw new BusinessException(ServiceMessage.TASK_DETAIL_INFO_NOT_EXIST);
                }
                if (StringUtil.isNotEmpty(dto.getChannel())) {
                    taskDetail.setChannel(dto.getChannel());
                }
                if (dto.getAmount() != null) {
                    taskDetail.setAmount(dto.getAmount());
                }
                if (dto.getTotalNum() != null) {
                    taskDetail.setTotalNum(dto.getTotalNum());
                }
                if (dto.getStartTime() != null) {
                    taskDetail.setStartTime(dto.getStartTime());
                }
                if (dto.getShowTime() != null) {
                    taskDetail.setShowTime(dto.getShowTime());
                }
                if (dto.getEndTime() != null) {
                    taskDetail.setEndTime(dto.getEndTime());
                }
                if (dto.getStatus() != null) {
                    taskDetail.setStatus(dto.getStatus());
                }
                taskDetailMapper.updateTaskDetail(taskDetail);
                return Response.SUCCESS;
            }
        });
    }

    private TaskPlanDetailDto convert2Dto(TaskDetail taskDetail) {
        TaskPlanDetailDto detailDto = new TaskPlanDetailDto();
        detailDto.setId(taskDetail.getId());
        detailDto.setAmount(taskDetail.getAmount());
        detailDto.setChannel(taskDetail.getChannel());
        detailDto.setPlanId(taskDetail.getPlanId());
        detailDto.setTotalNum(taskDetail.getTotalNum());
        detailDto.setReceivedNum(taskDetail.getReceivedNum());
        detailDto.setStartTime(taskDetail.getStartTime());
        detailDto.setEndTime(taskDetail.getEndTime());
        detailDto.setShowTime(taskDetail.getShowTime());
        detailDto.setCreateTime(taskDetail.getCreateTime());
        detailDto.setUpdateTime(taskDetail.getUpdateTime());
        detailDto.setStatus(taskDetail.getStatus());
        return detailDto;
    }

    private List<TaskPlanDetailDto> convert2DtoList(List<TaskDetail> taskDetailList) {
        List<TaskPlanDetailDto> dtoList = new ArrayList<>();
        if (taskDetailList != null && !taskDetailList.isEmpty()) {
            Iterator<TaskDetail> iterator = taskDetailList.iterator();
            while (iterator.hasNext()) {
                TaskDetail taskDetail = iterator.next();
                dtoList.add(convert2Dto(taskDetail));
            }
        }
        return dtoList;
    }
}
