/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.util.JsonUtil;
import com.idear.framework.response.Response;
import com.idear.user.center.core.mapper.AppInfoMapper;
import com.idear.user.center.core.mapper.MarketMapper;
import com.idear.user.center.core.mapper.TaskDetailMapper;
import com.idear.user.center.core.mapper.TaskPlanMapper;
import com.idear.user.center.core.po.AppInfo;
import com.idear.user.center.core.po.AppMarket;
import com.idear.user.center.core.po.TaskDetail;
import com.idear.user.center.core.po.TaskPlan;
import com.idear.user.center.service.dto.TaskDetailDto;
import com.idear.user.center.service.dto.TaskListDto;
import com.idear.user.center.service.facade.TaskService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Resource
    private TaskDetailMapper taskDetailMapper;

    @Resource
    private AppInfoMapper appInfoMapper;

    @Resource
    private TaskPlanMapper taskPlanMapper;

    @Resource
    private MarketMapper marketMapper;

    /**
     * 获取任务列表
     *
     * @param channel
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<TaskListDto> getTaskList(String channel, Integer pageNo, Integer pageSize) {
        Map<String, Object> queryTaskListParam = new HashMap<>(2);
        queryTaskListParam.put("channel", channel);
        queryTaskListParam.put("nowDate", new Date());
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        PageList<TaskDetail> detailList = taskDetailMapper.getOnlineTaskList(queryTaskListParam, pageBounds);
        if (detailList == null || detailList.isEmpty()) {
            return Response.SUCCESS;
        } else {
            List<Integer> planIdList = new ArrayList<>();
            for (TaskDetail taskDetail : detailList) {
                planIdList.add(taskDetail.getPlanId());
            }
            List<TaskPlan> planList = taskPlanMapper.getTaskPlanListByIds(planIdList);
            List<Integer> appIdList = new ArrayList<>();
            Map<Integer, TaskPlan> taskPlanMap = new HashMap<>(16);
            for (TaskPlan taskPlan : planList) {
                appIdList.add(taskPlan.getAppId());
                taskPlanMap.put(taskPlan.getId(), taskPlan);
            }
            List<AppInfo> appInfoList = appInfoMapper.getAppInfoListByIds(appIdList);
            Map<Integer, AppInfo> appInfoMap = new HashMap<>(16);
            for (AppInfo appInfo : appInfoList) {
                appInfoMap.put(appInfo.getId(), appInfo);
            }

            List<AppMarket> marketList = marketMapper.getMarketList();
            Map<Integer, AppMarket> marketMap = new HashMap<>(16);
            for (AppMarket market : marketList) {
                marketMap.put(market.getId(), market);
            }
            return new Response(convert2TaskListDto(detailList, taskPlanMap, appInfoMap, marketMap));
        }
    }

    private TaskListDto convert2TaskListDto(PageList<TaskDetail> detailList, Map<Integer, TaskPlan> taskPlanMap,
                                            Map<Integer, AppInfo> appInfoMap, Map<Integer, AppMarket> marketMap) {
        TaskListDto taskListDto = new TaskListDto();
        Integer totalNum = detailList.getPaginator().getTotalCount();
        Integer totalPage = detailList.getPaginator().getTotalPages();
        taskListDto.setTotalNum(totalNum);
        taskListDto.setTotalPage(totalPage);
        List<TaskDetailDto> taskDetailList = new ArrayList<>();
        for (TaskDetail taskDetail : detailList) {
            TaskPlan taskPlan = taskPlanMap.get(taskDetail.getPlanId());
            AppInfo appInfo = appInfoMap.get(taskPlan.getAppId());
            AppMarket market = null;
            Integer marketId = taskPlan.getMarketId();
            if (marketId != null) {
                market = marketMap.get(marketId);
            }
            taskDetailList.add(convert2TaskDetailDto(taskDetail, taskPlan, appInfo, market));
        }
        taskListDto.setTaskDetailList(taskDetailList);
        return taskListDto;
    }

    private TaskDetailDto convert2TaskDetailDto(TaskDetail taskDetail, TaskPlan taskPlan, AppInfo appInfo, AppMarket market) {
        TaskDetailDto taskDetailDto = new TaskDetailDto();
        taskDetailDto.setAmount(taskDetail.getAmount());
        taskDetailDto.setTaskDescription(taskPlan.getDescription());
        taskDetailDto.setEndTime(taskDetail.getEndTime());
        taskDetailDto.setStartTime(taskDetail.getStartTime());
        taskDetailDto.setShowTime(taskDetail.getShowTime());
        taskDetailDto.setTotalNum(taskDetail.getTotalNum());
        taskDetailDto.setReceivedNum(taskDetail.getReceivedNum());
        taskDetailDto.setRemainNum(taskDetail.getTotalNum() - taskDetail.getReceivedNum());
        taskDetailDto.setStatus(taskDetail.getStatus());
        taskDetailDto.setTaskId(taskDetail.getId());
        taskDetailDto.setTaskName(taskPlan.getName());
        taskDetailDto.setType(taskPlan.getType());
        if (market != null){
            taskDetailDto.setMarketName(market.getName());
            taskDetailDto.setMarketLogoUrl(market.getLogoUrl());
            taskDetailDto.setMarketPackageName(market.getPackageName());
        }
        taskDetailDto.setTypeInfo(taskPlan.getTypeInfo());
        taskDetailDto.setLabelList(JsonUtil.parseObject(taskPlan.getLabel(), ArrayList.class));
        taskDetailDto.setTutorialList(JsonUtil.parseObject(taskPlan.getTutorial(), ArrayList.class));
        taskDetailDto.setAppName(appInfo.getAppName());
        taskDetailDto.setAppLogoUrl(appInfo.getAppLogoUrl());
        taskDetailDto.setAppPackageName(appInfo.getPackageName());
        return taskDetailDto;
    }

    /**
     * 获取任务详情
     *
     * @param taskId
     * @return
     */
    @Override
    public Response<TaskDetailDto> getTaskDetail(Integer taskId) {
        TaskDetail taskDetail = taskDetailMapper.getTaskDetail(taskId);
        if (taskDetail == null) {
            return Response.SUCCESS;
        }
        TaskPlan taskPlan = taskPlanMapper.getTaskPlanById(taskDetail.getPlanId());
        Integer marketId = taskPlan.getMarketId();
        AppMarket market = null;
        if (marketId != null) {
            market = marketMapper.getMarket(marketId);
        }
        AppInfo appInfo = appInfoMapper.getAppInfoById(taskPlan.getAppId());
        return new Response(convert2TaskDetailDto(taskDetail, taskPlan, appInfo, market));
    }
}
