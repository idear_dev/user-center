package com.idear.user.center.service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.Resource;

import java.util.List;

/**
 *
 * @author CcShan
 * @date 2017/9/22
 */
public interface ResourceService {

    List<Resource> getResourceListById(List<Integer> resourceIdList);

    /**
     * 添加资源
     * @param resource
     * @return
     */
    int addResource(Resource resource);

    /**
     * 更改资源
     * @param resource
     * @return
     */
    int editResource(Resource resource);

    /**
     * 删除资源
     * @param id
     * @return
     */
    int deleteResource(Integer id);

    /**
     * 查询资源列表
     * @param resource
     * @return
     */
    PageList<Resource> queryResourceList(Resource resource, Integer pageNo, Integer pageSize);

    /**
     * 查看所有资源列表
     * @return
     */
    List<Resource> queryAllResourceList();

    /**
     * 根据名称查询所有资源，包括删除的
     * @param name
     * @return
     */
    Resource queryAllResourceByName(String name);

}
