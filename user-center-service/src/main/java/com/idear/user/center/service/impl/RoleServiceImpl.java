package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.mapper.RoleMapper;
import com.idear.user.center.core.po.Role;
import com.idear.user.center.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by CcShan on 2017/9/19.
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public PageList<Role> pageableRoleList(Integer pageNo, Integer pageSize) {
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        return roleMapper.getRoleList(pageBounds);
    }

    @Override
    public List<Role> getRoleListById(List<Integer> roleIdList) {
        return roleMapper.getRoleListById(roleIdList);
    }

    @Override
    public void createRole(Role role) {
        roleMapper.addRole(role);
    }

    @Override
    public void updateRole(Role role) {
        roleMapper.updateRole(role);
    }

    @Override
    public List<Role> getRoleListByGroupId(Integer groupId) {
        return roleMapper.getRoleListByGroupId(groupId);
    }

    @Override
    public List<Role> getRoleList() {
        return roleMapper.getRoleList();
    }
}
