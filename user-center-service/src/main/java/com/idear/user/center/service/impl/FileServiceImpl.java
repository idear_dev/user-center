/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.FileUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.config.FileUploadConfig;
import com.idear.user.center.service.facade.FileService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @author CcShan
 * @date 2018/9/18
 */
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private FileUploadConfig fileUploadConfig;

    @Autowired
    private BusinessTemplate businessTemplate;

    /**
     * 上传文件
     *
     * @param inputStream
     * @param path        相对路径
     * @return
     */
    @Override
    public Response<String> uploadFile(final InputStream inputStream, final String path) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String filePath = fileUploadConfig.getUploadFilePathIndex()+path;
                Boolean result = FileUtil.writeFileFromInputStream(inputStream,filePath);
                if (result){
                    return new Response(fileUploadConfig.getVisitUrlIndex()+path);
                }else {
                    throw new BusinessException(ServiceMessage.UPLOAD_FILE_FAILED);
                }
            }
        });

    }
}
