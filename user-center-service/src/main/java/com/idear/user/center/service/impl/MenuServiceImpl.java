package com.idear.user.center.service.impl;

import com.idear.user.center.core.enums.DelFlag;
import com.idear.user.center.core.mapper.MenuMapper;
import com.idear.user.center.core.po.Menu;
import com.idear.user.center.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author CcShan
 * @date 2017/9/22
 */

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> getMenuListById(List<Integer> menuIdList) {
        return menuMapper.getMenuListById(menuIdList);
    }

    public List<Menu> getMenuList() {
        return menuMapper.getMenuList();
    }

    @Override
    public int addMenu(Menu menu) {
        return menuMapper.insertSelective(menu);
    }

    @Override
    public int editMenu(Menu menu) {
        return menuMapper.updateByPrimaryKeySelective(menu);
    }

    @Override
    public int deleteMenu(Integer id) {
        Menu menu = new Menu();
        menu.setId(id);
        menu.setDelFlag(DelFlag.DELETED.getCode());
        return menuMapper.updateByPrimaryKeySelective(menu);
    }


    @Override
    public List<Menu> queryMenuList(Menu menu) {
        return menuMapper.queryMenuList(menu);
    }

    @Override
    public Menu queryAllMenuByName(String name) {
        return menuMapper.queryAllMenuByName(name);
    }

}
