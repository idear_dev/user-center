package com.idear.user.center.service;


import com.idear.user.center.core.enums.RoleGroupStatus;
import com.idear.user.center.core.po.RoleGroup;

import java.util.List;

/**
 *
 * @author CcShan
 * @date 2017/9/26
 */
public interface RoleGroupService {

    void addRoleGroup(RoleGroup roleGroup);

    RoleGroup getRoleGroupByName(String groupName);

    void updateRoleGroupStatus(Integer roleGroupId, RoleGroupStatus roleGroupStatus);

    void updateRoleGroupName(Integer roleGroupId, String roleGroupName);

    List<RoleGroup> getRoleGroupByStatus(RoleGroupStatus roleGroupStatus);
}
