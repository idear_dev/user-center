/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.DateUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.PageQueryUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.enums.*;
import com.idear.user.center.core.mapper.*;
import com.idear.user.center.core.po.*;
import com.idear.user.center.service.dto.TaskRecordDto;
import com.idear.user.center.service.dto.TaskRecordListDto;
import com.idear.user.center.service.facade.TaskRecordService;
import com.idear.user.center.service.message.ServiceMessage;
import com.idear.user.center.service.param.TaskRecordListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author ShanCc
 * @date 2018/9/17
 */
@Service
public class TaskRecordServiceImpl implements TaskRecordService {

    @Resource
    private TaskRecordMapper taskRecordMapper;

    @Resource
    private TaskDetailMapper taskDetailMapper;

    @Resource
    private TaskPlanMapper taskPlanMapper;

    @Resource
    private AppInfoMapper appInfoMapper;

    @Resource
    private MarketMapper marketMapper;

    @Resource
    private AccountMapper accountMapper;

    @Resource
    private AccountRecordMapper accountRecordMapper;

    @Autowired
    private BusinessTemplate businessTemplate;

    /**
     * 是否可以领取任务
     * true:可以领取
     * false:不可以领取
     *
     * @param memberId
     * @param taskId
     * @return
     */
    @Override
    public Response<TaskRecordDto> getEffectiveTaskRecord(final Integer memberId, final Integer taskId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                TaskDetail taskDetail = taskDetailMapper.getTaskDetail(taskId);
                if (taskDetail == null) {
                    throw new BusinessException(ServiceMessage.TASK_DETAIL_NOT_EXIST);
                }
                MemberTaskRecord memberTaskRecord = getEffectiveTaskRecord(memberId, taskDetail);
                if (memberTaskRecord == null) {
                    return Response.SUCCESS;
                } else {
                    return new Response(convert2RecordDto(memberTaskRecord));
                }

            }
        });
    }

    /**
     * 领取任务
     *
     * @param memberId
     * @param taskId
     * @return
     */
    @Override
    public Response<Integer> receiveTask(final Integer memberId, final Integer taskId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                TaskDetail taskDetail = taskDetailMapper.getTaskDetail(taskId);
                if (taskDetail == null) {
                    throw new BusinessException(ServiceMessage.TASK_DETAIL_NOT_EXIST);
                }
                if (canReceive(memberId, taskDetail)) {
                    Integer recordId = doReceive(memberId, taskDetail);
                    return new Response(recordId);
                } else {
                    throw new BusinessException(ServiceMessage.HAS_RECEIVE_TASK);
                }
            }
        });
    }

    /**
     * 取消任务
     *
     * @param memberId
     * @param recordId
     * @return
     */
    @Override
    public Response cancelTask(final Integer memberId, final Integer recordId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                MemberTaskRecord record = getTaskRecord(memberId, recordId);
                if (canCancel(record)) {
                    doCancel(record);
                    return Response.SUCCESS;
                } else {
                    throw new BusinessException(ServiceMessage.RECORD_CAN_NOT_CANCEL);
                }
            }
        });
    }

    /**
     * 提交审核
     *
     * @param memberId
     * @param recordId
     * @param auditInfo
     * @return
     */
    @Override
    public Response submitAudit(final Integer memberId, final Integer recordId, final List<String> auditInfo) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                MemberTaskRecord record = getTaskRecord(memberId, recordId);
                if (TaskRecordStatus.DOING.getCode().equals(record.getStatus())) {
                    record.setAuditInfo(JsonUtil.toJsonString(auditInfo));
                    record.setSubmitTime(new Date());
                    record.setStatus(TaskRecordStatus.TO_AUDIT.getCode());
                    taskRecordMapper.submitTaskRecordAuditInfo(record);
                    return Response.SUCCESS;
                } else {
                    throw new BusinessException(ServiceMessage.RECORD_CAN_NOT_SUBMIT_AUDIT);
                }
            }
        });
    }

    /**
     * 查询任务记录
     *
     * @param param
     * @return
     */
    @Override
    public Response<TaskRecordListDto> getRecordList(final TaskRecordListParam param) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer pageNo = param.getPageNo();
                if (pageNo == null) {
                    pageNo = PageQueryUtil.DEFAULT_PAGE_NO;
                }
                Integer pageSize = param.getPageSize();
                if (pageSize == null) {
                    pageSize = PageQueryUtil.DEFAULT_PAGE_SIZE;
                }
                if (pageSize > PageQueryUtil.DEFAULT_MAX_PAGE_SIZE) {
                    pageSize = PageQueryUtil.DEFAULT_MAX_PAGE_SIZE;
                }
                PageBounds pageBounds = new PageBounds(pageNo, pageSize);
                Map<String, Object> queryParam = new HashMap<>(16);
                queryParam.put("beginTime", param.getBeginTime());
                queryParam.put("endTime", param.getEndTime());
                queryParam.put("memberId", param.getMemberId());
                queryParam.put("statusList", param.getStatusList());
                PageList<MemberTaskRecord> recordList = taskRecordMapper.getTaskRecordList(queryParam, pageBounds);
                return new Response(convert2RecordListDto(recordList));
            }
        });
    }

    /**
     * 查询任务记录详情
     *
     * @param memberId
     * @param recordId
     * @return
     */
    @Override
    public Response<TaskRecordDto> getRecordDetail(final Integer memberId, final Integer recordId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                MemberTaskRecord record = getTaskRecord(memberId, recordId);
                TaskRecordDto taskRecordDto = convert2RecordDto(record);
                return new Response(taskRecordDto);
            }
        });
    }

    /**
     * 获取Banner位置信息
     *
     * @return
     */
    @Override
    public Response<List<Map<String, Object>>> listStatus() {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                TaskRecordStatus[] statusArray = TaskRecordStatus.values();
                List<Map<String, Object>> result = new ArrayList<>();
                for (TaskRecordStatus status : statusArray) {
                    Map<String, Object> temp = new HashMap<>(2);
                    temp.put("code", status.getCode());
                    temp.put("desc", status.getDesc());
                    result.add(temp);
                }
                return new Response<>(result);
            }
        });
    }

    /**
     * 管理端审核
     *
     * @param recordId
     * @param pass
     * @param remark
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Response audit(final Integer recordId, final Boolean pass, final String remark) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                MemberTaskRecord param = new MemberTaskRecord();
                param.setId(recordId);
                MemberTaskRecord record = taskRecordMapper.getTaskRecordById(param);
                if (record == null) {
                    throw new BusinessException(ServiceMessage.TASK_RECORD_NOT_EXIST);
                }
                if (!TaskRecordStatus.TO_AUDIT.getCode().equals(record.getStatus())){
                    throw new BusinessException(ServiceMessage.TASK_RECORD_STATUS_EXCEPTION);
                }
                record.setAuditRemark(remark);
                if (pass){
                    record.setStatus(TaskRecordStatus.AUDIT_SUCCEED.getCode());
                    inAccount(record);
                }else {
                    record.setStatus(TaskRecordStatus.AUDIT_FAILED.getCode());
                }
                record.setAuditTime(new Date());
                taskRecordMapper.updateTaskRecord(record);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 入帐
     * @param record
     */
    private void inAccount(MemberTaskRecord record){
        Integer memberId = record.getMemberId();
        Account account = accountMapper.getAccountByMemberId(memberId);
        Date nowDate = new Date();
        //开户
        if (account == null){
            account = new Account();
            account.setMemberId(memberId);
            account.setStatus(AccountStatus.NORMAL.getCode());
            account.setCreateTime(nowDate);
            account.setUpdateTime(nowDate);
            accountMapper.saveAccount(account);
        }
        Integer accountId = account.getId();
        AccountRecord accountRecord = new AccountRecord();
        accountRecord.setAccountId(accountId);
        accountRecord.setMemberId(memberId);
        JSONObject taskInfo = JsonUtil.parseJsonObject(record.getTaskInfo());
        accountRecord.setAmount(taskInfo.getInteger("amount"));
        accountRecord.setSerialNo(DateUtil.getCurrentDate(DateUtil.FULL_PATTERN)+System.currentTimeMillis());
        accountRecord.setInOutFlag(AccountRecordFlag.IN.getCode());
        accountRecord.setRecordType(AccountRecordType.CASH_BACK.getCode());
        accountRecord.setRemark("任务完成返现");
        accountRecord.setRecordTime(nowDate);
        accountRecord.setCreateTime(nowDate);
        accountRecord.setUpdateTime(nowDate);
        accountRecord.setStatus(AccountRecordStatus.TRANS_SUCCEED.getCode());
        accountRecordMapper.saveAccountRecord(accountRecord);
    }


    /**
     * 类型转换
     *
     * @param recordList
     * @return
     */
    private TaskRecordListDto convert2RecordListDto(PageList<MemberTaskRecord> recordList) {
        TaskRecordListDto taskRecordListDto = new TaskRecordListDto();
        Integer totalNum = recordList.getPaginator().getTotalCount();
        Integer totalPage = recordList.getPaginator().getTotalPages();
        taskRecordListDto.setTotalNum(totalNum);
        taskRecordListDto.setTotalPage(totalPage);
        List<TaskRecordDto> taskRecordList = new ArrayList<>();
        if (!recordList.isEmpty()) {
            for (MemberTaskRecord memberTaskRecord : recordList) {
                TaskRecordDto taskRecordDto = convert2RecordDto(memberTaskRecord);
                taskRecordList.add(taskRecordDto);
            }
        }
        taskRecordListDto.setTaskRecordList(taskRecordList);
        return taskRecordListDto;
    }

    /**
     * 类型转换
     *
     * @param memberTaskRecord
     * @return
     */
    private TaskRecordDto convert2RecordDto(MemberTaskRecord memberTaskRecord) {
        TaskRecordDto taskRecordDto = new TaskRecordDto();
        taskRecordDto.setRecordId(memberTaskRecord.getId());
        taskRecordDto.setMemberId(memberTaskRecord.getMemberId());
        taskRecordDto.setMarketInfo(memberTaskRecord.getMarketInfo());
        taskRecordDto.setAppInfo(memberTaskRecord.getAppInfo());
        taskRecordDto.setPlanInfo(memberTaskRecord.getPlanInfo());
        taskRecordDto.setTaskInfo(memberTaskRecord.getTaskInfo());
        taskRecordDto.setAuditInfo(memberTaskRecord.getAuditInfo());
        taskRecordDto.setAuditTime(memberTaskRecord.getAuditTime());
        taskRecordDto.setAuditRemark(memberTaskRecord.getAuditRemark());
        taskRecordDto.setSubmitTime(memberTaskRecord.getSubmitTime());
        taskRecordDto.setStatus(memberTaskRecord.getStatus());
        taskRecordDto.setCreateTime(memberTaskRecord.getCreateTime());
        taskRecordDto.setUpdateTime(memberTaskRecord.getUpdateTime());
        return taskRecordDto;
    }

    /**
     * 查询任务记录
     *
     * @param memberId
     * @param recordId
     * @return
     */
    private MemberTaskRecord getTaskRecord(Integer memberId, Integer recordId) {
        MemberTaskRecord param = new MemberTaskRecord();
        param.setMemberId(memberId);
        param.setId(recordId);
        MemberTaskRecord record = taskRecordMapper.getTaskRecordById(param);
        if (record == null) {
            throw new BusinessException(ServiceMessage.TASK_RECORD_NOT_EXIST);
        }
        return record;
    }

    /**
     * 取消任务
     *
     * @param record
     */
    @Transactional(rollbackFor = Exception.class)
    void doCancel(MemberTaskRecord record) {
        record.setStatus(TaskRecordStatus.CANCELED.getCode());
        taskRecordMapper.updateTaskRecord(record);
        taskDetailMapper.reduceReceivedNum(record.getTaskId());
    }


    /**
     * 是否可以取消
     *
     * @param record
     * @return
     */
    private Boolean canCancel(MemberTaskRecord record) {
        if (TaskRecordStatus.DOING.getCode().equals(record.getStatus())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * 执行领取任务
     *
     * @param memberId
     * @param taskDetail
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    Integer doReceive(Integer memberId, TaskDetail taskDetail) {
        checkTaskDate(taskDetail);
        checkTaskNum(taskDetail);
        increaseReceivedNum(taskDetail);
        return saveTaskRecord(memberId, taskDetail);
    }

    /**
     * 增加使用
     *
     * @param taskDetail
     */
    private void increaseReceivedNum(TaskDetail taskDetail) {
        taskDetailMapper.increaseReceivedNum(taskDetail.getId());
    }

    /**
     * 保存记录
     *
     * @param memberId
     * @param taskDetail
     * @return
     */
    private Integer saveTaskRecord(Integer memberId, TaskDetail taskDetail) {
        MemberTaskRecord memberTaskRecord = new MemberTaskRecord();
        TaskPlan taskPlan = taskPlanMapper.getTaskPlanById(taskDetail.getId());
        Integer marketId = taskPlan.getMarketId();
        if (marketId != null) {
            AppMarket appMarket = marketMapper.getMarket(marketId);
            memberTaskRecord.setMarketId(marketId);
            memberTaskRecord.setMarketInfo(JsonUtil.toJsonString(appMarket));
        }
        AppInfo appInfo = appInfoMapper.getAppInfoById(taskPlan.getAppId());
        memberTaskRecord.setMemberId(memberId);
        memberTaskRecord.setAppId(appInfo.getId());
        memberTaskRecord.setAppInfo(JsonUtil.toJsonString(appInfo));
        memberTaskRecord.setTaskId(taskDetail.getId());
        memberTaskRecord.setTaskInfo(JsonUtil.toJsonString(taskDetail));
        memberTaskRecord.setPlanId(taskPlan.getId());
        memberTaskRecord.setPlanInfo(JsonUtil.toJsonString(taskPlan));
        memberTaskRecord.setStatus(TaskRecordStatus.DOING.getCode());
        taskRecordMapper.saveTaskRecord(memberTaskRecord);
        return memberTaskRecord.getId();
    }

    /**
     * 检查库存数量
     *
     * @param taskDetail
     */
    private void checkTaskNum(TaskDetail taskDetail) {
        Integer totalNum = taskDetail.getTotalNum();
        Integer receivedNum = taskDetail.getReceivedNum();
        if (receivedNum >= totalNum) {
            throw new BusinessException(ServiceMessage.TASK_HAS_OVER);
        }
    }

    /**
     * 检查任务时间
     *
     * @param taskDetail
     */
    private void checkTaskDate(TaskDetail taskDetail) {
        Date startTime = taskDetail.getStartTime();
        Date endTime = taskDetail.getEndTime();
        Date nowTime = new Date();
        if (nowTime.before(startTime)) {
            throw new BusinessException(ServiceMessage.TASK_NOT_START);
        }
        if (nowTime.after(endTime)) {
            throw new BusinessException(ServiceMessage.TASK_HAS_END);
        }
    }

    /**
     * 获取有效的任务记录
     *
     * @param memberId
     * @param taskDetail
     * @return
     */
    private MemberTaskRecord getEffectiveTaskRecord(Integer memberId, TaskDetail taskDetail) {
        Integer planId = taskDetail.getPlanId();
        MemberTaskRecord param = new MemberTaskRecord();
        param.setMemberId(memberId);
        param.setPlanId(planId);
        List<MemberTaskRecord> recordList = taskRecordMapper.getTaskRecordByPlanId(param);
        if (recordList != null && !recordList.isEmpty()) {
            for (MemberTaskRecord memberTaskRecord : recordList) {
                //有进行中的任务不可以参加
                if (TaskRecordStatus.DOING.getCode().equals(memberTaskRecord.getStatus())) {
                    return memberTaskRecord;
                }
                //有等待审核的任务不可参加
                if (TaskRecordStatus.TO_AUDIT.getCode().equals(memberTaskRecord.getStatus())) {
                    return memberTaskRecord;
                }
                //有已经成功参加任务
                if (TaskRecordStatus.AUDIT_SUCCEED.getCode().equals(memberTaskRecord.getStatus())) {
                    return memberTaskRecord;
                }
            }
        }
        return null;
    }

    /**
     * 用户是否可以参加任务
     *
     * @param taskDetail
     * @param memberId
     * @return
     */
    private Boolean canReceive(Integer memberId, TaskDetail taskDetail) {
        MemberTaskRecord memberTaskRecord = getEffectiveTaskRecord(memberId, taskDetail);
        if (memberTaskRecord == null) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
