/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author CcShan
 * @date 2018/9/18
 */
@Component
public class FileUploadConfig {

    @Value("${upload.file.path.index}")
    private String uploadFilePathIndex;

    @Value("${visit.url.index}")
    private String visitUrlIndex;

    public String getUploadFilePathIndex() {
        return uploadFilePathIndex;
    }

    public void setUploadFilePathIndex(String uploadFilePathIndex) {
        this.uploadFilePathIndex = uploadFilePathIndex;
    }

    public String getVisitUrlIndex() {
        return visitUrlIndex;
    }

    public void setVisitUrlIndex(String visitUrlIndex) {
        this.visitUrlIndex = visitUrlIndex;
    }
}
