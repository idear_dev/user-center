package com.idear.user.center.service;


import com.idear.user.center.core.po.Menu;

import java.util.List;

/**
 * Created by CcShan on 2017/9/22.
 */
public interface MenuService {

    List<Menu> getMenuListById(List<Integer> menuIdList);

    List<Menu> getMenuList();

    /**
     * 添加菜单信息
     * @param menu
     * @return
     */
    int addMenu(Menu menu);

    /**
     * 编辑菜单信息
     * @param menu
     * @return
     */
    int editMenu(Menu menu);

    /**
     * 删除菜单信息
     * @param id
     * @return
     */
    int deleteMenu(Integer id);

    /**
     * 查询菜单列表
     * @param menu
     * @return
     */
    List<Menu> queryMenuList(Menu menu);

    /**
     * 根据名称查询菜单，包括删掉的
     * @param name
     * @return
     */
    Menu queryAllMenuByName(String name);

}
