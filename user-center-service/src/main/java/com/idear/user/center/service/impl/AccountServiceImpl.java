/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.dao.AccountRecordDao;
import com.idear.user.center.core.po.AccountRecord;
import com.idear.user.center.service.dto.AccountInfoDto;
import com.idear.user.center.service.dto.AccountRecordDto;
import com.idear.user.center.service.dto.AccountRecordListDto;
import com.idear.user.center.service.facade.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRecordDao accountRecordDao;

    @Autowired
    private BusinessTemplate businessTemplate;

    /**
     * 获取账户信息
     *
     * @param memberId
     * @return
     */
    @Override
    public Response<AccountInfoDto> getAccountInfo(final Integer memberId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                AccountInfoDto accountInfoDto = new AccountInfoDto();
                accountInfoDto.setTotalIncome(accountRecordDao.sumTotalIncome(memberId));
                accountInfoDto.setTodayIncome(accountRecordDao.sumTodayIncome(memberId));
                accountInfoDto.setCanWithdrawAmount(accountRecordDao.sumCanWithdrawAmount(memberId));
                accountInfoDto.setHasWithdrawAmount(accountRecordDao.sumHasWithdrawAmount(memberId));
                accountInfoDto.setWithdrawingAmount(accountRecordDao.sumWithdrawingAmount(memberId));
                accountInfoDto.setImportingAmount(accountRecordDao.sumImportingAmount(memberId));
                return new Response(accountInfoDto);
            }
        });
    }

    /**
     * 分期查询收入明细列表
     *
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<AccountRecordListDto> getIncomeList(final Integer memberId, final Integer pageNo, final Integer pageSize) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                PageList<AccountRecord> recordPageList = accountRecordDao.queryIncomeList(memberId,pageNo,pageSize);
                AccountRecordListDto accountRecordListDto = convert2AccountRecordListDto(recordPageList);
                return new Response(accountRecordListDto);
            }
        });
    }

    /**
     * 获取成功提现列表
     *
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<AccountRecordListDto> getHasWithdrawList(final Integer memberId, final Integer pageNo, final Integer pageSize) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                PageList<AccountRecord> recordPageList = accountRecordDao.queryHasWithdrawList(memberId,pageNo,pageSize);
                AccountRecordListDto accountRecordListDto = convert2AccountRecordListDto(recordPageList);
                return new Response(accountRecordListDto);
            }
        });
    }

    /**
     * 获取提现审核中的列表
     *
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<AccountRecordListDto> getWithdrawingList(final Integer memberId, final Integer pageNo, final Integer pageSize) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                PageList<AccountRecord> recordPageList = accountRecordDao.queryWithdrawingList(memberId,pageNo,pageSize);
                AccountRecordListDto accountRecordListDto = convert2AccountRecordListDto(recordPageList);
                return new Response(accountRecordListDto);
            }
        });
    }

    private AccountRecordListDto convert2AccountRecordListDto(PageList<AccountRecord> recordPageList){
        AccountRecordListDto incomeListDto = new AccountRecordListDto();
        if (recordPageList == null){
            incomeListDto.setTotalNum(0);
            incomeListDto.setTotalPage(0);
        }else {
            incomeListDto.setTotalNum(recordPageList.getPaginator().getTotalCount());
            incomeListDto.setTotalPage(recordPageList.getPaginator().getTotalPages());
            List<AccountRecordDto> accountRecordDtoList = new ArrayList<>();
            for (AccountRecord accountRecord:recordPageList){
                AccountRecordDto accountRecordDto = new AccountRecordDto();
                accountRecordDto.setAmount(accountRecord.getAmount());
                accountRecordDto.setId(accountRecord.getId());
                accountRecordDto.setSerialNo(accountRecord.getSerialNo());
                accountRecordDto.setRemark(accountRecord.getRemark());
                accountRecordDto.setStatus(accountRecord.getStatus());
                accountRecordDto.setRecordTime(accountRecord.getCreateTime());
                accountRecordDto.setUpdateTime(accountRecord.getUpdateTime());
                accountRecordDtoList.add(accountRecordDto);
            }
            incomeListDto.setAccountRecordDtoList(accountRecordDtoList);
        }
        return incomeListDto;
    }
}
