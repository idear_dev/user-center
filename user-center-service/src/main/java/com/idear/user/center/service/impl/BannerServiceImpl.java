/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.enums.BannerLocation;
import com.idear.user.center.core.enums.BannerStatus;
import com.idear.user.center.core.mapper.BannerMapper;
import com.idear.user.center.core.po.Banner;
import com.idear.user.center.service.dto.BannerDto;
import com.idear.user.center.service.facade.BannerService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author CcShan
 * @date 2018/9/2
 */
@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Resource
    private BannerMapper bannerMapper;

    /**
     * 根据位置信息获取banner信息
     *
     * @param location
     * @return
     */
    @Override
    public Response<List<BannerDto>> listBanner(final Integer location) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                BannerLocation bannerLocation = BannerLocation.getLocation(location);
                if (bannerLocation == null) {
                    throw new BusinessException(ServiceMessage.BANNER_LOCATION_NOT_EXIST);
                }
                List<Banner> bannerList = bannerMapper.getBannerListByLocation(bannerLocation.getCode());
                return new Response(convert2DtoList(bannerList));
            }
        });
    }

    /**
     * 根据位置信息获取banner信息
     *
     * @return
     */
    @Override
    public Response<List<BannerDto>> listBanner() {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                List<Banner> bannerList = bannerMapper.getBannerList();
                return new Response(convert2DtoList(bannerList));
            }
        });
    }

    /**
     * 获取Banner位置信息
     *
     * @return
     */
    @Override
    public Response<List<Map<String, Object>>> listLocation() {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                BannerLocation[] locationArray = BannerLocation.values();
                List<Map<String, Object>> result = new ArrayList<>();
                for (BannerLocation location : locationArray) {
                    Map<String,Object> temp = new HashMap<>(2);
                    temp.put("code",location.getCode());
                    temp.put("desc",location.getDesc());
                    result.add(temp);
                }
                return new Response<>(result);
            }
        });
    }

    /**
     * 添加广告信息
     *
     * @param bannerDto
     * @return
     */
    @Override
    public Response<Integer> addBanner(final BannerDto bannerDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Banner banner = new Banner();
                //默认图片类型
                banner.setCategory(1);
                banner.setImageUrl(bannerDto.getImageUrl());
                banner.setLinkUrl(bannerDto.getLinkUrl());
                banner.setLocation(bannerDto.getLocation());
                banner.setSort(bannerDto.getSort());
                banner.setStatus(BannerStatus.ENABLE.getCode());
                banner.setTitle(bannerDto.getTitle());
                Date nowDate = new Date();
                banner.setCreateTime(nowDate);
                banner.setUpdateTime(nowDate);
                bannerMapper.saveBanner(banner);
                return new Response(banner.getId());
            }
        });
    }

    /**
     * 删除Banner信息
     *
     * @param bannerId
     * @return
     */
    @Override
    public Response deleteBanner(final Integer bannerId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Banner banner = bannerMapper.getBanner(bannerId);
                if (banner == null){
                    throw new BusinessException(ServiceMessage.BANNER_NOT_EXIT);
                }
                banner.setStatus(BannerStatus.DISABLE.getCode());
                bannerMapper.updateBanner(banner);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 更新banner信息
     *
     * @param bannerDto
     * @return
     */
    @Override
    public Response updateBanner(final BannerDto bannerDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer bannerId = bannerDto.getId();
                Banner banner = bannerMapper.getBanner(bannerId);
                if (banner == null){
                    throw new BusinessException(ServiceMessage.BANNER_NOT_EXIT);
                }
                if (StringUtil.isNotEmpty(bannerDto.getImageUrl())){
                    banner.setImageUrl(bannerDto.getImageUrl());
                }
                if (StringUtil.isNotEmpty(bannerDto.getLinkUrl())){
                    banner.setLinkUrl(bannerDto.getLinkUrl());
                }
                if (StringUtil.isNotEmpty(bannerDto.getTitle())){
                    banner.setTitle(bannerDto.getTitle());
                }
                if (bannerDto.getSort() != null){
                    banner.setSort(bannerDto.getSort());
                }
                if (bannerDto.getLocation() != null){
                    banner.setLocation(bannerDto.getLocation());
                }
                if (bannerDto.getStatus() != null){
                    banner.setStatus(bannerDto.getStatus());
                }
                bannerMapper.updateBanner(banner);
                return Response.SUCCESS;
            }
        });
    }

    private BannerDto convert2Dto(Banner banner) {
        BannerDto bannerDto = new BannerDto();
        bannerDto.setId(banner.getId());
        bannerDto.setImageUrl(banner.getImageUrl());
        bannerDto.setLinkUrl(banner.getLinkUrl());
        bannerDto.setLocation(banner.getLocation());
        bannerDto.setSort(banner.getSort());
        bannerDto.setStatus(banner.getStatus());
        bannerDto.setTitle(banner.getTitle());
        bannerDto.setCreateTime(banner.getCreateTime());
        bannerDto.setUpdateTime(banner.getUpdateTime());
        return bannerDto;
    }

    private List<BannerDto> convert2DtoList(List<Banner> bannerList){
        List<BannerDto> dtoList = new ArrayList<>();
        if (bannerList != null && !bannerList.isEmpty()) {
            Iterator<Banner> iterator = bannerList.iterator();
            while (iterator.hasNext()) {
                Banner banner = iterator.next();
                if (BannerStatus.ENABLE.getCode().equals(banner.getStatus())) {
                    dtoList.add(convert2Dto(banner));
                }
            }
        }
        return dtoList;
    }
}
