/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.CommonUtil;
import com.idear.common.util.Md5Util;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.config.MemberConfig;
import com.idear.user.center.core.dao.MemberDao;
import com.idear.user.center.core.enums.MemberStatus;
import com.idear.user.center.core.enums.Sex;
import com.idear.user.center.core.po.Member;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.service.facade.MemberService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberConfig memberConfig;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private BusinessTemplate businessTemplate;

    /**
     * 检查Token有效性，每检查一次延长Token有效时间
     *
     * @param token
     * @return
     */
    @Override
    public Response<Boolean> checkToken(final String token) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Boolean result = memberDao.checkToken(token,memberConfig.getTokenDelayTime());
                return new Response(result);
            }
        });
    }

    /**
     * 按照Token获取会员信息
     *
     * @param token
     * @return
     */
    @Override
    public Response<MemberInfoDto> getMemberByToken(final String token) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Member member = memberDao.getMemberByToken(token);
                return new Response(convert2MemberInfoDto(member));
            }
        });
    }

    /**
     * 按照会员Id获取会员信息
     *
     * @param memberId
     * @return
     */
    @Override
    public Response<MemberInfoDto> getMemberById(final Integer memberId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Member member = memberDao.getMemberById(memberId);
                return new Response(convert2MemberInfoDto(member));
            }
        });
    }

    /**
     * 判断手机号是否已注册
     *
     * @param mobileNo
     * @return true 已经注册 false 未注册
     */
    @Override
    public Response<Boolean> checkMobileNoIsRegistry(final String mobileNo) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Member member = memberDao.getMemberByMobileNo(mobileNo);
                if (member != null){
                    return new Response(Boolean.TRUE);
                }
                return new Response(Boolean.FALSE);
            }
        });
    }

    /**
     * 根据手机号查询会员信息
     *
     * @param mobileNo
     * @return
     */
    @Override
    public Response<MemberInfoDto> getMemberByMobileNo(final String mobileNo) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Member member = memberDao.getMemberByMobileNo(mobileNo);
                if (member != null){
                    return new Response(convert2MemberInfoDto(member));
                }
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 手机号码注册
     *
     * @param mobileNo
     * @param password
     * @return
     */
    @Override
    public Response registry(final String mobileNo, final String password) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Date nowDate = new Date();
                Member member = new Member();
                member.setMobileNo(mobileNo);
                member.setLoginPassword(Md5Util.md5(password));
                member.setCreateTime(nowDate);
                member.setUpdateTime(nowDate);
                member.setNickname(getDefaultRandomNickName());
                member.setStatus(MemberStatus.ENABLE.getCode());
                member.setMemberNo(generateMemberNo(mobileNo));
                try {
                    memberDao.saveMember(member);
                }catch (Exception e){
                    throw new BusinessException(ServiceMessage.SAVE_MEMBER_FAILED);
                }
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 重置密码
     *
     * @param mobileNo
     * @param password
     * @return
     */
    @Override
    public Response resetLoginPassword(final String mobileNo, final String password) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Member member = memberDao.getMemberByMobileNo(mobileNo);
                member.setLoginPassword(Md5Util.md5(password));
                try {
                    memberDao.updateMember(member);
                }catch (Exception e){
                    throw new BusinessException(ServiceMessage.SAVE_MEMBER_FAILED);
                }
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 登出
     *
     * @param token
     * @return
     */
    @Override
    public Response logout(final String token) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                memberDao.logout(token);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 获取默认随机昵称
     * @return
     */
    private String getDefaultRandomNickName(){
        return "miker"+ CommonUtil.getRandomNum(5);
    }

    /**
     * PO转DTO
     * @param member
     * @return
     */
    private MemberInfoDto convert2MemberInfoDto(Member member){
        if (member == null){
            return null;
        }else {
            MemberInfoDto memberInfoDto = new MemberInfoDto();
            memberInfoDto.setBirthday(member.getBirthday());
            memberInfoDto.setEmail(member.getEmail());
            memberInfoDto.setHeadImgUrl(member.getHeadImgUrl());
            memberInfoDto.setId(member.getId());
            memberInfoDto.setMemberNo(member.getMemberNo());
            memberInfoDto.setMobileNo(member.getMobileNo());
            memberInfoDto.setNickname(member.getNickname());
            memberInfoDto.setRegistryTime(member.getCreateTime());
            if (member.getSex() != null){
                memberInfoDto.setSexCode(member.getSex());
                memberInfoDto.setSexDesc(Sex.getSex(member.getSex()).getDesc());
            }
            return memberInfoDto;
        }

    }
    /**
     * 生成会员编号
     * @param mobileNo
     * @return
     */
    private String generateMemberNo(String mobileNo){
        return CommonUtil.getRandomNum(5)+mobileNo.substring(3,8);
    }
}
