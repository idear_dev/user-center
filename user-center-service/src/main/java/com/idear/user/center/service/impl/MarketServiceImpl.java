/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.mapper.MarketMapper;
import com.idear.user.center.core.po.AppMarket;
import com.idear.user.center.service.dto.MarketDto;
import com.idear.user.center.service.facade.MarketService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/10/24
 */
@Service
public class MarketServiceImpl implements MarketService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Resource
    private MarketMapper marketMapper;

    /**
     * 根据位置信息获取banner信息
     *
     * @return
     */
    @Override
    public Response<List<MarketDto>> listMarket() {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                List<AppMarket> marketList = marketMapper.getMarketList();
                return new Response(convert2DtoList(marketList));
            }
        });
    }

    /**
     * 添加广告信息
     *
     * @param marketDto
     * @return
     */
    @Override
    public Response<Integer> addMarket(final MarketDto marketDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                AppMarket appMarket = new AppMarket();
                appMarket.setName(marketDto.getName());
                appMarket.setLogoUrl(marketDto.getLogoUrl());
                appMarket.setPackageName(marketDto.getPackageName());
                Date now = new Date();
                appMarket.setCreateTime(now);
                appMarket.setUpdateTime(now);
                marketMapper.saveMarket(appMarket);
                return new Response(appMarket.getId());
            }
        });
    }

    /**
     * 更新banner信息
     *
     * @param marketDto
     * @return
     */
    @Override
    public Response updateMarket(final MarketDto marketDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer marketId = marketDto.getId();
                AppMarket market = marketMapper.getMarket(marketId);
                if (market == null){
                    throw new BusinessException(ServiceMessage.MARKET_NOT_EXIT);
                }
                if (StringUtil.isNotEmpty(marketDto.getName())){
                    market.setName(marketDto.getName());
                }
                if (StringUtil.isNotEmpty(marketDto.getLogoUrl())){
                    market.setLogoUrl(marketDto.getLogoUrl());
                }
                if (StringUtil.isNotEmpty(marketDto.getPackageName())){
                    market.setPackageName(marketDto.getPackageName());
                }
                marketMapper.updateMarket(market);
                return Response.SUCCESS;
            }
        });
    }

    private MarketDto convert2Dto(AppMarket appMarket) {
        MarketDto marketDto = new MarketDto();
        marketDto.setId(appMarket.getId());
        marketDto.setName(appMarket.getName());
        marketDto.setPackageName(appMarket.getPackageName());
        marketDto.setLogoUrl(appMarket.getLogoUrl());
        marketDto.setCreateTime(appMarket.getCreateTime());
        marketDto.setUpdateTime(appMarket.getUpdateTime());
        return marketDto;
    }

    private List<MarketDto> convert2DtoList(List<AppMarket> marketList){
        List<MarketDto> dtoList = new ArrayList<>();
        if (marketList != null && !marketList.isEmpty()) {
            Iterator<AppMarket> iterator = marketList.iterator();
            while (iterator.hasNext()) {
                AppMarket appMarket = iterator.next();
                dtoList.add(convert2Dto(appMarket));
            }
        }
        return dtoList;
    }
}
