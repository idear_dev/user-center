/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.mapper.WechatMapper;
import com.idear.user.center.core.po.Wechat;
import com.idear.user.center.service.dto.WechatDto;
import com.idear.user.center.service.facade.WechatService;
import com.idear.user.center.service.message.ServiceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author ShanCc
 * @date 2018/11/6
 */
@Service
public class WechatServiceImpl implements WechatService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Resource
    private WechatMapper wechatMapper;

    /**
     * 根据memberId获取微信信息
     *
     * @param memberId
     * @return
     */
    @Override
    public Response<WechatDto> getWechatDtoByMemberId(final Integer memberId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Wechat wechat = wechatMapper.getWechatByMemberId(memberId);
                if (wechat == null) {
                    return Response.SUCCESS;
                }
                return new Response(convert2Dto(wechat));
            }
        });
    }

    /**
     * 微信用户关注公众号
     *
     * @param wechatDto
     * @return
     */
    @Override
    public Response subscribe(final WechatDto wechatDto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String openId = wechatDto.getOpenid();
                Wechat wechat = wechatMapper.getWechatByOpenid(openId);
                if (wechat == null) {
                    wechat = new Wechat();
                    setValue(wechat, wechatDto);
                    wechatMapper.saveWechat(wechat);
                } else {
                    setValue(wechat, wechatDto);
                    wechatMapper.updateWechat(wechat);
                }
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 绑定会员信息
     *
     * @param memberId
     * @param openId
     * @return
     */
    @Override
    public Response bindMember(final Integer memberId, final String openId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Wechat wechat = wechatMapper.getWechatByOpenid(openId);
                //已绑定微信，暂时不允许更换
                Integer memberId = wechat.getMemberId();
                if (memberId != null) {
                    wechat.setMemberId(memberId);
                    wechatMapper.bindWechat(wechat);
                } else {
                    throw new BusinessException(ServiceMessage.WECHAT_HAS_BIND_MEMBER);
                }
                return Response.SUCCESS;
            }
        });
    }

    private void setValue(Wechat wechat, WechatDto wechatDto) {
        wechat.setOpenid(wechatDto.getOpenid());
        wechat.setCity(wechatDto.getCity());
        wechat.setCountry(wechatDto.getCountry());
        wechat.setHeadImgUrl(wechatDto.getHeadImgUrl());
        wechat.setLanguage(wechatDto.getLanguage());
        wechat.setNickname(wechatDto.getNickname());
        wechat.setProvince(wechatDto.getProvince());
        wechat.setSex(wechatDto.getSex());
        wechat.setSubscribeTime(wechatDto.getSubscribeTime());
        wechat.setUnionId(wechatDto.getUnionId());
    }

    private WechatDto convert2Dto(Wechat wechat) {
        WechatDto wechatDto = new WechatDto();
        wechatDto.setId(wechat.getId());
        wechatDto.setCity(wechat.getCity());
        wechatDto.setCountry(wechat.getCountry());
        wechatDto.setProvince(wechat.getProvince());
        wechatDto.setHeadImgUrl(wechat.getHeadImgUrl());
        wechatDto.setLanguage(wechat.getLanguage());
        wechatDto.setNickname(wechat.getNickname());
        wechatDto.setMemberId(wechat.getMemberId());
        wechatDto.setOpenid(wechat.getOpenid());
        wechatDto.setUnionId(wechat.getUnionId());
        wechatDto.setSex(wechat.getSex());
        wechatDto.setSubscribeTime(wechat.getSubscribeTime());
        wechatDto.setCreateTime(wechat.getCreateTime());
        wechatDto.setUpdateTime(wechat.getUpdateTime());
        return wechatDto;
    }
}
