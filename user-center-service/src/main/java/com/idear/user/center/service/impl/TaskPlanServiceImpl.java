/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.PageQueryUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.core.enums.PlanStatus;
import com.idear.user.center.core.mapper.AppInfoMapper;
import com.idear.user.center.core.mapper.MarketMapper;
import com.idear.user.center.core.mapper.TaskPlanMapper;
import com.idear.user.center.core.po.AppInfo;
import com.idear.user.center.core.po.AppMarket;
import com.idear.user.center.core.po.TaskPlan;
import com.idear.user.center.service.dto.TaskPlanDto;
import com.idear.user.center.service.dto.TaskPlanListDto;
import com.idear.user.center.service.dto.Tutorial;
import com.idear.user.center.service.facade.TaskPlanService;
import com.idear.user.center.service.message.ServiceMessage;
import com.idear.user.center.service.param.TaskPlanListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
@Service
public class TaskPlanServiceImpl implements TaskPlanService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Resource
    private TaskPlanMapper taskPlanMapper;

    @Resource
    private AppInfoMapper appInfoMapper;

    @Resource
    private MarketMapper marketMapper;

    /**
     * 查询任务列表
     *
     * @param param
     * @return
     */
    @Override
    public Response<TaskPlanListDto> queryTaskPlan(TaskPlanListParam param) {
        Map<String, Object> paramMap = new HashMap<>(8);
        Integer pageNo, pageSize;
        if (param.getPageNo() == null) {
            pageNo = PageQueryUtil.DEFAULT_PAGE_NO;
        }else {
            pageNo = param.getPageNo();
        }
        if (param.getPageSize() == null || param.getPageSize() > PageQueryUtil.DEFAULT_MAX_PAGE_SIZE) {
            pageSize = PageQueryUtil.DEFAULT_PAGE_SIZE;
        }else {
            pageSize = param.getPageSize();
        }
        if (StringUtil.isNotEmpty(param.getName())){
            paramMap.put("planName",param.getName());
        }
        paramMap.put("beginTime",null);
        paramMap.put("endTime",null);
        if (param.getBeginTime() != null){
            paramMap.put("beginTime",param.getBeginTime());
        }
        if (param.getEndTime() != null){
            paramMap.put("endTime",param.getEndTime());
        }
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        PageList<TaskPlan> pageList = taskPlanMapper.listTaskPlan(paramMap, pageBounds);
        if (pageList != null || !pageList.isEmpty()) {
            TaskPlanListDto taskPlanListDto = new TaskPlanListDto();
            Integer totalNum = pageList.getPaginator().getTotalCount();
            Integer totalPage = pageList.getPaginator().getTotalPages();
            taskPlanListDto.setTotalNum(totalNum);
            taskPlanListDto.setTotalPage(totalPage);
            taskPlanListDto.setTaskPlanList(convert2DtoList(pageList));
            return new Response(taskPlanListDto);
        } else {
            return Response.SUCCESS;
        }
    }

    /**
     * 新增任务计划
     *
     * @param dto
     * @return
     */
    @Override
    public Response<Integer> addTaskPlan(final TaskPlanDto dto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                TaskPlan taskPlan = new TaskPlan();
                taskPlan.setAppId(dto.getAppId());
                taskPlan.setDescription(dto.getDescription());
                taskPlan.setMarketId(dto.getMarketId());
                taskPlan.setName(dto.getName());
                if (dto.getLabelList() != null && !dto.getLabelList().isEmpty()){
                    taskPlan.setLabel(JsonUtil.toJsonString(dto.getLabelList()));
                }
                taskPlan.setStartTime(dto.getStartTime());
                taskPlan.setEndTime(dto.getEndTime());
                taskPlan.setStatus(PlanStatus.ON_LINE.getCode());
                taskPlan.setTotalNum(dto.getTotalNum());
                if (dto.getTutorialList() != null && !dto.getTutorialList().isEmpty()){
                    taskPlan.setTutorial(JsonUtil.toJsonString(dto.getTutorialList()));
                }
                taskPlan.setType(dto.getType());
                if (StringUtil.isNotBlank(dto.getTypeInfo())){
                    taskPlan.setTypeInfo(dto.getTypeInfo());
                }
                Date nowDate = new Date();
                taskPlan.setCreateTime(nowDate);
                taskPlan.setUpdateTime(nowDate);
                taskPlanMapper.saveTaskPlan(taskPlan);
                return new Response(taskPlan.getId());
            }
        });
    }


    /**
     * 更新任务计划
     *
     * @param dto
     * @return
     */
    @Override
    public Response updateTaskPlan(final TaskPlanDto dto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer planId = dto.getId();
                if (planId == null) {
                    throw new BusinessException(ServiceMessage.TASK_PLAN_ID_IS_NULL);
                }
                TaskPlan taskPlan = taskPlanMapper.getTaskPlanById(dto.getId());
                if (taskPlan == null) {
                    throw new BusinessException(ServiceMessage.TASK_PLAN_INFO_NOT_EXIST);
                }
                if (dto.getAppId() != null) {
                    taskPlan.setAppId(dto.getAppId());
                }
                if (dto.getMarketId() != null) {
                    taskPlan.setMarketId(dto.getMarketId());
                }
                if (StringUtil.isNotEmpty(dto.getName())) {
                    taskPlan.setName(dto.getName());
                }
                if (StringUtil.isNotEmpty(dto.getDescription())) {
                    taskPlan.setDescription(dto.getDescription());
                }
                if (dto.getTutorialList() != null && !dto.getTutorialList().isEmpty()) {
                    taskPlan.setTutorial(JsonUtil.toJsonString(dto.getTutorialList()));
                }
                if (dto.getLabelList() != null && !dto.getLabelList().isEmpty()) {
                    taskPlan.setLabel(JsonUtil.toJsonString(dto.getLabelList()));
                }
                if (dto.getType() != null){
                    taskPlan.setType(dto.getType());
                }
                if (StringUtil.isNotEmpty(dto.getTypeInfo())){
                    taskPlan.setTypeInfo(dto.getTypeInfo());
                }
                if (dto.getStartTime() != null) {
                    taskPlan.setStartTime(dto.getStartTime());
                }
                if (dto.getEndTime() != null) {
                    taskPlan.setEndTime(dto.getEndTime());
                }
                if (dto.getTotalNum() != null) {
                    taskPlan.setTotalNum(dto.getTotalNum());
                }
                if (dto.getStatus() != null) {
                    taskPlan.setStatus(dto.getStatus());
                }
                taskPlanMapper.updateTaskPlan(taskPlan);
                return Response.SUCCESS;
            }
        });
    }

    private TaskPlanDto convert2Dto(TaskPlan taskPlan) {
        Integer appId = taskPlan.getAppId();
        Integer marketId = taskPlan.getMarketId();
        AppInfo appInfo = appInfoMapper.getAppInfoById(appId);
        AppMarket appMarket = marketMapper.getMarket(marketId);
        TaskPlanDto taskPlanDto = new TaskPlanDto();
        taskPlanDto.setId(taskPlan.getId());
        taskPlanDto.setAppName(appInfo.getAppName());
        taskPlanDto.setAppId(appId);
        taskPlanDto.setAppLogoUrl(appInfo.getAppLogoUrl());
        taskPlanDto.setMarketId(marketId);
        taskPlanDto.setMarketName(appMarket.getName());
        taskPlanDto.setMarketLogoUrl(appMarket.getLogoUrl());
        taskPlanDto.setName(taskPlan.getName());
        taskPlanDto.setDescription(taskPlan.getDescription());
        taskPlanDto.setStartTime(taskPlan.getStartTime());
        taskPlanDto.setEndTime(taskPlan.getEndTime());
        if (StringUtil.isNotEmpty(taskPlan.getLabel())){
            taskPlanDto.setLabelList(JsonUtil.parseObject(taskPlan.getLabel(),ArrayList.class));
        }
        if (StringUtil.isNotEmpty(taskPlan.getTutorial())){
            taskPlanDto.setTutorialList(JsonUtil.parseArray(taskPlan.getTutorial(),Tutorial.class));
        }
        taskPlanDto.setType(taskPlan.getType());
        taskPlanDto.setTypeInfo(taskPlan.getTypeInfo());
        taskPlanDto.setTotalNum(taskPlan.getTotalNum());
        taskPlanDto.setStatus(taskPlan.getStatus());
        taskPlanDto.setCreateTime(taskPlan.getCreateTime());
        taskPlanDto.setUpdateTime(taskPlan.getUpdateTime());
        return taskPlanDto;
    }

    private List<TaskPlanDto> convert2DtoList(List<TaskPlan> taskPlanList) {
        List<TaskPlanDto> dtoList = new ArrayList<>();
        if (taskPlanList != null && !taskPlanList.isEmpty()) {
            Iterator<TaskPlan> iterator = taskPlanList.iterator();
            while (iterator.hasNext()) {
                TaskPlan taskPlan = iterator.next();
                dtoList.add(convert2Dto(taskPlan));
            }
        }
        return dtoList;
    }
}
