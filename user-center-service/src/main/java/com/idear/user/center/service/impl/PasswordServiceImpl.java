/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.Md5Util;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.user.center.config.MemberConfig;
import com.idear.user.center.core.dao.MemberDao;
import com.idear.user.center.core.enums.MemberStatus;
import com.idear.user.center.core.po.Member;
import com.idear.user.center.service.message.ServiceMessage;
import com.idear.user.center.service.facade.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author CcShan
 * @date 2017/12/20
 */
@Service
public class PasswordServiceImpl implements PasswordService {

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private MemberConfig memberConfig;

    @Autowired
    private BusinessTemplate businessTemplate;

    /**
     * 验证登录密码并维护redis信息
     *
     * @param mobileNo
     * @param password
     * @return
     */
    @Override
    public Response validateLoginPassword(final String mobileNo, final String password) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Member member = memberDao.getMemberByMobileNo(mobileNo);
                if (member == null) {
                    throw new BusinessException(ServiceMessage.MOBILE_NO_NOT_REGISTRY);
                } else if (MemberStatus.DISABLE.getCode().equals(member.getStatus())) {
                    throw new BusinessException(ServiceMessage.MOBILE_NO_IS_DISABLE);
                } else if (!Md5Util.md5(password).equals(member.getLoginPassword())){
                    throw new BusinessException(ServiceMessage.PASSWORD_OR_MOBILE_ERROR);
                }
                String token = memberDao.generateLoginToken(member, memberConfig.getTokenDelayTime());
                return new Response(token);
            }
        });
    }
}
