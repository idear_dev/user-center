package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.mapper.ManagerMapper;
import com.idear.user.center.core.po.Manager;
import com.idear.user.center.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2017/9/21
 */
@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerMapper managerMapper;

    @Override
    public Manager getManagerByEmail(String email) {
        return managerMapper.getManagerByEmail(email);
    }


    @Override
    public void updateManager(Manager manager) {
        managerMapper.updateManager(manager);
    }

    @Override
    public void addManager(Manager manager) {
        managerMapper.addManager(manager);
    }

    @Override
    public PageList<Manager> getManagerList(String name, Integer roleId, PageBounds pageBounds) {
        Map<String, Object> map = new HashMap<>();
        map.put("managerName", name);
        map.put("roleId", roleId);
        return managerMapper.getManagerList(map, pageBounds);
    }

    @Override
    public List<Manager> getManagerListByRoleIds(List<Integer> roleIdList) {
        return managerMapper.getManagerListByRoleIds(roleIdList);
    }

    @Override
    public Manager getAllManagerByEmail(String email) {
        return managerMapper.getAllManagerByEmail(email);
    }

    @Override
    public Manager getManagerById(Integer id) {
        return managerMapper.getManagerById(id);
    }
}
