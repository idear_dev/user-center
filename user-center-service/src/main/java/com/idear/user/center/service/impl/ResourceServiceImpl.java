package com.idear.user.center.service.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.enums.DelFlag;
import com.idear.user.center.core.mapper.ResourceMapper;
import com.idear.user.center.core.po.Resource;
import com.idear.user.center.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 *
 * @author CcShan
 * @date 2017/9/22
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceMapper resourceMapper;

    @Override
    public List<Resource> getResourceListById(List<Integer> resourceIdSet) {
        return resourceMapper.getResourceListById(resourceIdSet);
    }

    @Override
    public int addResource(Resource resource) {
        return resourceMapper.insertSelective(resource);
    }

    @Override
    public int editResource(Resource resource) {
        return resourceMapper.updateByPrimaryKeySelective(resource);
    }

    @Override
    public int deleteResource(Integer id) {
        Resource resource = new Resource();
        resource.setId(id);
        resource.setDelFlag(DelFlag.DELETED.getCode());
        resource.setUpdateTime(new Date());
        return resourceMapper.updateByPrimaryKeySelective(resource);
    }

    @Override
    public PageList<Resource> queryResourceList(Resource resource, Integer pageNo, Integer pageSize) {
        PageBounds pageBounds = new PageBounds(pageNo,pageSize);
        return resourceMapper.queryResourceList(resource,pageBounds);
    }

    @Override
    public List<Resource> queryAllResourceList() {
        return resourceMapper.queryResourceList();
    }

    @Override
    public Resource queryAllResourceByName(String name) {
        return resourceMapper.queryAllResourceByName(name);
    }
}
