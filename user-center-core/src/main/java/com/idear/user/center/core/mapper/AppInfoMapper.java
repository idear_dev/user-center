/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.AppInfo;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public interface AppInfoMapper {

    /**
     * 根据appId列表查询app信息
     * @param idList
     * @return
     */
    List<AppInfo> getAppInfoListByIds(List<Integer> idList);

    /**
     * 产看app信息
     * @param appId
     * @return
     */
    AppInfo getAppInfoById(Integer appId);

    /**
     * 保存产品信息
     * @param appInfo
     */
    void saveAppInfo(AppInfo appInfo);

    /**
     * 根据参数分页查询AppInfo
     * @param param
     * @param pageBounds
     * @return
     */
    PageList<AppInfo> getAppInfoList(AppInfo param, PageBounds pageBounds);

    /**
     * 更新App信息
     * @param appInfo
     */
    void updateAppInfo(AppInfo appInfo);
}
