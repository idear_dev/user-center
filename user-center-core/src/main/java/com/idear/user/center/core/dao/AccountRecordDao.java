/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.AccountRecord;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public interface AccountRecordDao {

    /**
     * 计算所有收入
     * @param memberId
     * @return
     */
    Integer sumTotalIncome(Integer memberId);

    /**
     * 计算今日收入
     * @param memberId
     * @return
     */
    Integer sumTodayIncome(Integer memberId);

    /**
     * 可以提现收入
     * @param memberId
     * @return
     */
    Integer sumCanWithdrawAmount(Integer memberId);

    /**
     * 计算已经提现收入
     * @param memberId
     * @return
     */
    Integer sumHasWithdrawAmount(Integer memberId);

    /**
     * 计算出账中的金额
     * @param memberId
     * @return
     */
    Integer sumWithdrawingAmount(Integer memberId);

    /**
     * 计算入账中的金额
     * @param memberId
     * @return
     */
    Integer sumImportingAmount(Integer memberId);

    /**
     * 分页获取收入明细
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageList<AccountRecord> queryIncomeList(Integer memberId,Integer pageNo,Integer pageSize);

    /**
     * 分页获取提现成功或者失败列表
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageList<AccountRecord> queryHasWithdrawList(Integer memberId,Integer pageNo,Integer pageSize);

    /**
     * 分页获取提现审核中列表
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageList<AccountRecord> queryWithdrawingList(Integer memberId,Integer pageNo,Integer pageSize);

    /**
     * 按照serialNo查询账户记录
     * @param memberId
     * @param serialNo
     * @return
     */
    AccountRecord queryAccountRecord(Integer memberId,String serialNo);
}
