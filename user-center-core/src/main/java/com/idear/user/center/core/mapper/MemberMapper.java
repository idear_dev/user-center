/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.Member;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
public interface MemberMapper {

    /**
     * 根据手机号查询会员信息
     * @param mobileNo
     * @return
     */
    Member getMemberByMobileNo(String mobileNo);

    /**
     * 更具会员Id查询会员信息
     * @param memberId
     * @return
     */
    Member getMemberById(Integer memberId);

    /**
     * 保存会员信息
     * @param member
     */
    void saveMember(Member member);

    /**
     * 更新会员信息
     * @param member
     */
    void updateMember(Member member);
}
