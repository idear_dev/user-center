/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.dao;

import com.idear.user.center.core.po.Member;

/**
 *
 * @author CcShan
 * @date 2017/12/20
 */
public interface MemberDao {

    /**
     * 按照手机号查询用户信息
     *
     * @param mobileNo
     * @return
     */
    Member getMemberByMobileNo(String mobileNo);

    /**
     * 根据Token获取用户信息
     * @param token
     * @return
     */
    Member getMemberByToken(String token);

    /**
     * 根据MemberId获取会员信息
     * @param memberId
     * @return
     */
    Member getMemberById(Integer memberId);

    /**
     * 生成登录Token，并缓存用户信息
     *
     * @param member
     * @param delayTime token有效期
     * @return
     */
    String generateLoginToken(Member member, Long delayTime);

    /**
     * 检查Token是否过期
     *
     * @param token
     * @param delayTime
     * @return
     */
    boolean checkToken(String token, Long delayTime);

    /**
     * 登出
     * @param token
     */
    void logout(String token);

    /**
     * 保存会员信息
     * @param member
     */
    void saveMember(Member member);

    /**
     * 更新会员信息
     * @param member
     */
    void updateMember(Member member);
}
