/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.enums;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
public enum Sex {

    /**
     * 男性
     */
    MALE(1, "男"),

    /**
     * 女性
     */
    FEMALE(2, "女"),

    /**
     * 未知行吧
     */
    UNKNOWN(0, "未知");

    private Integer code;

    private String desc;

    Sex(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 根据code获取性别枚举
     * @param code
     * @return
     */
    public static Sex getSex(Integer code){
        Sex[] sexes = Sex.values();
        for (Sex sex:sexes){
            if (sex.getCode().equals(code)){
                return sex;
            }
        }
        return Sex.UNKNOWN;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
