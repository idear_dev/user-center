/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.po;

import java.util.Date;

/**
 * 会员账户记录
 * Created By CcShan on 2017/12/24.
 * @author Administrator
 */
public class AccountRecord extends AbstractPo{

    /**
     * 账户Id
     */
    private Integer accountId;

    /**
     * 会员编号
     */
    private Integer memberId;

    /**
     * 账户记录流水号
     */
    private String serialNo;

    /**
     * 类型 1返现 2提现
     */
    private Integer recordType;

    /**
     * 金额（分）
     */
    private Integer amount;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 出入账标记 1入账 9出账
     */
    private Integer inOutFlag;

    /**
     * 记录时间
     */
    private Date recordTime;

    /**
     * 状态
     */
    private Integer status;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Integer getRecordType() {
        return recordType;
    }

    public void setRecordType(Integer recordType) {
        this.recordType = recordType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getInOutFlag() {
        return inOutFlag;
    }

    public void setInOutFlag(Integer inOutFlag) {
        this.inOutFlag = inOutFlag;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
