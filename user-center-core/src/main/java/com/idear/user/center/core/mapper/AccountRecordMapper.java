/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.AccountRecord;

import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public interface AccountRecordMapper {

    /**
     * 计算所有入账成功
     * @param memberId
     * @return
     */
    Integer sumTotalIncome(Integer memberId);

    /**
     * 计算今日入账成功
     * @param params
     * @return
     */
    Integer sumTodayIncome(Map<String,Object> params);

    /**
     * 计算所有出账中+出账成功
     * @param memberId
     * @return
     */
    Integer sumTotalWithdrawAmount(Integer memberId);

    /**
     * 计算已经成功提现金额
     * @param memberId
     * @return
     */
    Integer sumHasWithdrawAmount(Integer memberId);

    /**
     * 提现审核中的总金额
     * @param memberId
     * @return
     */
    Integer sumWithdrawingAmount(Integer memberId);

    /**
     * 入账中的中金额
     * @param memberId
     * @return
     */
    Integer sumImportingAmount(Integer memberId);

    /**
     * 查询收入列表
     * @param memberId
     * @param pageBounds
     * @return
     */
    PageList<AccountRecord> queryIncomeList(Integer memberId, PageBounds pageBounds);

    /**
     * 查询已经成功提现记录列表
     * @param memberId
     * @param pageBounds
     * @return
     */
    PageList<AccountRecord> queryHasWithdrawList(Integer memberId, PageBounds pageBounds);

    /**
     * 查询提现审核中的记录列表
     * @param memberId
     * @param pageBounds
     * @return
     */
    PageList<AccountRecord> queryWithdrawingList(Integer memberId, PageBounds pageBounds);

    /**
     * 查询提现明细
     * @param params
     * @return
     */
    AccountRecord queryRecord(Map<String,Object> params);

    /**
     * 生成记录
     * @param accountRecord
     */
    void saveAccountRecord(AccountRecord accountRecord);
}
