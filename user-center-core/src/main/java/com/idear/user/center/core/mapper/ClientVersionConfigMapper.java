/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.ClientVersionConfig;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/8/19
 */
public interface ClientVersionConfigMapper {

    /**
     * 保存版本信息
     * @param clientVersionConfig
     */
    void saveConfig(ClientVersionConfig clientVersionConfig);

    /**
     * 按照渠道信息获取版本信息
     * @param param
     * @return
     */
    List<ClientVersionConfig> getConfigListByChannel(Map param);

    /**
     * 按照渠道信息获取版本信息
     * @param pageBounds
     * @return
     */
    PageList<ClientVersionConfig> getConfigList(PageBounds pageBounds);

    /**
     * 更新版本配置信息
     * @param clientVersionConfig
     */
    void updateConfig(ClientVersionConfig clientVersionConfig);

    /**
     * 根据Id查询配置信息
     * @param id
     * @return
     */
    ClientVersionConfig getConfig(Integer id);
}
