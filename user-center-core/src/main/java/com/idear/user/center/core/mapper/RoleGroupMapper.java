package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.RoleGroup;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by CcShan on 2017/9/26.
 */
@Repository
public interface RoleGroupMapper {

    RoleGroup getRoleGroupByName(String groupName);

    void addRoleGroup(RoleGroup roleGroup);

    void updateGroupStatus(Map<String, Object> map);

    void updateGroupName(Map<String, Object> map);

    List<RoleGroup> getRoleGroupByStatus(Map<String, Object> map);
}
