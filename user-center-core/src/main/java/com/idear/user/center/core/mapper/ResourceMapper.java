package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.Resource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by CcShan on 2017/9/22.
 */
@Repository
public interface ResourceMapper {

    List<Resource> getResourceListById(List<Integer> resourceIdSet);

    int insertSelective(Resource record);

    int updateByPrimaryKeySelective(Resource record);

    PageList<Resource> queryResourceList(Resource resource, PageBounds pageBounds);

    List<Resource> queryResourceList();

    Resource queryAllResourceByName(String name);
}
