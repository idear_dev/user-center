/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.TaskPlan;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public interface TaskPlanMapper {

    /**
     * 根据Id查看计划列表
     * @param idList
     * @return
     */
    List<TaskPlan> getTaskPlanListByIds(List<Integer> idList);

    /**
     * 查询计划信息
     * @param taskId
     * @return
     */
    TaskPlan getTaskPlanById(Integer taskId);

    /**
     * 获取任务列表
     * @param param
     * @param pageBounds
     * @return
     */
    PageList<TaskPlan> listTaskPlan(Map<String,Object> param, PageBounds pageBounds);

    /**
     * 新增任务记录
     * @param taskPlan
     */
    void saveTaskPlan(TaskPlan taskPlan);

    /**
     * 更新任务计划
     * @param taskPlan
     */
    void updateTaskPlan(TaskPlan taskPlan);
}
