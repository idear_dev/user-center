package com.idear.user.center.core.enums;

/**
 * @author wenqx
 * @desc 删除标志
 * @date 2017年9月27日
 */
public enum DelFlag {

    NORMAL(1, "未删除"),
    DELETED(0, "已删除");

    DelFlag(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private Integer code;

    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
