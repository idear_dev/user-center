package com.idear.user.center.core.enums;

/**
 * Created by CcShan on 2017/9/19.
 */
public enum ManagerStatus {

    CLOSED(0, "关闭"),
    OPENED(1, "打开");

    ManagerStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private Integer status;

    private String desc;

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
