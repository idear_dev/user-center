/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.TaskDetail;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public interface TaskDetailMapper {

    /**
     * 查询在线的任务列表
     * @param param
     * @param pageBounds
     * @return
     */
    PageList<TaskDetail> getOnlineTaskList(Map<String,Object> param, PageBounds pageBounds);

    /**
     * 查询任务详情
     * @param taskId
     * @return
     */
    TaskDetail getTaskDetail(Integer taskId);

    /**
     * 查询任务详情
     * @param planId
     * @return
     */
    List<TaskDetail> getTaskDetailByPlanId(Integer planId);

    /**
     * 保存taskDetail
     * @param taskDetail
     */
    void saveTaskDetail(TaskDetail taskDetail);

    /**
     * 保存taskDetail
     * @param taskDetail
     */
    void updateTaskDetail(TaskDetail taskDetail);

    /**
     * 增加领取任务数
     * @param taskId
     */
    void increaseReceivedNum(Integer taskId);

    /**
     * 减少领取任务数
     * @param taskId
     */
    void reduceReceivedNum(Integer taskId);

}
