package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by CcShan on 2017/9/19.
 */
@Repository
public interface RoleMapper {

    PageList<Role> getRoleList(PageBounds pageBounds);

    List<Role> getRoleListById(List<Integer> roleIdList);

    void addRole(Role role);

    void updateRole(Role role);

    List<Role> getRoleListByGroupId(Integer groupId);

    List<Role> getRoleList();
}
