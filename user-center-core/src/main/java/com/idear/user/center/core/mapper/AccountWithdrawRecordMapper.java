/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.AccountWithdrawRecord;

import java.util.Map;

/**
 * Created By CcShan on 2018/1/4.
 * @author Administrator
 */
public interface AccountWithdrawRecordMapper {

    /**
     * 查询提现明细
     * @param serialNo
     * @return
     */
    AccountWithdrawRecord queryRecord(String serialNo);

    /**
     * 保存提现明细
     * @param record
     */
    void saveRecord(AccountWithdrawRecord record);

    /**
     * 获取记录列表
     * @param param
     * @param pageBounds
     * @return
     */
    PageList<AccountWithdrawRecord> getRecordList(Map<String,Object> param, PageBounds pageBounds);
}
