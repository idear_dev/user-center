package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by CcShan on 2017/9/22.
 */
@Repository
public interface MenuMapper {

    List<Menu> getMenuListById(List<Integer> menuIdList);

    List<Menu> getMenuList();

    int insertSelective(Menu record);

    int updateByPrimaryKeySelective(Menu record);

    int deleteByPrimaryKey(Integer id);

    List<Menu> queryMenuList(Menu menu);

    Menu queryAllMenuByName(String name);

}
