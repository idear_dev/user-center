/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.AppMarket;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/10/24
 */
public interface MarketMapper {

    /**
     * 查询所有market
     * @return
     */
    List<AppMarket> getMarketList();

    /**
     * 保存market信息
     * @param market
     */
    void saveMarket(AppMarket market);

    /**
     * 更新market信息
     * @param market
     */
    void updateMarket(AppMarket market);

    /**
     * 查询market详情
     * @param id
     * @return
     */
    AppMarket getMarket(Integer id);
}
