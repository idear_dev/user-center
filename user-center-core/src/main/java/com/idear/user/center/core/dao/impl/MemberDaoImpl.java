/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.dao.impl;

import com.idear.common.util.JsonUtil;
import com.idear.common.util.Md5Util;
import com.idear.common.util.StringUtil;
import com.idear.user.center.core.constants.CacheConstant;
import com.idear.user.center.core.dao.MemberDao;
import com.idear.user.center.core.mapper.MemberMapper;
import com.idear.user.center.core.po.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author CcShan
 * @date 2017/12/20
 */
@Component
public class MemberDaoImpl implements MemberDao {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Resource
    private MemberMapper memberMapper;

    /**
     * 按照手机号查询用户信息
     *
     * @param mobileNo
     * @return
     */
    @Override
    public Member getMemberByMobileNo(String mobileNo){
        return memberMapper.getMemberByMobileNo(mobileNo);
    }

    /**
     * 根据Token获取用户信息
     *
     * @param token
     * @return
     */
    @Override
    public Member getMemberByToken(String token) {
        String memberId = redisTemplate.opsForValue().get(CacheConstant.USER_TOKEN_INDEX+token);
        if (StringUtil.isEmpty(memberId)){
            return null;
        }else {
            String value = redisTemplate.opsForValue().get(CacheConstant.USER_MEMBER_INDEX+memberId);
            if (StringUtil.isEmpty(value)){
                return memberMapper.getMemberById(Integer.valueOf(memberId));
            }else {
                return JsonUtil.parseObject(value,Member.class);
            }
        }
    }

    /**
     * 根据memberId获取会员信息
     *
     * @param memberId
     * @return
     */
    @Override
    public Member getMemberById(Integer memberId) {
        String value = redisTemplate.opsForValue().get(CacheConstant.USER_MEMBER_INDEX+memberId);
        if (StringUtil.isEmpty(value)){
            return memberMapper.getMemberById(Integer.valueOf(memberId));
        }else {
            return JsonUtil.parseObject(value,Member.class);
        }
    }


    /**
     * 生成登录Token，并缓存用户信息
     *
     * @param member
     * @return
     */
    @Override
    public String generateLoginToken(Member member,Long delayTime) {
        String value = JsonUtil.toJsonString(member);
        String token = Md5Util.md5(value+System.currentTimeMillis());
        Integer memberId = member.getId();
        redisTemplate.opsForValue().set(CacheConstant.USER_TOKEN_INDEX+token,memberId.toString(),delayTime, TimeUnit.SECONDS);
        redisTemplate.opsForValue().set(CacheConstant.USER_MEMBER_INDEX+memberId,value,delayTime, TimeUnit.SECONDS);
        return token;
    }


    /**
     * 检查Token是否过期
     *
     * @param token
     * @param delayTime
     * @return
     */
    @Override
    public boolean checkToken(String token, Long delayTime) {
        String memberId = redisTemplate.opsForValue().get(CacheConstant.USER_TOKEN_INDEX+token);
        if (!StringUtil.isEmpty(memberId)){
            redisTemplate.expire(CacheConstant.USER_TOKEN_INDEX+token,delayTime, TimeUnit.SECONDS);
            redisTemplate.expire(CacheConstant.USER_MEMBER_INDEX+memberId,delayTime, TimeUnit.SECONDS);
            return true;
        }
        return false;
    }

    /**
     * 登出
     *
     * @param token
     */
    @Override
    public void logout(String token) {
        String memberId = redisTemplate.opsForValue().get(CacheConstant.USER_TOKEN_INDEX+token);
        if (!StringUtil.isEmpty(memberId)){
            redisTemplate.delete(CacheConstant.USER_TOKEN_INDEX+token);
            redisTemplate.delete(CacheConstant.USER_MEMBER_INDEX+memberId);
        }
    }

    /**
     * 保存会员信息
     *
     * @param member
     */
    @Override
    public void saveMember(Member member) {
        memberMapper.saveMember(member);
    }

    /**
     * 更新会员信息
     *
     * @param member
     */
    @Override
    public void updateMember(Member member) {
        memberMapper.updateMember(member);
    }

}
