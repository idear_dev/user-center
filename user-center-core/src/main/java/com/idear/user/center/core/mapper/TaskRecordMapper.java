/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.MemberTaskRecord;

import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/9/17
 */
public interface TaskRecordMapper {

    /**
     * 根据TaskId查询任务记录
     * @param param
     * @return
     */
    List<MemberTaskRecord> getTaskRecordByPlanId(MemberTaskRecord param);

    /**
     * 保存领取记录
     * @param memberTaskRecord
     */
    void saveTaskRecord(MemberTaskRecord memberTaskRecord);

    /**
     * 根据记录Id查询记录信息
     * @param param
     * @return
     */
    MemberTaskRecord getTaskRecordById(MemberTaskRecord param);

    /**
     * 更新任务记录状态
     * @param param
     */
    void updateTaskRecord(MemberTaskRecord param);

    /**
     * 提交审核信息
     * @param param
     */
    void submitTaskRecordAuditInfo(MemberTaskRecord param);

    /**
     * 查询任务记录列表
     * @param param
     * @param pageBounds
     * @return
     */
    PageList<MemberTaskRecord> getTaskRecordList(Map param, PageBounds pageBounds);
}
