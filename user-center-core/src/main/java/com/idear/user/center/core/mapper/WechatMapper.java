/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.Wechat;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
public interface WechatMapper {

    /**
     * 按照memberId查询微信信息
     * @param memberId
     * @return
     */
    Wechat getWechatByMemberId(Integer memberId);

    /**
     * 根据Openid查询微信信息
     * @param openid
     * @return
     */
    Wechat getWechatByOpenid(String openid);

    /**
     * 保存微信信息
     * @param wechat
     */
    void saveWechat(Wechat wechat);

    /**
     * 更新微信信息
     * @param wechat
     */
    void updateWechat(Wechat wechat);
    /**
     * 绑定微信
     * @param wechat
     */
    void bindWechat(Wechat wechat);
}
