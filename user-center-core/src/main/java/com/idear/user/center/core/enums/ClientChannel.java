/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.enums;

/**
 * @author ShanCc
 * @date 2018/8/19
 */
public enum ClientChannel {

    /**
     * 未知
     */
    UNKNOWN("UNKNOWN"),

    /**
     * ios
     */
    IOS("IOS"),

    /**
     * android
     */
    ANDROID("ANDROID");

    private String value;

    ClientChannel(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 根据value获取指定的ClientChannel
     * @param value
     * @return
     */
    public static ClientChannel getChannel(String value) {
        ClientChannel[] clientChannels = ClientChannel.values();
        for (ClientChannel clientChannel : clientChannels) {
            if (clientChannel.getValue().equals(value)){
                return clientChannel;
            }
        }
        return ClientChannel.UNKNOWN;
    }
}
