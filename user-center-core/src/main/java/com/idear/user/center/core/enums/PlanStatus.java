/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.enums;

/**
 * @author ShanCc
 * @date 2018/9/14
 */
public enum PlanStatus {

    /**
     * 上线
     */
    ON_LINE(1, "上线"),

    /**
     * 下线
     */
    OFF_LINE(2, "下线");

    private Integer code;

    private String desc;

    PlanStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
