/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.dao.impl;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.constants.MapConstant;
import com.idear.user.center.core.dao.AccountRecordDao;
import com.idear.user.center.core.mapper.AccountRecordMapper;
import com.idear.user.center.core.po.AccountRecord;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
@Component
public class AccountRecordDaoImpl implements AccountRecordDao {

    @Resource
    private AccountRecordMapper accountRecordMapper;

    /**
     * 计算所有收入
     *
     * @param memberId
     * @return
     */
    @Override
    public Integer sumTotalIncome(Integer memberId) {
        Integer amount = accountRecordMapper.sumTotalIncome(memberId);
        if (amount == null){
            return 0;
        }
        return accountRecordMapper.sumTotalIncome(memberId);
    }

    /**
     * 计算今日收入
     *
     * @param memberId
     * @return
     */
    @Override
    public Integer sumTodayIncome(Integer memberId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Map params = new HashMap(MapConstant.INITIALCAPACITY_16);
        Date nowDate = new Date();
        Date beginTime = null;
        Date endTime = null;
        try {
            beginTime = timeFormat.parse(dateFormat.format(nowDate) + " 00:00:01");
            endTime = timeFormat.parse(dateFormat.format(nowDate) + " 24:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        params.put("memberId", memberId);
        params.put("beginTime", beginTime);
        params.put("beginTime", endTime);
        Integer amount = accountRecordMapper.sumTodayIncome(params);
        if (amount == null){
            return 0;
        }
        return accountRecordMapper.sumTodayIncome(params);
    }

    /**
     * 可以提现收入
     *
     * @param memberId
     * @return
     */
    @Override
    public Integer sumCanWithdrawAmount(Integer memberId) {
        Integer totalIncome = accountRecordMapper.sumTotalIncome(memberId);
        Integer totalWithdrawAmount = accountRecordMapper.sumTotalWithdrawAmount(memberId);
        if (totalIncome == null){
            totalIncome = 0;
        }
        if (totalWithdrawAmount == null){
            totalWithdrawAmount = 0;
        }
        return totalIncome - totalWithdrawAmount;
    }

    /**
     * 计算已经成功提现收入
     *
     * @param memberId
     * @return
     */
    @Override
    public Integer sumHasWithdrawAmount(Integer memberId) {
        Integer amount = accountRecordMapper.sumHasWithdrawAmount(memberId);
        if (amount == null){
            return 0;
        }
        return accountRecordMapper.sumHasWithdrawAmount(memberId);
    }

    /**
     * 计算出账中的金额
     *
     * @param memberId
     * @return
     */
    @Override
    public Integer sumWithdrawingAmount(Integer memberId) {
        Integer amount = accountRecordMapper.sumWithdrawingAmount(memberId);
        if (amount == null){
            return 0;
        }
        return accountRecordMapper.sumWithdrawingAmount(memberId);
    }

    /**
     * 计算入账中的金额
     *
     * @param memberId
     * @return
     */
    @Override
    public Integer sumImportingAmount(Integer memberId) {
        Integer amount = accountRecordMapper.sumImportingAmount(memberId);;
        if (amount == null){
            return 0;
        }
        return accountRecordMapper.sumImportingAmount(memberId);
    }

    /**
     * 分页获取收入明细
     *
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageList<AccountRecord> queryIncomeList(Integer memberId, Integer pageNo, Integer pageSize) {
        PageBounds pageBounds = new PageBounds(pageNo,pageSize);
        return accountRecordMapper.queryIncomeList(memberId,pageBounds);
    }

    /**
     * 分页获取提现成功明细
     *
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageList<AccountRecord> queryHasWithdrawList(Integer memberId, Integer pageNo, Integer pageSize) {
        PageBounds pageBounds = new PageBounds(pageNo,pageSize);
        return accountRecordMapper.queryHasWithdrawList(memberId,pageBounds);
    }

    /**
     * 分页获取提现审核中列表
     *
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageList<AccountRecord> queryWithdrawingList(Integer memberId, Integer pageNo, Integer pageSize) {
        PageBounds pageBounds = new PageBounds(pageNo,pageSize);
        return accountRecordMapper.queryWithdrawingList(memberId,pageBounds);
    }

    /**
     * 按照serialNo查询账户记录
     *
     * @param memberId
     * @param serialNo
     * @return
     */
    @Override
    public AccountRecord queryAccountRecord(Integer memberId, String serialNo) {
        Map<String,Object> params = new HashMap<>(MapConstant.INITIALCAPACITY_16);
        params.put("memberId",memberId);
        params.put("serialNo",serialNo);
        return accountRecordMapper.queryRecord(params);
    }
}
