/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.enums;

/**
 * Created By CcShan on 2017/12/24.
 * @author Administrator
 */
public enum AccountRecordStatus {

    /**
     * 入账中
     */
    IN_ING(1, "入账中"),

    /**
     * 出账中
     */
    OUT_ING(2, "出账中"),

    /**
     * 交易成功
     */
    TRANS_SUCCEED(3,"交易成功"),

    /**
     * 交易失败
     */
    TRANS_FAILED(4,"交易失败");

    private Integer code;

    private String decs;

    AccountRecordStatus(Integer code, String desc) {
        this.code = code;
        this.decs = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDecs() {
        return decs;
    }

    public void setDecs(String decs) {
        this.decs = decs;
    }
}
