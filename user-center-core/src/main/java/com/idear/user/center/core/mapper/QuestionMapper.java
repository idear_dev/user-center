/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.Question;

import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/19
 */
public interface QuestionMapper {

    /**
     * 按照状态查看QA信息
     *
     * @param question
     * @return
     */
    List<Question> getQuestionListByStatus(Question question);

    /**
     * 新增QA信息
     *
     * @param question
     */
    void saveQuestion(Question question);

    /**
     * 更新QA信息
     * @param question
     */
    void updateQuestion(Question question);

    /**
     * 根据Id查询QA信息
     * @param id
     * @return
     */
    Question getQuestion(Integer id);
}
