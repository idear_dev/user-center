/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.enums;

/**
 * @author CcShan
 * @date 2018/9/2
 */
public enum BannerLocation {

    /**
     * 首页
     */
    HOME_PAGE(1,"首页");

    private Integer code;

    private String desc;

    BannerLocation(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 根据value获取指定的BannerLocation
     * @param code
     * @return
     */
    public static BannerLocation getLocation(Integer code) {
        BannerLocation[] locations = BannerLocation.values();
        for (BannerLocation bannerLocation : locations) {
            if (bannerLocation.getCode().equals(code)){
                return bannerLocation;
            }
        }
        return null;
    }
}
