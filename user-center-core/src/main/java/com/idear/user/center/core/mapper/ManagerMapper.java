package com.idear.user.center.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.user.center.core.po.Manager;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2017/9/21
 */
@Repository
public interface ManagerMapper {

    /**
     * 根据邮箱信息查询用户
     * @param email
     * @return
     */
    Manager getManagerByEmail(String email);

    /**
     * 更新用户信息
     * @param manager
     */
    void updateManager(Manager manager);

    /**
     * 添加管理员
     * @param manager
     */
    void addManager(Manager manager);

    /**
     * 获取管理员列表
     * @param map
     * @param pageBounds
     * @return
     */
    PageList<Manager> getManagerList(Map<String, Object> map, PageBounds pageBounds);

    /**
     * 按照角色信息查询用户信息
     * @param roleIdList
     * @return
     */
    List<Manager> getManagerListByRoleIds(List<Integer> roleIdList);

    /**
     * 包含删除的
     * @param email
     * @return
     */
    Manager getAllManagerByEmail(String email);

    /**
     * 按照Id查询用户信息
     * @param id
     * @return
     */
    Manager getManagerById(Integer id);
}
