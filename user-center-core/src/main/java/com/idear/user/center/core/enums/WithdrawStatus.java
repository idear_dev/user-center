/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.enums;

/**
 * Created By CcShan on 2017/12/24.
 * @author Administrator
 */
public enum WithdrawStatus {

    /**
     * 审核中
     */
    AUDITING(1, "审核中"),

    /**
     * 交易中
     */
    IN_TRANS(2, "交易中"),

    /**
     * 提现成功
     */
    SUCCEED(8, "成功"),

    /**
     * 提现失败
     */
    FAILED(9, "失败");

    private Integer code;

    private String desc;

    WithdrawStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
