/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.Account;

/**
 * @author ShanCc
 * @date 2018/11/5
 */
public interface AccountMapper {

    /**
     * 根据memberId获取账户信息
     * @param memberId
     * @return
     */
    Account getAccountByMemberId(Integer memberId);

    /**
     * 保存账户信息
     * @param account
     * @return
     */
    void saveAccount(Account account);

    /**
     * 更新账户信息
     * @param account
     * @return
     */
    Account updateAccount(Account account);
}
