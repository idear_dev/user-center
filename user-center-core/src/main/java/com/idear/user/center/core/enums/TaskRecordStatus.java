/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.enums;

/**
 * @author ShanCc
 * @date 2018/9/17
 */
public enum TaskRecordStatus {

    /**
     * 进行中
     */
    DOING(1, "进行中"),

    /**
     * 待审核
     */
    TO_AUDIT(2, "等待审核"),

    /**
     * 审核失败
     */
    AUDIT_FAILED(3, "审核失败"),

    /**
     * 已经取消（主动取消、被动取消）
     */
    CANCELED(4, "已经取消"),

    /**
     * 审核成功
     */
    AUDIT_SUCCEED(5, "审核成功");

    private Integer code;

    private String desc;

    TaskRecordStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
