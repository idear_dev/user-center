/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.enums;

/**
 * Created By CcShan on 2017/12/24.
 * @author Administrator
 */
public enum AccountRecordFlag {

    /**
     * 入账
     */
    IN(1,"入账"),

    /**
     * 出账
     */
    OUT(9,"出账")
    ;

    private Integer code;

    private String decs;

    AccountRecordFlag(Integer code, String desc) {
        this.code = code;
        this.decs = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDecs() {
        return decs;
    }

    public void setDecs(String decs) {
        this.decs = decs;
    }
}
