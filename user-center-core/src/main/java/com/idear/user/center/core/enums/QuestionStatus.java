/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.core.enums;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public enum QuestionStatus {

    /**
     * 可用状态
     */
    ENABLE(1, "可用"),

    /**
     * 不可使用状态
     */
    DISABLE(0, "不可用");

    private Integer code;

    private String desc;

    QuestionStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
