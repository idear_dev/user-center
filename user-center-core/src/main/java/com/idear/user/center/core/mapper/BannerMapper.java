/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.core.mapper;

import com.idear.user.center.core.po.Banner;

import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/2
 */
public interface BannerMapper {

    /**
     * 根据位置信息查询banner列表
     * @param location 位置信息
     * @return
     */
    List<Banner> getBannerListByLocation(Integer location);

    /**
     * 查询所有banner
     * @return
     */
    List<Banner> getBannerList();

    /**
     * 保存banner信息
     * @param banner
     */
    void saveBanner(Banner banner);

    /**
     * 更新Banner信息
     * @param banner
     */
    void updateBanner(Banner banner);

    /**
     * 查询banner详情
     * @param id
     * @return
     */
    Banner getBanner(Integer id);
}
