/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.convert;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.idear.common.util.DateUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.StringUtil;
import com.idear.user.center.service.dto.TaskRecordDto;
import com.idear.user.center.service.dto.TaskRecordListDto;
import com.idear.user.center.web.vo.TaskRecordListVO;

import java.util.*;

/**
 * @author ShanCc
 * @date 2018/9/19
 */
public class TaskRecordConverter {

    public static TaskRecordListVO convert2TaskRecordListVO(TaskRecordListDto taskRecordListDto) {
        TaskRecordListVO taskRecordListVO = new TaskRecordListVO();
        taskRecordListVO.setTotalNum(taskRecordListDto.getTotalNum());
        taskRecordListVO.setTotalPage(taskRecordListDto.getTotalPage());
        List<TaskRecordDto> dtoList = taskRecordListDto.getTaskRecordList();
        List<Map<String, Object>> taskRecordList = new ArrayList<>();
        for (TaskRecordDto taskRecordDto : dtoList) {
            taskRecordList.add(convert2TaskRecordMap(taskRecordDto));
        }
        taskRecordListVO.setRecordList(taskRecordList);
        return taskRecordListVO;
    }

    public static Map<String,Object> convert2TaskDetailMap(TaskRecordDto taskRecordDto){
        Map<String, Object> temp = new HashMap<>(32);
        JSONObject appInfo = JsonUtil.parseJsonObject(taskRecordDto.getAppInfo());
        JSONObject taskInfo = JsonUtil.parseJsonObject(taskRecordDto.getTaskInfo());
        JSONObject planInfo = JsonUtil.parseJsonObject(taskRecordDto.getPlanInfo());
        planInfo.put("label",planInfo.getJSONArray("label"));
        planInfo.put("tutorial",planInfo.getJSONArray("tutorial"));
        if (StringUtil.isNotEmpty(taskRecordDto.getMarketInfo())){
            JSONObject marketInfo = JsonUtil.parseJsonObject(taskRecordDto.getMarketInfo());
            temp.put("marketInfo",marketInfo);
        }
        if (StringUtil.isNotEmpty(taskRecordDto.getAuditInfo())){
            JSONArray auditInfo = JsonUtil.parseJsonArray(taskRecordDto.getAuditInfo());
            temp.put("auditInfo",auditInfo);
        }
        temp.put("recordId", taskRecordDto.getRecordId());
        Date submitTime = taskRecordDto.getSubmitTime();
        if (submitTime != null){
            temp.put("submitTime", DateUtil.getFullCNPatternNow(submitTime));
        }
        String auditRemark = taskRecordDto.getAuditRemark();
        if (StringUtil.isNotEmpty(taskRecordDto.getAuditRemark())){
            temp.put("auditRemark", auditRemark);
        }
        Date auditTime = taskRecordDto.getAuditTime();
        if (auditTime != null){
            temp.put("auditTime", DateUtil.getFullCNPatternNow(auditTime));
        }
        temp.put("appInfo",appInfo);
        temp.put("taskInfo",taskInfo);
        temp.put("planInfo",planInfo);
        temp.put("status", taskRecordDto.getStatus());
        return temp;
    }

    public static Map<String, Object> convert2TaskRecordMap(TaskRecordDto taskRecordDto) {
        Map<String, Object> temp = new HashMap<>(32);
        JSONObject appInfo = JsonUtil.parseJsonObject(taskRecordDto.getAppInfo());
        JSONObject taskInfo = JsonUtil.parseJsonObject(taskRecordDto.getTaskInfo());
        JSONObject planInfo = JsonUtil.parseJsonObject(taskRecordDto.getPlanInfo());
        temp.put("recordId", taskRecordDto.getRecordId());
        temp.put("appName", appInfo.getString("appName"));
        temp.put("appLogoUrl", appInfo.getString("appLogoUrl"));
        temp.put("taskId", taskInfo.getInteger("id"));
        temp.put("description", planInfo.getString("description"));
        temp.put("amount", taskInfo.getInteger("amount"));
        temp.put("receiveTime", DateUtil.getFullCNPatternNow(taskRecordDto.getCreateTime()));
        Date submitTime = taskRecordDto.getSubmitTime();
        if (submitTime != null){
            temp.put("submitTime", DateUtil.getFullCNPatternNow(submitTime));
        }
        String auditRemark = taskRecordDto.getAuditRemark();
        if (StringUtil.isNotEmpty(taskRecordDto.getAuditRemark())){
            temp.put("auditRemark", auditRemark);
        }
        Date auditTime = taskRecordDto.getAuditTime();
        if (auditTime != null){
            temp.put("auditTime", DateUtil.getFullCNPatternNow(auditTime));
        }
        temp.put("status", taskRecordDto.getStatus());
        return temp;
    }
}
