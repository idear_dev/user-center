package com.idear.user.center.web.vo;

/**
 * 客户端版本信息
 * @author cc
 * @date 2018/08/26
 */
public class ClientVersionVO {

    /**
     * 是否强制升级
     */
    private Boolean forceUpdate;

    /**
     * 是否提醒升级信息
     */
    private Boolean remind;

    /**
     * 升级提示信息
     */
    private String description;

    /**
     * 下载地址
     */
    private String downloadUrl;

    /**
     * 更新的版本
     */
    private Integer version;

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public Boolean getRemind() {
        return remind;
    }

    public void setRemind(Boolean remind) {
        this.remind = remind;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
