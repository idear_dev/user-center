/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.CommonUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.sms.service.facade.SmsService;
import com.idear.sms.service.param.SendSingleMessageParam;
import com.idear.user.center.service.facade.MemberService;
import com.idear.user.center.web.business.ValidCodeBusiness;
import com.idear.user.center.web.constants.CacheConstant;
import com.idear.user.center.web.enums.SmsFlag;
import com.idear.user.center.web.message.WebMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Created By CcShan on 2017/12/20.
 *
 * @author Administrator
 */
@Component
public class ValidCodeBusinessImpl implements ValidCodeBusiness {

    @Resource
    private MemberService memberService;

    @Resource
    private SmsService smsService;

    @Value("${validate.sms.code}")
    private Boolean validateSmsCode;

    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final String SMS_VALID_CODE_TEMPLATE_CODE = "0001";

    private static final Long SMS_VALID_CODE_EXPIRED_TIME = 600L;

    /**
     * 发送注册短信验证码
     *
     * @param mobileNo
     * @param flag
     * @return
     */
    @Override
    public Response sendSmsValidCode(String mobileNo, Integer flag) {
        Response<Boolean> response = memberService.checkMobileNoIsRegistry(mobileNo);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        Boolean isRegistry = response.getRespBody();
        if (SmsFlag.REGISTRY.getCode().equals(flag) && isRegistry){
            throw new BusinessException(WebMessage.MOBILE_NO_IS_REGISTRY);
        }

        if (SmsFlag.CHANGE_PWD.getCode().equals(flag) && !isRegistry){
            throw new BusinessException(WebMessage.MOBILE_NO_IS_NOT_REGISTRY);
        }
        String value = redisTemplate.opsForValue().get(CacheConstant.USER_SMS_VALID_CODE + mobileNo);
        if (!StringUtil.isEmpty(value)) {
            throw new BusinessException(WebMessage.SMS_VALID_CODE_HAS_SEND);
        }
        String validCode = CommonUtil.getRandomNum(6);
        SendSingleMessageParam sendSingleMessageParam = new SendSingleMessageParam();
        sendSingleMessageParam.setMobileNo(mobileNo);
        sendSingleMessageParam.setTemplateCode(SMS_VALID_CODE_TEMPLATE_CODE);
        sendSingleMessageParam.setParams(new String[]{validCode});
        Response sendResponse = smsService.sendSingleMessage(sendSingleMessageParam);
        if (sendResponse == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        if (!sendResponse.success()) {
            throw new BusinessException(sendResponse.getRespCode(), sendResponse.getRespMsg());
        } else {
            redisTemplate.opsForValue().set(CacheConstant.USER_SMS_VALID_CODE + mobileNo, validCode, SMS_VALID_CODE_EXPIRED_TIME, TimeUnit.SECONDS);
        }
        return Response.SUCCESS;
    }

    /**
     * 验证验证码
     *
     * @param ticket
     * @param validCode
     */
    @Override
    public void validateCaptcha(String ticket, String validCode) {
        String value = redisTemplate.opsForValue().get(CacheConstant.USER_VALID_CODE_TICKET_INDEX + ticket);
        if (StringUtil.isEmpty(value)) {
            throw new BusinessException(WebMessage.VALID_CODE_EXPIRED);
        } else {
            redisTemplate.delete(CacheConstant.USER_VALID_CODE_TICKET_INDEX + ticket);
            if (!validCode.equalsIgnoreCase(value)) {
                throw new BusinessException(WebMessage.VALID_CODE_ERROR);
            }
        }
    }

    /**
     * 验证短信验证码
     *
     * @param mobileNo
     * @param validCode
     */
    @Override
    public void validateSmsValidCode(String mobileNo, String validCode) {
        if (validateSmsCode) {
            String value = redisTemplate.opsForValue().get(CacheConstant.USER_SMS_VALID_CODE + mobileNo);
            if (StringUtil.isEmpty(value)) {
                throw new BusinessException(WebMessage.SMS_VALID_CODE_HAS_EXPIRED);
            } else {
                redisTemplate.delete(CacheConstant.USER_SMS_VALID_CODE + mobileNo);
                if (!value.equals(validCode)) {
                    throw new BusinessException(WebMessage.VALID_CODE_ERROR);
                }
            }
        }
    }
}
