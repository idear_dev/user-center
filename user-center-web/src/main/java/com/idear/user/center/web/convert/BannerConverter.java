/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.convert;

import com.idear.user.center.service.dto.BannerDto;
import com.idear.user.center.web.vo.BannerVO;

/**
 * @author CcShan
 * @date 2018/9/2
 */
public class BannerConverter {

    public static BannerVO convert2BannerVO(BannerDto bannerDto){
        BannerVO bannerVO = new BannerVO();
        bannerVO.setImageUrl(bannerDto.getImageUrl());
        bannerVO.setLinkUrl(bannerDto.getLinkUrl());
        bannerVO.setSort(bannerDto.getSort());
        bannerVO.setTitle(bannerDto.getTitle());
        return bannerVO;
    }
}
