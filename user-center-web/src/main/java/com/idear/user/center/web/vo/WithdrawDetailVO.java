/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.vo;

import com.idear.common.util.JsonUtil;

/**
 * Created By CcShan on 2018/1/3.
 * @author Administrator
 */
public class WithdrawDetailVO {

    @Override
    public String toString(){
        return JsonUtil.toJsonString(this);
    }
}
