/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.business.WithdrawBusiness;
import com.idear.user.center.web.form.ApplyWithdrawForm;
import com.idear.user.center.web.form.WithdrawDetailForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 提现相关接口
 * Created By CcShan on 2018/1/3.
 *
 * @author Administrator
 */
@Controller
@RequestMapping("withdraw")
public class WithdrawController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private WithdrawBusiness withdrawBusiness;

    @Autowired
    private MemberBusiness memberBusiness;

    /**
     * 获取提现明细
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "get", method = RequestMethod.POST)
    @ResponseBody
    public String getWithdrawDetail(@Valid @RequestBody final WithdrawDetailForm withdrawDetailForm, final HttpServletRequest request, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = request.getHeader("token");
                return withdrawBusiness.getDetail(token, withdrawDetailForm.getSerialNo());
            }
        }, bindingResult);
    }

    /**
     * 微信账号绑定状态
     *
     * @return
     */
    @RequestMapping(value = "wechat/bind/status", method = RequestMethod.POST)
    @ResponseBody
    public String wechatBindStatus() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return withdrawBusiness.wechatBindStatus(memberId);
            }
        });
    }

    /**
     * 提现申请
     *
     * @return
     */
    @RequestMapping(value = "apply", method = RequestMethod.POST)
    @ResponseBody
    public String applyWithdraw(@Valid @RequestBody final ApplyWithdrawForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                Integer amount = form.getAmount();
                return withdrawBusiness.apply(memberId, amount);
            }
        }, bindingResult);
    }
}
