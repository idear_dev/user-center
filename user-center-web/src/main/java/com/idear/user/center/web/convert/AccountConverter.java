/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.convert;

import com.idear.common.constants.MapConstant;
import com.idear.common.util.JsonUtil;
import com.idear.user.center.service.dto.AccountInfoDto;
import com.idear.user.center.service.dto.AccountRecordDto;
import com.idear.user.center.service.dto.AccountRecordListDto;
import com.idear.user.center.web.vo.AccountInfoVO;
import com.idear.user.center.web.vo.IncomeListVO;
import com.idear.user.center.web.vo.WithdrawListVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public class AccountConverter {

    /**
     * VO转换
     * @param accountInfoDto
     * @return
     */
    public static AccountInfoVO convert2AccountInfoVO(AccountInfoDto accountInfoDto) {
        AccountInfoVO accountInfoVO = new AccountInfoVO();
        accountInfoVO.setCanWithdrawAmount(accountInfoDto.getCanWithdrawAmount());
        accountInfoVO.setHasWithdrawAmount(accountInfoDto.getHasWithdrawAmount());
        accountInfoVO.setTotalIncome(accountInfoDto.getTotalIncome());
        accountInfoVO.setTodayIncome(accountInfoDto.getTodayIncome());
        accountInfoVO.setWithdrawingAmount(accountInfoDto.getWithdrawingAmount());
        accountInfoVO.setImportingAmount(accountInfoDto.getImportingAmount());
        return accountInfoVO;
    }

    /**
     * VO转换
     * @param incomeListDto
     * @return
     */
    public static IncomeListVO convert2IncomeListVO(AccountRecordListDto incomeListDto) {
        IncomeListVO incomeListVO = new IncomeListVO();
        incomeListVO.setTotalNum(incomeListDto.getTotalNum());
        incomeListVO.setTotalPage(incomeListDto.getTotalPage());
        if (incomeListDto.getAccountRecordDtoList() != null){
            List<AccountRecordDto> recordDtoList = incomeListDto.getAccountRecordDtoList();
            List<Map<String,Object>> incomeList = new ArrayList<>();
            for (AccountRecordDto accountRecordDto:recordDtoList){
                Map<String,Object> map = new HashMap<>(MapConstant.INITIALCAPACITY_16);
                map.put("amount",accountRecordDto.getAmount());
                map.put("id",accountRecordDto.getId());
                map.put("serialNo",accountRecordDto.getSerialNo());
                map.put("recordTime",accountRecordDto.getRecordTime());
                map.put("updateTime",accountRecordDto.getUpdateTime());
                map.put("status",accountRecordDto.getStatus());
                map.put("remark", accountRecordDto.getRemark());
                incomeList.add(map);
            }
            incomeListVO.setIncomeList(incomeList);
        }
        return incomeListVO;
    }

    /**
     * VO转换
     * @param incomeListDto
     * @return
     */
    public static WithdrawListVO convert2WithdrawListVO(AccountRecordListDto incomeListDto) {
        WithdrawListVO withdrawListVO = new WithdrawListVO();
        withdrawListVO.setTotalNum(incomeListDto.getTotalNum());
        withdrawListVO.setTotalPage(incomeListDto.getTotalPage());
        if (incomeListDto.getAccountRecordDtoList() != null){
            List<AccountRecordDto> recordDtoList = incomeListDto.getAccountRecordDtoList();
            List<Map<String,Object>> withdrawList = new ArrayList<>();
            for (AccountRecordDto accountRecordDto:recordDtoList){
                Map<String,Object> map = new HashMap<>(MapConstant.INITIALCAPACITY_16);
                map.put("amount",accountRecordDto.getAmount());
                map.put("id",accountRecordDto.getId());
                map.put("serialNo",accountRecordDto.getSerialNo());
                map.put("recordTime",accountRecordDto.getRecordTime());
                map.put("updateTime",accountRecordDto.getUpdateTime());
                map.put("status",accountRecordDto.getStatus());
                withdrawList.add(map);
            }
            withdrawListVO.setWithdrawList(withdrawList);
        }
        return withdrawListVO;
    }
}
