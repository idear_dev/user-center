/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.web.business.LoginBusiness;
import com.idear.user.center.web.business.ValidCodeBusiness;
import com.idear.user.center.web.form.PasswordLoginForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
@Controller
@RequestMapping(value = "login")
public class LoginController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private LoginBusiness loginBusiness;

    /**
     * 使用手机号密码登录
     * @param loginForm
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "password",method = RequestMethod.POST)
    @ResponseBody
    public String loginByPassword(@Valid @RequestBody final PasswordLoginForm loginForm, BindingResult bindingResult){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return loginBusiness.login(loginForm);
            }
        },bindingResult);
    }
}
