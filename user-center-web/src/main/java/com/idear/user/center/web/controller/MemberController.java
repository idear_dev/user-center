/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.business.ValidCodeBusiness;
import com.idear.user.center.web.form.RegistryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
@Controller
@RequestMapping("member")
public class MemberController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private MemberBusiness memberBusiness;

    @Autowired
    private ValidCodeBusiness validCodeBusiness;

    /**
     * 根据Token获取用户基本信息
     * @return
     */
    @RequestMapping(value = "get",method = RequestMethod.POST)
    @ResponseBody
    public String login(final HttpServletRequest request){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return memberBusiness.getMemberByToken(request.getHeader("token"));
            }
        });
    }

    /**
     * 手机号注册
     * @return
     */
    @RequestMapping(value = "registry",method = RequestMethod.POST)
    @ResponseBody
    public String registry(@RequestBody @Valid final RegistryForm registryForm , BindingResult bindingResult){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                validCodeBusiness.validateSmsValidCode(registryForm.getMobileNo(),registryForm.getValidCode());
                return memberBusiness.registry(registryForm.getMobileNo(),registryForm.getPassword());
            }
        },bindingResult);
    }

    /**
     * 退出登录
     * @return
     */
    @RequestMapping(value = "logout",method = RequestMethod.POST)
    @ResponseBody
    public String logout(){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                return memberBusiness.logout(token);
            }
        });
    }
}
