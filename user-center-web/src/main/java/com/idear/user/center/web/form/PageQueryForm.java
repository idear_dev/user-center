/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.form;


/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public class PageQueryForm {

    private static Integer DEFAULT_MAX_PAGE_SIZE = 30;

    /**
     * 页数
     */
    private Integer pageNo = 1;

    /**
     * 查询数
     */
    private Integer pageSize = 10;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        //禁止大批量分页查询
        if (pageSize > DEFAULT_MAX_PAGE_SIZE) {
            return DEFAULT_MAX_PAGE_SIZE;
        }
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
