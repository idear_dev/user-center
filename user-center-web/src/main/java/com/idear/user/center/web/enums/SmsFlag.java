/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.enums;

/**
 * @author ShanCc
 * @date 2018/12/18
 */
public enum SmsFlag {

    /**
     * 注册
     */
    REGISTRY(1, "注册"),

    /**
     * 修改登录密码
     */
    CHANGE_PWD(0, "修改登录密码");

    private Integer code;

    private String desc;

    SmsFlag(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
