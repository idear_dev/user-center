/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.web.form.ListRecordForm;
import com.idear.user.center.web.vo.TaskRecordListVO;

import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/17
 */
public interface TaskRecordBusiness {

    /**
     * 取消任务
     *
     * @param memberId
     * @param recordId
     * @return
     */
    Response cancelTask(Integer memberId, Integer recordId);

    /**
     * 提交审核
     *
     * @param memberId
     * @param recordId
     * @param auditInfo
     * @return
     */
    Response submitAudit(Integer memberId, Integer recordId, List<String> auditInfo);

    /**
     * 查询用户任务记录
     *
     * @param memberId
     * @param listRecordForm
     * @return
     */
    Response<TaskRecordListVO> getTaskRecordList(Integer memberId, ListRecordForm listRecordForm);

    /**
     * 根据会员编号查询用户当前成就
     *
     * @param memberId
     * @return
     */
    Response getAchievement(Integer memberId);

    /**
     * 获取任务记录详情
     * @param memberId
     * @param recordId
     * @return
     */
    Response getTaskRecordDetail(Integer memberId, Integer recordId);
}
