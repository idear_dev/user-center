/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.user.center.core.enums.ClientChannel;
import com.idear.user.center.service.dto.BannerDto;
import com.idear.user.center.service.dto.ClientVersionDto;
import com.idear.user.center.service.facade.ConfigService;
import com.idear.user.center.web.business.ConfigBusiness;
import com.idear.user.center.web.message.WebMessage;
import com.idear.user.center.web.vo.ClientVersionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author CcShan
 * @date 2018/8/27
 */
@Component
public class ConfigBusinessImpl implements ConfigBusiness {

    @Autowired
    private ConfigService configService;

    /**
     * 根据渠道信息和版本信息获取当前客户端版本信息
     *
     * @param channel 渠道
     * @param version 版本
     * @return
     */
    @Override
    public Response<ClientVersionVO> getClientVersionInfo(String channel, Integer version) {
        ClientChannel clientChannel = ClientChannel.getChannel(channel);
        if (ClientChannel.UNKNOWN.getValue().equals(clientChannel.getValue())) {
            throw new BusinessException(WebMessage.CHANNEL_NOT_EXIST);
        }
        //查询version之后更新的版本信息
        Response<List<ClientVersionDto>> response = configService.listNewClientVersion(clientChannel.getValue(), version);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            List<ClientVersionDto> clientVersionDtoList = response.getRespBody();
            //有版本信息
            if (clientVersionDtoList != null && !clientVersionDtoList.isEmpty()){
                ClientVersionVO vo = getClientVersion(clientVersionDtoList,version);
                return new Response(vo);
            }else {
                //没有该版本信息
                throw new BusinessException(WebMessage.VERSION_NOT_EXIT);
            }
        }
    }

    /**
     * 当前版本之前
     * @param clientVersionDtoList
     * @param currentVersion
     * @return
     */
    private ClientVersionVO getClientVersion(List<ClientVersionDto> clientVersionDtoList,Integer currentVersion){
        ClientVersionVO clientVersionVO = new ClientVersionVO();
        //对版本信息进行升序排序
        Collections.sort(clientVersionDtoList);
        //获取最新对版本信息
        ClientVersionDto newVersion = clientVersionDtoList.get(clientVersionDtoList.size()-1);
        clientVersionVO.setDescription(newVersion.getDescription());
        clientVersionVO.setDownloadUrl(newVersion.getDownloadUrl());
        clientVersionVO.setVersion(newVersion.getVersion());
        //设置默认不强制升级
        clientVersionVO.setForceUpdate(false);
        if (clientVersionDtoList.size() == 1){
            //当前版本是最新版本，不提示升级，不强制升级
            clientVersionVO.setRemind(false);
        }else {
            //有最新版本提示升级
            clientVersionVO.setRemind(true);
            Iterator<ClientVersionDto> iterator = clientVersionDtoList.iterator();
            while (iterator.hasNext()){
                ClientVersionDto clientVersionDto = iterator.next();
                //剔除当前版本
                if (!clientVersionDto.getVersion().equals(currentVersion)){
                    if (clientVersionDto.getForceUpdate()){
                        clientVersionVO.setForceUpdate(true);
                        break;
                    }
                }
            }
        }
        return clientVersionVO;
    }
}
