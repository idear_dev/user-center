/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.constants;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
public class CacheConstant {

    public static final String USER_VALID_CODE_TICKET_INDEX = "USER:VALID_CODE_TICKET:";

    public static final String USER_SMS_VALID_CODE = "USER:SMS_VALID_CODE:";
}
