/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.FileUtil;
import com.idear.framework.response.Response;
import com.idear.user.center.service.facade.FileService;
import com.idear.user.center.web.business.FileBusiness;
import com.idear.user.center.web.message.WebMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * @author CcShan
 * @date 2018/9/18
 */
@Component
public class FileBusinessImpl implements FileBusiness {

    @Autowired
    private FileService fileService;

    /**
     * 上传图片信息
     *
     * @param memberId
     * @param multipartFile
     * @return
     */
    @Override
    public Response uploadImage(Integer memberId, MultipartFile multipartFile) {
        try {
            InputStream inputStream = multipartFile.getInputStream();
            String originalFilename = multipartFile.getOriginalFilename();
            String fileType = FileUtil.getFileType(originalFilename);
            String path = memberId+"/"+ UUID.randomUUID().toString()+"."+fileType;
            if (!FileUtil.isCorrectImageType(fileType)){
                throw new BusinessException(WebMessage.UPLOAD_FILE_NOT_SUPPORT);
            }
            Response response = fileService.uploadFile(inputStream,path);
            if (response == null) {
                throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
            } else if (!response.success()) {
                throw new BusinessException(response.getRespCode(), response.getRespMsg());
            } else {
                Map<String,Object> resultMap = new HashMap<>(2);
                resultMap.put("visitUrl",response.getRespBody());
                return new Response(resultMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(WebMessage.UPLOAD_READ_FILE_FAILED);
        }
    }


}
