/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.vo;

import com.idear.common.util.JsonUtil;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public class AccountInfoVO {

    /**
     * 总收入
     */
    private Integer totalIncome;

    /**
     * 今日收入
     */
    private Integer todayIncome;

    /**
     * 可提现金额
     */
    private Integer canWithdrawAmount;

    /**
     * 已成功提现金额
     */
    private Integer hasWithdrawAmount;

    /**
     * 提现审核中的金额
     */
    private Integer withdrawingAmount;

    /**
     * 入账中的总金额
     */
    private Integer importingAmount;

    public Integer getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Integer totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Integer getTodayIncome() {
        return todayIncome;
    }

    public void setTodayIncome(Integer todayIncome) {
        this.todayIncome = todayIncome;
    }

    public Integer getCanWithdrawAmount() {
        return canWithdrawAmount;
    }

    public void setCanWithdrawAmount(Integer canWithdrawAmount) {
        this.canWithdrawAmount = canWithdrawAmount;
    }

    public Integer getHasWithdrawAmount() {
        return hasWithdrawAmount;
    }

    public void setHasWithdrawAmount(Integer hasWithdrawAmount) {
        this.hasWithdrawAmount = hasWithdrawAmount;
    }

    public Integer getWithdrawingAmount() {
        return withdrawingAmount;
    }

    public void setWithdrawingAmount(Integer withdrawingAmount) {
        this.withdrawingAmount = withdrawingAmount;
    }

    public Integer getImportingAmount() {
        return importingAmount;
    }

    public void setImportingAmount(Integer importingAmount) {
        this.importingAmount = importingAmount;
    }

    @Override
    public String toString(){
        return JsonUtil.toJsonString(this);
    }
}
