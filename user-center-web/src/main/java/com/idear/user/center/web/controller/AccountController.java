/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.web.business.AccountBusiness;
import com.idear.user.center.web.form.PageQueryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 账户相关接口
 * Created By CcShan on 2017/12/23.
 * @author Administrator
 */
@Controller
@RequestMapping(value = "account")
public class AccountController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private AccountBusiness accountBusiness;

    /**
     * 获取账户信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "get", method = RequestMethod.POST)
    @ResponseBody
    public String getAccountInfo(final HttpServletRequest request) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return accountBusiness.getAccountInfo(request.getHeader("token"));
            }
        });
    }

    /**
     * 收入列表
     * @param pageQueryForm
     * @param request
     * @return
     */
    @RequestMapping(value = "income/list", method = RequestMethod.POST)
    @ResponseBody
    public String getIncomeList(@Valid @RequestBody final PageQueryForm pageQueryForm, final HttpServletRequest request) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = request.getHeader("token");
                return accountBusiness.getIncomeList(token,pageQueryForm.getPageNo(),pageQueryForm.getPageSize());
            }
        });
    }

    /**
     * 已经提现的列表，包含提现成功和失败
     * @param pageQueryForm
     * @param request
     * @return
     */
    @RequestMapping(value = "has/withdraw/list", method = RequestMethod.POST)
    @ResponseBody
    public String getHasWithdrawList(@Valid @RequestBody final PageQueryForm pageQueryForm, final HttpServletRequest request) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = request.getHeader("token");
                return accountBusiness.getHasWithdrawList(token,pageQueryForm.getPageNo(),pageQueryForm.getPageSize());
            }
        });
    }

    /**
     * 提现审核中的列表
     * @param pageQueryForm
     * @param request
     * @return
     */
    @RequestMapping(value = "withdrawing/list", method = RequestMethod.POST)
    @ResponseBody
    public String getWithdrawingList(@Valid @RequestBody final PageQueryForm pageQueryForm, final HttpServletRequest request) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = request.getHeader("token");
                return accountBusiness.getWithdrawingList(token,pageQueryForm.getPageNo(),pageQueryForm.getPageSize());
            }
        });
    }
}
