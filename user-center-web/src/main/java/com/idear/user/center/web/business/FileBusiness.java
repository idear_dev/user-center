/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author CcShan
 * @date 2018/9/18
 */
public interface FileBusiness {

    /**
     * 上传图片信息
     * @param memberId
     * @param multipartFile
     * @return
     */
    Response uploadImage(Integer memberId,MultipartFile multipartFile);
}
