/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.inteceptor;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.user.center.service.facade.MemberService;
import com.idear.user.center.web.message.WebMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证Token有效性拦截器
 * @author CcShan
 * @date 2018/1/29
 */
@Component("validateTokenInterceptor")
public class ValidateTokenInterceptor implements HandlerInterceptor{

    @Autowired
    private MemberService memberService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        String token = httpServletRequest.getHeader("token");
        if (StringUtil.isNotEmpty(token)){
            Response<Boolean> response = memberService.checkToken(token);
            if (response == null || !response.getRespBody()){
                throw new BusinessException(WebMessage.TOKEN_IS_ILLEGAL);
            }
        }else {
            throw new BusinessException(WebMessage.TOKEN_IS_EMPTY);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
