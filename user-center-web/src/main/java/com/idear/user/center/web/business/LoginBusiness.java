/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.web.form.PasswordLoginForm;

/**
 *
 * @author CcShan
 * @date 2017/12/20
 */
public interface LoginBusiness {

    /**
     * 用户登录
     * @param loginForm
     * @return
     */
    Response login(PasswordLoginForm loginForm);
}
