/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.AccountInfoDto;
import com.idear.user.center.service.dto.AccountRecordListDto;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.service.facade.AccountService;
import com.idear.user.center.web.business.AccountBusiness;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.convert.AccountConverter;
import com.idear.user.center.web.vo.AccountInfoVO;
import com.idear.user.center.web.vo.IncomeListVO;
import com.idear.user.center.web.vo.WithdrawListVO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author CcShan
 * @date 2018/1/2
 */
@Component
public class AccountBusinessImpl implements AccountBusiness {

    @Resource
    private MemberBusiness memberBusiness;

    @Resource
    private AccountService accountService;

    /**
     * 获取账户信息
     *
     * @param token
     * @return
     */
    @Override
    public Response<AccountInfoVO> getAccountInfo(String token) {
        MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
        Integer memberId = memberInfoDto.getId();
        Response<AccountInfoDto> response = accountService.getAccountInfo(memberId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            return new Response(AccountConverter.convert2AccountInfoVO(response.getRespBody()));
        }

    }

    /**
     * 获取收入明细
     *
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<IncomeListVO> getIncomeList(String token, Integer pageNo, Integer pageSize) {
        MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
        Integer memberId = memberInfoDto.getId();
        Response<AccountRecordListDto> incomeListDtoResponse = accountService.getIncomeList(memberId, pageNo, pageSize);
        if (incomeListDtoResponse == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!incomeListDtoResponse.success()) {
            throw new BusinessException(incomeListDtoResponse.getRespCode(), incomeListDtoResponse.getRespMsg());
        } else {
            return new Response(AccountConverter.convert2IncomeListVO(incomeListDtoResponse.getRespBody()));
        }
    }

    /**
     * 获取已经已经成功提现列表
     *
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<WithdrawListVO> getHasWithdrawList(String token, Integer pageNo, Integer pageSize) {
        MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
        Integer memberId = memberInfoDto.getId();
        Response<AccountRecordListDto> hasWithdrawList = accountService.getHasWithdrawList(memberId, pageNo, pageSize);
        return handleWithdrawList(hasWithdrawList);
    }

    /**
     * 获取已经已经成功提现列表
     *
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<WithdrawListVO> getWithdrawingList(String token, Integer pageNo, Integer pageSize) {
        MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
        Integer memberId = memberInfoDto.getId();
        Response<AccountRecordListDto> withdrawingList = accountService.getWithdrawingList(memberId, pageNo, pageSize);
        return handleWithdrawList(withdrawingList);
    }

    /**
     * 处理提现列表类
     *
     * @param withdrawingList
     * @return
     */
    private Response<WithdrawListVO> handleWithdrawList(Response<AccountRecordListDto> withdrawingList) {
        if (withdrawingList == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!withdrawingList.success()) {
            throw new BusinessException(withdrawingList.getRespCode(), withdrawingList.getRespMsg());
        } else {
            return new Response(AccountConverter.convert2WithdrawListVO(withdrawingList.getRespBody()));
        }
    }
}
