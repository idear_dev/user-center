/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.MemberInfoDto;

/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
public interface MemberBusiness {

    /**
     * 获取用户基本信息
     * @param token
     * @return
     */
    Response getMemberByToken(String token);

    /**
     * 根据Token获取memberInfo
     * @param token
     * @return
     */
    MemberInfoDto getMemberInfoByToken(String token);
    /**
     * 手机号注册
     * @param mobileNo
     * @param password
     * @return
     */
    Response registry(String mobileNo,String password);

    /**
     * 重置登录密码
     * @param mobileNo
     * @param password
     * @return
     */
    Response resetLoginPassword(String mobileNo,String password);

    /**
     * 登出
     * @param token
     * @return
     */
    Response logout(String token);
}
