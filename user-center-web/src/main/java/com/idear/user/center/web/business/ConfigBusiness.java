/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.BannerDto;
import com.idear.user.center.web.vo.ClientVersionVO;

import java.util.List;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public interface ConfigBusiness {

    /**
     * 根据渠道信息和版本信息获取当前客户端版本信息
     * @param channel 渠道
     * @param version 版本
     * @return
     */
    Response<ClientVersionVO> getClientVersionInfo(String channel, Integer version);
}
