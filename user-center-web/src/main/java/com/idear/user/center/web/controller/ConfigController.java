/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.controller;

import com.idear.common.exception.impl.ValidationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.web.business.ConfigBusiness;
import com.idear.user.center.web.message.WebMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 配置信息
 * @author ShanCc
 * @date 2018/8/5
 */

@Controller
@RequestMapping(value = "config")
public class ConfigController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private ConfigBusiness configBusiness;

    /**
     * 获取账户信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/client/version", method = RequestMethod.POST)
    @ResponseBody
    public String getClientVersionInfo(final HttpServletRequest request){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String channel = request.getHeader(SystemHeader.CHANNEL);
                String clientVersion = request.getHeader(SystemHeader.APP_VERSION);
                if (StringUtil.isAnyEmpty(channel,clientVersion)){
                    throw new ValidationException(BaseMessage.ERROR_PARAM);
                }
                return configBusiness.getClientVersionInfo(channel,Integer.parseInt(clientVersion));
            }
        });
    }
}
