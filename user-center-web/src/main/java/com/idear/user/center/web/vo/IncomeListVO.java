/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.vo;

import com.idear.common.util.JsonUtil;

import java.util.List;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public class IncomeListVO extends PageQueryVO{

    private List<Map<String,Object>> incomeList;

    public List<Map<String, Object>> getIncomeList() {
        return incomeList;
    }

    public void setIncomeList(List<Map<String, Object>> incomeList) {
        this.incomeList = incomeList;
    }

    @Override
    public String toString(){
        return JsonUtil.toJsonString(this);
    }
}
