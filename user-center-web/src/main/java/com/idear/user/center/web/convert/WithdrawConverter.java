/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.convert;

import com.idear.user.center.service.dto.WithdrawDetailDto;
import com.idear.user.center.web.vo.WithdrawDetailVO;

/**
 * Created By CcShan on 2018/1/3.
 * @author Administrator
 */
public class WithdrawConverter {

    /**
     * VO转换
     * @param withdrawDetailDto
     * @return
     */
    public static WithdrawDetailVO convert2WithdrawDetailVO(WithdrawDetailDto withdrawDetailDto) {
        WithdrawDetailVO withdrawDetailVO = new WithdrawDetailVO();
        return withdrawDetailVO;
    }
}
