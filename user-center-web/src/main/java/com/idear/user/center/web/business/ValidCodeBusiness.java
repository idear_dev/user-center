/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;


import com.idear.framework.response.Response;

/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
public interface ValidCodeBusiness {

    /**
     * 发送短信验证码
     * @param mobileNo
     * @param flag
     * @return
     */
    Response sendSmsValidCode(String mobileNo,Integer flag);

    /**
     * 验证图片验证码
     * @param ticket
     * @param validCode
     */
    void validateCaptcha(String ticket,String validCode);

    /**
     * 验证注册短信验证码
     * @param mobileNo
     * @param validCode
     */
    void validateSmsValidCode(String mobileNo,String validCode);
}
