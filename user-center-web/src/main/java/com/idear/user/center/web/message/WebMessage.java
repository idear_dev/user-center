/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.message;

import com.idear.common.message.Message;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
public enum  WebMessage implements Message {

    /**
     * web错误码区间(7000~7200]
     */
    GENERATE_VALID_CODE_FAILED("7001","获取验证码图片失败"),
    VALID_CODE_EXPIRED("7002","验证码已过期"),
    TOKEN_IS_EMPTY("7003","token信息为空"),
    MOBILE_NO_IS_REGISTRY("7004","手机号已注册"),
    SMS_VALID_CODE_HAS_SEND("7005","短信已发送，请稍后重试"),
    SMS_VALID_CODE_HAS_EXPIRED("7006","短信验证码已过期，请重新获取"),
    VALID_CODE_ERROR("7007","验证码错误"),
    TOKEN_IS_ILLEGAL("7008","请重新登录"),
    MOBILE_NO_IS_NOT_REGISTRY("7009","手机号未注册"),

    CHANNEL_NOT_EXIST("7201","渠道信息不存在"),
    VERSION_NOT_EXIT("7202","版本信息不存在"),
    UPLOAD_READ_FILE_FAILED("7203","上传文件失败"),
    UPLOAD_FILE_NOT_SUPPORT("7204","上传文件不支持");

    private String code;

    private String msg;

    WebMessage(String code,String msg){
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getRespCode() {
        return this.code;
    }

    @Override
    public String getRespMsg() {
        return this.msg;
    }
}
