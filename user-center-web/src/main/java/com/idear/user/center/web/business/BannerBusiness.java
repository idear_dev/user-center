/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.web.vo.BannerVO;

import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/2
 */
public interface BannerBusiness {

    /**
     * 根据位置信息查询banner列表
     * @param location
     * @return
     */
    Response<List<BannerVO>> getBannerInfo(Integer location);
}
