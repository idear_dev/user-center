/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.service.facade.MemberService;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.convert.MemberConverter;
import com.idear.user.center.web.message.WebMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
@Component
public class MemberBusinessImpl implements MemberBusiness {

    @Resource
    private MemberService memberService;

    /**
     * 获取用户基本信息
     *
     * @param token
     * @return
     */
    @Override
    public Response getMemberByToken(String token) {
        MemberInfoDto memberInfoDto = getMemberInfoByToken(token);
        return new Response(MemberConverter.convert2MemberBaseInfoVO(memberInfoDto));
    }

    /**
     * 获取会员信息
     * @param token
     * @return
     */
    @Override
    public MemberInfoDto getMemberInfoByToken(String token){
        if (StringUtil.isEmpty(token)) {
            throw new BusinessException(WebMessage.TOKEN_IS_EMPTY);
        }else {
            Response<MemberInfoDto> response = memberService.getMemberByToken(token);
            if (response == null) {
                throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
            } else if (!response.success()) {
                throw new BusinessException(response.getRespCode(), response.getRespMsg());
            } else {
                return response.getRespBody();
            }
        }
    }

    /**
     * 手机号注册
     *
     * @param mobileNo
     * @param password
     * @return
     */
    @Override
    public Response registry(String mobileNo, String password) {
        Response<Boolean> response = memberService.checkMobileNoIsRegistry(mobileNo);
        if (response == null){
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        if (response.getRespBody()){
            throw new BusinessException(WebMessage.MOBILE_NO_IS_REGISTRY);
        }else {
            return memberService.registry(mobileNo,password);
        }
    }

    /**
     * 重置登录密码
     *
     * @param mobileNo
     * @param password
     * @return
     */
    @Override
    public Response resetLoginPassword(String mobileNo, String password) {
        Response<Boolean> response = memberService.checkMobileNoIsRegistry(mobileNo);
        if (response == null){
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        //手机号未注册
        if (!response.getRespBody()){
            throw new BusinessException(WebMessage.MOBILE_NO_IS_NOT_REGISTRY);
        }else {
            return memberService.resetLoginPassword(mobileNo,password);
        }
    }

    /**
     * 登出
     *
     * @param token
     * @return
     */
    @Override
    public Response logout(String token) {
        Response response = memberService.logout(token);
        if (response == null){
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        return response;
    }

}
