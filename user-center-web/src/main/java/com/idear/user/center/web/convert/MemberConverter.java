/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.convert;

import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.web.vo.MemberBaseInfoVO;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public class MemberConverter {

    /**
     * VO转换
     * @param memberInfoDto
     * @return
     */
    public static MemberBaseInfoVO convert2MemberBaseInfoVO(MemberInfoDto memberInfoDto) {
        MemberBaseInfoVO memberBaseInfoVO = new MemberBaseInfoVO();
        memberBaseInfoVO.setHeadImgUrl(memberInfoDto.getHeadImgUrl());
        memberBaseInfoVO.setMemberNo(memberInfoDto.getMemberNo());
        memberBaseInfoVO.setNickname(memberInfoDto.getNickname());
        return memberBaseInfoVO;
    }
}
