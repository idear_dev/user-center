/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.web.vo.AccountInfoVO;
import com.idear.user.center.web.vo.IncomeListVO;
import com.idear.user.center.web.vo.WithdrawListVO;


/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public interface AccountBusiness {

    /**
     * 获取账户信息
     * @param token
     * @return
     */
    Response<AccountInfoVO> getAccountInfo(String token);

    /**
     * 获取收入明细列表
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<IncomeListVO> getIncomeList(String token, Integer pageNo, Integer pageSize);


    /**
     * 获取已经已经成功提现列表
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<WithdrawListVO> getHasWithdrawList(String token,Integer pageNo, Integer pageSize);

    /**
     * 获取已经已经成功提现列表
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<WithdrawListVO> getWithdrawingList(String token,Integer pageNo, Integer pageSize);

}
