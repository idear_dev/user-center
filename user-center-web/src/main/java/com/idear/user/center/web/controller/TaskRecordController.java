/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.business.TaskRecordBusiness;
import com.idear.user.center.web.form.ListRecordForm;
import com.idear.user.center.web.form.TaskRecordDetailForm;
import com.idear.user.center.web.form.TaskRecordForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/17
 */
@Controller
@RequestMapping(value = "record")
public class TaskRecordController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private MemberBusiness memberBusiness;

    @Autowired
    private TaskRecordBusiness taskRecordBusiness;

    /**
     * 领取任务
     *
     * @return
     */
    @RequestMapping(value = "cancel", method = RequestMethod.POST)
    @ResponseBody
    public String cancelTask(@Valid @RequestBody final TaskRecordForm taskRecordForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer recordId = taskRecordForm.getRecordId();
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskRecordBusiness.cancelTask(memberId,recordId);
            }
        }, bindingResult);
    }

    /**
     * 领取任务
     *
     * @return
     */
    @RequestMapping(value = "audit/submit", method = RequestMethod.POST)
    @ResponseBody
    public String submitAudit(@Valid @RequestBody final TaskRecordForm taskRecordForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer recordId = taskRecordForm.getRecordId();
                List<String> auditInfo = taskRecordForm.getAuditInfo();
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskRecordBusiness.submitAudit(memberId,recordId,auditInfo);
            }
        }, bindingResult);
    }

    /**
     * 查询用户领取任务记录
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ResponseBody
    public String listRecord(@Valid @RequestBody final ListRecordForm listRecordForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskRecordBusiness.getTaskRecordList(memberId,listRecordForm);
            }
        }, bindingResult);
    }

    /**
     * 查询用户领取任务记录
     *
     * @return
     */
    @RequestMapping(value = "detail", method = RequestMethod.POST)
    @ResponseBody
    public String getRecordDetail(@Valid @RequestBody final TaskRecordDetailForm taskRecordDetailForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskRecordBusiness.getTaskRecordDetail(memberId,taskRecordDetailForm.getRecordId());
            }
        }, bindingResult);
    }


    /**
     * 成就
     *
     * @return
     */
    @RequestMapping(value = "achievement", method = RequestMethod.POST)
    @ResponseBody
    public String getAchievement() {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskRecordBusiness.getAchievement(memberId);
            }
        });
    }
}
