/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.business.ValidCodeBusiness;
import com.idear.user.center.web.form.RegistryForm;
import com.idear.user.center.web.form.ResetPasswordForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @author CcShan
 * @date 2018/9/2
 */
@Controller
@RequestMapping(value = "password")
public class PasswordController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private ValidCodeBusiness validCodeBusiness;

    @Autowired
    private MemberBusiness memberBusiness;

    /**
     * 重置登录
     * @return
     */
    @RequestMapping(value = "reset",method = RequestMethod.POST)
    @ResponseBody
    public String reset(@RequestBody @Valid final ResetPasswordForm resetPasswordForm , BindingResult bindingResult){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                validCodeBusiness.validateSmsValidCode(resetPasswordForm.getMobileNo(),resetPasswordForm.getValidCode());
                return memberBusiness.resetLoginPassword(resetPasswordForm.getMobileNo(),resetPasswordForm.getPassword());
            }
        },bindingResult);
    }
}
