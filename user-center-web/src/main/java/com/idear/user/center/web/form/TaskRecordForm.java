/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.form;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/17
 */
public class TaskRecordForm {
    /**
     * 任务id
     */
    @NotNull(message = "请输入记录Id")
    private Integer recordId;

    private List<String> auditInfo;

    @NotNull
    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(@NotNull Integer recordId) {
        this.recordId = recordId;
    }

    public List<String> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<String> auditInfo) {
        this.auditInfo = auditInfo;
    }
}
