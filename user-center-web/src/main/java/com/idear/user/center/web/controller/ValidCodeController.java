/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.AesUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.patchca.service.Captcha;
import com.idear.patchca.service.ConfigurableCaptchaService;
import com.idear.user.center.web.business.ValidCodeBusiness;
import com.idear.user.center.web.constants.CacheConstant;
import com.idear.user.center.web.form.SmsValidCodeForm;
import com.idear.user.center.web.message.WebMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
@Controller
@RequestMapping("valid/code")
public class ValidCodeController {

    private static final String AES_KEY = "IegW9zKiVagnyrUKxJZxOQ==";

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ConfigurableCaptchaService configurableCaptchaService;

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private ValidCodeBusiness validCodeBusiness;

    /**
     * 获取图片验证码
     * @param response
     */
    @RequestMapping(value = "picture", method = RequestMethod.POST)
    public void getValidCode(HttpServletResponse response) {
        Captcha captcha = configurableCaptchaService.getCaptcha();
        String challenge = captcha.getChallenge();
        String ticket = generateTicket(challenge);
        //5分钟失效
        redisTemplate.opsForValue().set(CacheConstant.USER_VALID_CODE_TICKET_INDEX + ticket, challenge, 300, TimeUnit.SECONDS);
        response.setHeader("ticket", ticket);
        BufferedImage image = captcha.getImage();
        response.setContentType("image/png");
        try {
            OutputStream os = response.getOutputStream();
            ImageIO.write(image, "png", os);
        } catch (IOException e) {
            throw new BusinessException(WebMessage.GENERATE_VALID_CODE_FAILED);
        }
    }

    private String generateTicket(String challenge) {
        try {
            return AesUtil.encrypt(challenge + System.currentTimeMillis(), AES_KEY);
        } catch (Exception e) {
            throw new BusinessException(WebMessage.GENERATE_VALID_CODE_FAILED);
        }
    }

    /**
     * 发送验证码
     * @param smsValidCodeForm
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "sms", method = RequestMethod.POST)
    @ResponseBody
    public String getValidCode(@RequestBody @Valid final SmsValidCodeForm smsValidCodeForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String ticket = smsValidCodeForm.getTicket();
                String validCode = smsValidCodeForm.getValidCode();
                String mobileNo = smsValidCodeForm.getMobileNo();
                Integer flag = smsValidCodeForm.getFlag();
                validCodeBusiness.validateCaptcha(ticket, validCode);
                return validCodeBusiness.sendSmsValidCode(mobileNo,flag);
            }
        },bindingResult);
    }
}
