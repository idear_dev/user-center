/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.user.center.service.facade.PasswordService;
import com.idear.user.center.web.business.LoginBusiness;
import com.idear.user.center.web.form.PasswordLoginForm;
import com.idear.user.center.web.vo.LoginVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 *
 * @author CcShan
 * @date 2017/12/20
 */
@Component
public class LoginBusinessImpl implements LoginBusiness {

    @Resource
    private PasswordService passwordService;

    /**
     * 登录
     * @param loginForm
     * @return
     */
    @Override
    public Response login(PasswordLoginForm loginForm) {
        String token = validateLoginPassword(loginForm.getMobileNo(),loginForm.getPassword());
        LoginVO loginVO = new LoginVO();
        loginVO.setToken(token);
        return new Response(loginVO);
    }


    /**
     * 验证登录密码
     * @param mobile
     * @param password
     */
    private String validateLoginPassword(String mobile,String password){
        Response response = passwordService.validateLoginPassword(mobile,password);
        if (response == null){
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        if (!response.success()){
            throw new BusinessException(response.getRespCode(),response.getRespMsg());
        }else {
            return (String) response.getRespBody();
        }
    }
}
