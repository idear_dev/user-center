/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.convert;

import com.idear.common.util.DateUtil;
import com.idear.user.center.service.dto.TaskDetailDto;
import com.idear.user.center.service.dto.TaskListDto;
import com.idear.user.center.web.vo.TaskListVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public class TaskConverter {

    public static TaskListVO convert2TaskListVO(TaskListDto taskListDto) {
        TaskListVO taskListVO = new TaskListVO();
        taskListVO.setTotalNum(taskListDto.getTotalNum());
        taskListVO.setTotalPage(taskListDto.getTotalPage());
        List<TaskDetailDto> dtoList = taskListDto.getTaskDetailList();
        List<Map<String, Object>> taskList = new ArrayList<>();
        for (TaskDetailDto taskDetailDto : dtoList) {
            taskList.add(convert2TaskDetailMap(taskDetailDto));
        }
        taskListVO.setTaskList(taskList);
        return taskListVO;
    }

    public static Map<String, Object> convert2TaskDetailMap(TaskDetailDto taskDetailDto) {
        Map<String, Object> temp = new HashMap<>(20);
        temp.put("taskId", taskDetailDto.getTaskId());
        temp.put("taskName", taskDetailDto.getTaskName());
        temp.put("taskDescription",taskDetailDto.getTaskDescription());
        temp.put("appName", taskDetailDto.getAppName());
        temp.put("appLogoUrl", taskDetailDto.getAppLogoUrl());
        temp.put("appPackageName", taskDetailDto.getAppPackageName());
        temp.put("marketName", taskDetailDto.getMarketName());
        temp.put("marketLogoUrl", taskDetailDto.getMarketLogoUrl());
        temp.put("marketPackageName", taskDetailDto.getMarketPackageName());
        temp.put("amount", taskDetailDto.getAmount());
        temp.put("showTime", DateUtil.formatDate(taskDetailDto.getShowTime(), DateUtil.FULL_CHINESE_PATTERN));
        temp.put("startTime", DateUtil.formatDate(taskDetailDto.getStartTime(), DateUtil.FULL_CHINESE_PATTERN));
        temp.put("endTime", DateUtil.formatDate(taskDetailDto.getEndTime(), DateUtil.FULL_CHINESE_PATTERN));
        temp.put("labelList", taskDetailDto.getLabelList());
        temp.put("type", taskDetailDto.getType());
        temp.put("typeInfo", taskDetailDto.getTypeInfo());
        temp.put("totalNum", taskDetailDto.getTotalNum());
        temp.put("remainNum", taskDetailDto.getRemainNum());
        temp.put("receivedNum", taskDetailDto.getReceivedNum());
        temp.put("status", taskDetailDto.getStatus());
        temp.put("tutorialList", taskDetailDto.getTutorialList());
        return temp;
    }

}
