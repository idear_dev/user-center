/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.core.enums.ConfigStatus;
import com.idear.user.center.service.dto.QuestionDto;
import com.idear.user.center.service.facade.ConfigService;
import com.idear.user.center.web.business.CommonBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/19
 */
@Component
public class CommonBusinessImpl implements CommonBusiness {

    @Autowired
    private ConfigService configService;

    /**
     * 获取常见问题
     *
     * @return
     */
    @Override
    public Response getQuestion() {
        Response<List<QuestionDto>> response = configService.listQuestion(ConfigStatus.ENABLE.getCode());
        return ResponseUtil.getSimpleResponse(response);
    }
}
