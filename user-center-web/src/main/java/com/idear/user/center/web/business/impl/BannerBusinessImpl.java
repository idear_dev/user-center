/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.BannerDto;
import com.idear.user.center.service.facade.BannerService;
import com.idear.user.center.web.business.BannerBusiness;
import com.idear.user.center.web.convert.BannerConverter;
import com.idear.user.center.web.vo.BannerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author CcShan
 * @date 2018/9/2
 */
@Component
public class BannerBusinessImpl implements BannerBusiness {

    @Autowired
    private BannerService bannerService;

    /**
     * 根据位置信息查询banner列表
     *
     * @param location
     * @return
     */
    @Override
    public Response<List<BannerVO>> getBannerInfo(Integer location) {
        //查询version之后更新的版本信息
        Response<List<BannerDto>> response = bannerService.listBanner(location);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            List<BannerDto> bannerDtoList = response.getRespBody();
            Collections.sort(bannerDtoList);
            //有版本信息
            List<BannerVO> voList = new ArrayList<>();
            if (bannerDtoList != null && !bannerDtoList.isEmpty()) {
                for (BannerDto bannerDto : bannerDtoList){
                    voList.add(BannerConverter.convert2BannerVO(bannerDto));
                }
            }
            return new Response(voList);
        }
    }
}
