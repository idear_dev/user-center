/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.service.dto.WechatDto;
import com.idear.user.center.service.dto.WithdrawDetailDto;
import com.idear.user.center.service.facade.WechatService;
import com.idear.user.center.service.facade.WithdrawService;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.business.WithdrawBusiness;
import com.idear.user.center.web.convert.WithdrawConverter;
import com.idear.user.center.web.vo.WithdrawDetailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 提现业务逻辑处理类
 * Created By CcShan on 2018/1/3.
 * @author Administrator
 */
@Component
public class WithdrawBusinessImpl implements WithdrawBusiness {

    @Autowired
    private MemberBusiness memberBusiness;

    @Resource
    private WithdrawService withdrawService;

    @Resource
    private WechatService wechatService;

    /**
     * 获取提现详情
     *
     * @param token
     * @param serialNo 提现流水号
     * @return
     */
    @Override
    public Response<WithdrawDetailVO> getDetail(String token, String serialNo) {
        MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
        Integer memberId = memberInfoDto.getId();
        Response<WithdrawDetailDto> response = withdrawService.getWithdrawDetail(memberId,serialNo);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            return new Response(WithdrawConverter.convert2WithdrawDetailVO(response.getRespBody()));
        }
    }

    /**
     * 查看微信绑定状态
     *
     * @param memberId
     * @return
     */
    @Override
    public Response wechatBindStatus(Integer memberId) {
        Response<WechatDto> response = wechatService.getWechatDtoByMemberId(memberId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            Map<String,Object> resultMap = new HashMap(4);
            WechatDto dto = response.getRespBody();
            resultMap.put("bind", Boolean.TRUE);
            if (dto == null) {
                resultMap.put("bind", Boolean.FALSE);
            }
            return new Response(resultMap);
        }
    }

    /**
     * 提现申请
     *
     * @param memberId
     * @param amount
     * @return
     */
    @Override
    public Response apply(Integer memberId, Integer amount) {
        Response<String> response = withdrawService.apply(memberId,amount);
        return ResponseUtil.getSimpleResponse(response);
    }

}
