/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.TaskDetailDto;
import com.idear.user.center.service.dto.TaskListDto;
import com.idear.user.center.service.dto.TaskRecordDto;
import com.idear.user.center.service.facade.TaskRecordService;
import com.idear.user.center.service.facade.TaskService;
import com.idear.user.center.web.business.TaskBusiness;
import com.idear.user.center.web.convert.TaskConverter;
import com.idear.user.center.web.vo.TaskListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
@Component
public class TaskBusinessImpl implements TaskBusiness {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskRecordService taskRecordService;

    /**
     * 获取进行中的任务列表
     *
     * @param channel
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Response<TaskListVO> getTaskList(String channel, Integer pageNo, Integer pageSize) {
        Response<TaskListDto> response = taskService.getTaskList(channel, pageNo, pageSize);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            TaskListDto taskListDto = response.getRespBody();
            if (taskListDto == null || taskListDto.getTaskDetailList().isEmpty()) {
                return Response.SUCCESS;
            } else {
                return new Response<>(TaskConverter.convert2TaskListVO(taskListDto));
            }
        }
    }

    /**
     * 查看任务详情
     *
     * @param taskId
     * @return
     */
    @Override
    public Response getTaskDetail(Integer taskId) {
        Response<TaskDetailDto> response = taskService.getTaskDetail(taskId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            TaskDetailDto taskDetailDto = response.getRespBody();
            if (taskDetailDto == null) {
                return Response.SUCCESS;
            } else {
                return new Response<>(TaskConverter.convert2TaskDetailMap(taskDetailDto));
            }
        }
    }

    /**
     * 查看用户任务状态
     *
     * @param memberId
     * @param taskId
     * @return
     */
    @Override
    public Response canReceiveTask(Integer memberId, Integer taskId) {
        Response<TaskRecordDto> response = taskRecordService.getEffectiveTaskRecord(memberId, taskId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            Map<String, Object> resultMap = new HashMap<>(4);
            TaskRecordDto taskRecordDto = response.getRespBody();
            if (taskRecordDto == null){
                resultMap.put("result", Boolean.TRUE);
            }else {
                resultMap.put("result", Boolean.FALSE);
                resultMap.put("recordId", taskRecordDto.getRecordId());
                resultMap.put("status", taskRecordDto.getStatus());
            }
            return new Response<>(resultMap);
        }
    }

    /**
     * 领取任务
     *
     * @param memberId
     * @param taskId
     * @return
     */
    @Override
    public Response receiveTask(Integer memberId, Integer taskId) {
        Response<Integer> response = taskRecordService.receiveTask(memberId, taskId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            Integer recordId = response.getRespBody();
            Map<String,Object> resultMap = new HashMap<>(2);
            resultMap.put("recordId",recordId);
            return new Response(resultMap);
        }
    }
}
