/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.constants.SystemHeader;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.web.business.MemberBusiness;
import com.idear.user.center.web.business.TaskBusiness;
import com.idear.user.center.web.form.PageQueryForm;
import com.idear.user.center.web.form.TaskForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
@Controller
@RequestMapping(value = "task")
public class TaskController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private TaskBusiness taskBusiness;

    @Autowired
    private MemberBusiness memberBusiness;

    /**
     * 获取任务列表
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ResponseBody
    public String list(@Valid @RequestBody final PageQueryForm pageQueryForm) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer pageSize = pageQueryForm.getPageSize();
                Integer pageNo = pageQueryForm.getPageNo();
                String channel = GlobalWebContext.getRequest().getHeader(SystemHeader.CHANNEL);
                return taskBusiness.getTaskList(channel, pageNo, pageSize);
            }
        });
    }

    /**
     * 获取任务详情
     *
     * @return
     */
    @RequestMapping(value = "detail", method = RequestMethod.POST)
    @ResponseBody
    public String detail(@Valid @RequestBody final TaskForm taskForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer taskId = taskForm.getTaskId();
                return taskBusiness.getTaskDetail(taskId);
            }
        }, bindingResult);
    }

    /**
     * 领取任务
     *
     * @return
     */
    @RequestMapping(value = "receive", method = RequestMethod.POST)
    @ResponseBody
    public String receiveTask(@Valid @RequestBody final TaskForm taskForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer taskId = taskForm.getTaskId();
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskBusiness.receiveTask(memberId,taskId);
            }
        }, bindingResult);
    }


    /**
     * 用户是否可以参加任务
     *
     * @return
     */
    @RequestMapping(value = "member/status", method = RequestMethod.POST)
    @ResponseBody
    public String canReceiveTask(@Valid @RequestBody final TaskForm taskForm, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer taskId = taskForm.getTaskId();
                String token = GlobalWebContext.getRequest().getHeader(SystemHeader.TOKEN);
                MemberInfoDto memberInfoDto = memberBusiness.getMemberInfoByToken(token);
                Integer memberId = memberInfoDto.getId();
                return taskBusiness.canReceiveTask(memberId, taskId);
            }
        }, bindingResult);
    }
}
