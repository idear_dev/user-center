/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.form;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
public class SmsValidCodeForm {


    /**
     * 注册手机号
     */
    @NotBlank(message = "请输入注册手机号")
    private String mobileNo;

    /**
     * 验证码ticket
     */
    @NotBlank(message = "请刷新验证码")
    private String ticket;

    /**
     * 验证码
     */
    @NotBlank(message = "请输入验证码")
    private String validCode;

    /**
     * flag
     */
    @NotNull(message = "参数校验失败")
    private Integer flag;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getValidCode() {
        return validCode;
    }

    public void setValidCode(String validCode) {
        this.validCode = validCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}
