/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.web.vo.TaskListVO;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public interface TaskBusiness {

    /**
     * 获取进行中的任务列表
     *
     * @param channel  渠道
     * @param pageNo   页数
     * @param pageSize 每页的大小
     * @return
     */
    Response<TaskListVO> getTaskList(String channel, Integer pageNo, Integer pageSize);

    /**
     * 查看任务详情
     *
     * @param taskId
     * @return
     */
    Response getTaskDetail(Integer taskId);

    /**
     * 用户是否可以参加活动
     *
     * @param memberId
     * @param taskId
     * @return
     */
    Response canReceiveTask(Integer memberId,Integer taskId);

    /**
     * 领取任务
     * @param memberId
     * @param taskId
     * @return
     */
    Response receiveTask(Integer memberId,Integer taskId);
}
