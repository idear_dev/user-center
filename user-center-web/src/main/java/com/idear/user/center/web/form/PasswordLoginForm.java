/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.form;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author CcShan
 * @date 2017/12/19
 */
public class PasswordLoginForm {

    /**
     * 注册手机号
     */
    @NotBlank(message = "请输入登录手机号")
    private String mobileNo;

    /**
     * 登录密码
     */
    @NotBlank(message = "请输入登录密码")
    private String password;


    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
