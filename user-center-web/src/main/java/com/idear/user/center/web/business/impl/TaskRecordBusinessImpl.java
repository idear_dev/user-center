/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business.impl;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.DateUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.response.Response;
import com.idear.framework.util.ResponseUtil;
import com.idear.user.center.core.enums.TaskRecordStatus;
import com.idear.user.center.service.dto.AccountInfoDto;
import com.idear.user.center.service.dto.TaskRecordDto;
import com.idear.user.center.service.dto.TaskRecordListDto;
import com.idear.user.center.service.facade.AccountService;
import com.idear.user.center.service.facade.TaskRecordService;
import com.idear.user.center.service.param.TaskRecordListParam;
import com.idear.user.center.web.business.TaskRecordBusiness;
import com.idear.user.center.web.form.ListRecordForm;
import com.idear.user.center.web.convert.TaskRecordConverter;
import com.idear.user.center.web.vo.TaskRecordListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author CcShan
 * @date 2018/9/17
 */
@Component
public class TaskRecordBusinessImpl implements TaskRecordBusiness {

    @Autowired
    private TaskRecordService taskRecordService;

    @Autowired
    private AccountService accountService;

    /**
     * 取消任务
     *
     * @param memberId
     * @param recordId
     * @return
     */
    @Override
    public Response cancelTask(Integer memberId, Integer recordId) {
        Response response = taskRecordService.cancelTask(memberId, recordId);
        return  ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 提交审核
     *
     * @param memberId
     * @param recordId
     * @param auditInfo
     * @return
     */
    @Override
    public Response submitAudit(Integer memberId, Integer recordId, List<String> auditInfo) {
        if (auditInfo == null || auditInfo.isEmpty()) {
            throw new BusinessException(BaseMessage.ERROR_PARAM);
        }
        Response response = taskRecordService.submitAudit(memberId, recordId, auditInfo);
        return ResponseUtil.getSimpleResponse(response);
    }

    /**
     * 查询用户任务记录
     *
     * @param memberId
     * @param listRecordForm
     * @return
     */
    @Override
    public Response<TaskRecordListVO> getTaskRecordList(Integer memberId, ListRecordForm listRecordForm) {
        TaskRecordListParam taskRecordListParam = new TaskRecordListParam();
        taskRecordListParam.setMemberId(memberId);
        if (StringUtil.isNotEmpty(listRecordForm.getDate())) {
            Date beginTime = DateUtil.parseDate(listRecordForm.getDate() + " 00:00:01", DateUtil.FULL_CHINESE_PATTERN);
            Date endTime = DateUtil.parseDate(listRecordForm.getDate() + " 23:00:00", DateUtil.FULL_CHINESE_PATTERN);
            taskRecordListParam.setBeginTime(beginTime);
            taskRecordListParam.setEndTime(endTime);
        }
        taskRecordListParam.setPageNo(listRecordForm.getPageNo());
        taskRecordListParam.setPageSize(listRecordForm.getPageSize());
        taskRecordListParam.setStatusList(listRecordForm.getStatusList());
        Response<TaskRecordListDto> response = taskRecordService.getRecordList(taskRecordListParam);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        } else {
            TaskRecordListDto taskRecordListDto = response.getRespBody();
            if (taskRecordListDto == null || taskRecordListDto.getTaskRecordList().isEmpty()) {
                return Response.SUCCESS;
            } else {
                return new Response<>(TaskRecordConverter.convert2TaskRecordListVO(taskRecordListDto));
            }
        }
    }

    /**
     * 根据会员编号查询用户当前成就
     *
     * @param memberId
     * @return
     */
    @Override
    public Response getAchievement(Integer memberId) {
        Map<String,Object> resultMap = new HashMap<>(3);
        resultMap.put("todayIncome",getTodayIncome(memberId));
        resultMap.put("todayCompletedTaskNum",getTodayCompletedTaskNum(memberId));
        return new Response(resultMap);
    }

    /**
     * 获取任务记录详情
     *
     * @param memberId
     * @param recordId
     * @return
     */
    @Override
    public Response getTaskRecordDetail(Integer memberId, Integer recordId) {
        Response<TaskRecordDto> response = taskRecordService.getRecordDetail(memberId,recordId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        }else {
            TaskRecordDto taskRecordDto = response.getRespBody();
            if (taskRecordDto == null){
                return Response.SUCCESS;
            }else {
                return new Response(TaskRecordConverter.convert2TaskDetailMap(taskRecordDto));
            }
        }
    }

    /**
     * 获取进入收入
     * @param memberId
     * @return
     */
    private Integer getTodayIncome(Integer memberId){
        Response<AccountInfoDto> response = accountService.getAccountInfo(memberId);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        }else {
            return response.getRespBody().getTodayIncome();
        }
    }

    /**
     * 查看进入完成任务数
     * @param memberId
     * @return
     */
    private Integer getTodayCompletedTaskNum(Integer memberId) {
        TaskRecordListParam taskRecordListParam = new TaskRecordListParam();
        taskRecordListParam.setMemberId(memberId);
        String nowDate = DateUtil.getDefaultCurrentDate();
        taskRecordListParam.setBeginTime(DateUtil.parseDate(nowDate + " 00:00:01", DateUtil.FULL_CHINESE_PATTERN));
        taskRecordListParam.setEndTime(DateUtil.parseDate(nowDate + " 23:00:00", DateUtil.FULL_CHINESE_PATTERN));
        taskRecordListParam.setPageNo(1);
        taskRecordListParam.setPageSize(10);
        List<Integer> statusList = new ArrayList<>();
        statusList.add(TaskRecordStatus.AUDIT_SUCCEED.getCode());
        statusList.add(TaskRecordStatus.TO_AUDIT.getCode());
        taskRecordListParam.setStatusList(statusList);
        Response<TaskRecordListDto> response = taskRecordService.getRecordList(taskRecordListParam);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        } else if (!response.success()) {
            throw new BusinessException(response.getRespCode(), response.getRespMsg());
        }else {
            return response.getRespBody().getTotalNum();
        }
    }

}
