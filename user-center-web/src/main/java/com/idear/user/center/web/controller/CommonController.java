/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.controller;

import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.web.business.CommonBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author CcShan
 * @date 2018/9/19
 */
@Controller
@RequestMapping(value = "common")
public class CommonController {

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private CommonBusiness commonBusiness;

    /**
     * 获取常见问题
     *
     * @return
     */
    @RequestMapping(value = "/question", method = RequestMethod.POST)
    @ResponseBody
    public String getQuestion(){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                return commonBusiness.getQuestion();
            }
        });
    }
}
