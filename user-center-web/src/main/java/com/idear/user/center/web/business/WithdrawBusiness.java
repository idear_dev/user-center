/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.web.business;

import com.idear.framework.response.Response;
import com.idear.user.center.web.vo.WithdrawDetailVO;

/**
 * Created By CcShan on 2018/1/3.
 * @author Administrator
 */
public interface WithdrawBusiness {

    /**
     * 获取提现详情
     * @param token
     * @param serialNo
     * @return
     */
    Response<WithdrawDetailVO> getDetail(String token,String serialNo);

    /**
     * 查看微信绑定状态
     * @param memberId
     * @return
     */
    Response wechatBindStatus(Integer memberId);

    /**
     * 提现申请
     * @param memberId
     * @param amount
     * @return
     */
    Response apply(Integer memberId,Integer amount);
}
