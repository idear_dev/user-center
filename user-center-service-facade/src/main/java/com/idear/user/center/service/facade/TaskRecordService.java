/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.TaskRecordDto;
import com.idear.user.center.service.dto.TaskRecordListDto;
import com.idear.user.center.service.param.TaskRecordListParam;

import java.util.List;
import java.util.Map;


/**
 * @author CcShan
 * @date 2018/9/16
 */
public interface TaskRecordService {

    /**
     * 获取有效任务记录
     * @param memberId
     * @param taskId
     * @return
     */
    Response<TaskRecordDto> getEffectiveTaskRecord(Integer memberId,Integer taskId);

    /**
     * 领取任务
     * @param memberId
     * @param taskId
     * @return
     */
    Response<Integer> receiveTask(Integer memberId,Integer taskId);

    /**
     * 取消任务
     * @param memberId
     * @param recordId
     * @return
     */
    Response cancelTask(Integer memberId,Integer recordId);

    /**
     * 提交审核
     * @param memberId
     * @param recordId
     * @param auditInfo
     * @return
     */
    Response submitAudit(Integer memberId, Integer recordId , List<String> auditInfo);

    /**
     * 查询任务记录
     * @param param
     * @return
     */
    Response<TaskRecordListDto> getRecordList(TaskRecordListParam param);

    /**
     * 查询任务记录详情
     * @param memberId
     * @param recordId
     * @return
     */
    Response<TaskRecordDto> getRecordDetail(Integer memberId,Integer recordId);

    /**
     * 获取状态信息
     * @return
     */
    Response<List<Map<String,Object>>> listStatus();

    /**
     * 管理端审核
     * @param recordId
     * @param pass
     * @param remark
     * @return
     */
    Response audit(Integer recordId,Boolean pass,String remark);

}
