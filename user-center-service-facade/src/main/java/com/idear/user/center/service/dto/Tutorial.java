/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.dto;

import java.io.Serializable;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
public class Tutorial implements Serializable{

    private String name;

    private String textExplain;

    private String imageExplain;

    private Boolean needCapture;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextExplain() {
        return textExplain;
    }

    public void setTextExplain(String textExplain) {
        this.textExplain = textExplain;
    }

    public String getImageExplain() {
        return imageExplain;
    }

    public void setImageExplain(String imageExplain) {
        this.imageExplain = imageExplain;
    }

    public Boolean getNeedCapture() {
        return needCapture;
    }

    public void setNeedCapture(Boolean needCapture) {
        this.needCapture = needCapture;
    }
}
