/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.dto;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
public class TaskPlanListDto {

    private Integer totalNum;

    private Integer totalPage;

    private List<TaskPlanDto> taskPlanList;

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<TaskPlanDto> getTaskPlanList() {
        return taskPlanList;
    }

    public void setTaskPlanList(List<TaskPlanDto> taskPlanList) {
        this.taskPlanList = taskPlanList;
    }
}
