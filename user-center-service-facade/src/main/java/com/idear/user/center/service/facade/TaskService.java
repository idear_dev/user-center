/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.TaskDetailDto;
import com.idear.user.center.service.dto.TaskListDto;


/**
 * @author ShanCc
 * @date 2018/9/15
 */
public interface TaskService {

    /**
     * 获取任务列表
     * @param channel
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<TaskListDto> getTaskList(String channel, Integer pageNo, Integer pageSize);

    /**
     * 获取任务详情
     * @param taskId
     * @return
     */
    Response<TaskDetailDto> getTaskDetail(Integer taskId);

}
