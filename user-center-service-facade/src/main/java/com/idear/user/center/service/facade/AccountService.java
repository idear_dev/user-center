/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;


import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.AccountInfoDto;
import com.idear.user.center.service.dto.AccountRecordListDto;

/**
 *
 * @author CcShan
 * @date 2018/1/2
 */
public interface AccountService {

    /**
     * 获取账户信息
     * @param memberId
     * @return
     */
    Response<AccountInfoDto> getAccountInfo(Integer memberId);

    /**
     * 分期查询收入明细列表
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<AccountRecordListDto> getIncomeList(Integer memberId, Integer pageNo, Integer pageSize);

    /**
     * 获取成功提现列表
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<AccountRecordListDto> getHasWithdrawList(Integer memberId, Integer pageNo, Integer pageSize);

    /**
     * 获取提现审核中的列表
     * @param memberId
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<AccountRecordListDto> getWithdrawingList(Integer memberId, Integer pageNo, Integer pageSize);
}
