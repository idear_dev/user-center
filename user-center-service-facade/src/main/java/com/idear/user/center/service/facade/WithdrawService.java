/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.WithdrawDetailDto;
import com.idear.user.center.service.dto.WithdrawDetailListDto;
import com.idear.user.center.service.param.WithdrawListParam;

import java.util.List;
import java.util.Map;

/**
 *
 * 提现相关服务类
 * Created By CcShan on 2018/1/3.
 * @author Administrator
 */
public interface WithdrawService {

    /**
     * 获取提现记录明细
     * @param serialNo 提现流水号
     * @param memberId 会员Id
     * @return
     */
    Response<WithdrawDetailDto> getWithdrawDetail(Integer memberId, String serialNo);

    /**
     * 提现申请
     * @param memberId
     * @param amount
     * @return
     */
    Response<String> apply(Integer memberId,Integer amount);

    /**
     * 获取提现申请状态信息
     * @return
     */
    Response<List<Map<String,Object>>> listStatus();

    /**
     * 查询提现记录列表
     * @param param
     * @return
     */
    Response<WithdrawDetailListDto> getWithdrawDetailList(WithdrawListParam param);
}
