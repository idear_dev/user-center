/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.MarketDto;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/10/24
 */
public interface MarketService {

    /**
     * 根据位置信息获取banner信息
     * @return
     */
    Response<List<MarketDto>> listMarket();

    /**
     * 添加广告信息
     * @param marketDto
     * @return
     */
    Response<Integer> addMarket(MarketDto marketDto);

    /**
     * 更新banner信息
     * @param marketDto
     * @return
     */
    Response updateMarket(MarketDto marketDto);
}
