/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;

import java.io.InputStream;

/**
 * @author CcShan
 * @date 2018/9/18
 */
public interface FileService {

    /**
     * 上传文件
     * @param inputStream
     * @param path 相对路径
     * @return
     */
    Response<String> uploadFile(InputStream inputStream,String path);
}
