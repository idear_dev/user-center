/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public class TaskListDto implements Serializable {

    private Integer totalNum;

    private Integer totalPage;

    private List<TaskDetailDto> taskDetailList;

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<TaskDetailDto> getTaskDetailList() {
        return taskDetailList;
    }

    public void setTaskDetailList(List<TaskDetailDto> taskDetailList) {
        this.taskDetailList = taskDetailList;
    }
}
