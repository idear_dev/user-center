/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.message;

import com.idear.common.message.Message;

/**
 * @author CcShan
 * @date 2017/12/20
 */
public enum ServiceMessage implements Message {

    /**
     * web错误码区间(7200~7400]
     */
    MOBILE_NO_NOT_REGISTRY("7201", "手机号未注册"),
    MOBILE_NO_IS_DISABLE("7202", "手机号不可用"),
    PASSWORD_OR_MOBILE_ERROR("7203", "手机号和密码不匹配"),
    SAVE_MEMBER_FAILED("7204", "保存用户信息失败"),
    QUERY_WITHDRAW_DETAIL_FAILED("7205", "查询提现详情失败"),
    BANNER_LOCATION_NOT_EXIST("7206", "广告位信息不存在"),

    TASK_DETAIL_NOT_EXIST("7207", "任务信息不存在"),
    HAS_RECEIVE_TASK("7208", "已经参加过该任务"),
    TASK_NOT_START("7209", "任务未开始"),
    TASK_HAS_END("7210", "任务已结束"),
    TASK_HAS_OVER("7211", "任务被抢光了"),
    TASK_RECORD_STATUS_EXCEPTION("7299","任务记录状态异常"),
    TASK_RECORD_NOT_EXIST("7300", "任务记录不存在"),
    RECORD_CAN_NOT_CANCEL("7301", "不可以取消"),
    UPLOAD_FILE_FAILED("7302", "上传文件失败"),
    RECORD_CAN_NOT_SUBMIT_AUDIT("7303", "不可以提交审核"),
    BANNER_NOT_EXIT("7304", "Banner信息不存在"),
    MARKET_NOT_EXIT("7305", "Market信息不存在"),
    APP_ID_IS_NULL("7306", "AppId为空"),
    APP_INFO_NOT_EXIST("7307", "AppInfo不存在"),
    VERSION_ID_IS_NULL("7308", "VersionId为空"),
    VERSION_INFO_NOT_EXIST("7309", "Version不存在"),
    QUESTION_ID_IS_NULL("7310", "QuestionId为空"),
    QUESTION_INFO_NOT_EXIST("7311", "QuestionInfo不存在"),
    TASK_PLAN_ID_IS_NULL("7312", "TaskPlan不存在"),
    TASK_PLAN_INFO_NOT_EXIST("7313", "TaskPlan为空"),
    TASK_DETAIL_ID_IS_NULL("7314", "TaskDetail不存在"),
    TASK_DETAIL_INFO_NOT_EXIST("7315", "TaskDetail为空"),
    APPLY_AMOUNT_IS_ILLEGAL("7316", "提现金额至少需要20元"),
    APPLY_AMOUNT_IS_OVER("7317", "提现金额超过账户余额"),
    WECHAT_HAS_BIND_MEMBER("7319", "您已经成功绑定微信"),
    WECHAT_NOT_BIND("7318", "请关注微信公众号[入米客]绑定手机号，再进行提现操作");

    private String code;

    private String msg;

    ServiceMessage(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getRespCode() {
        return this.code;
    }

    @Override
    public String getRespMsg() {
        return this.msg;
    }
}
