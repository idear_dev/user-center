/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.TaskPlanDto;
import com.idear.user.center.service.dto.TaskPlanListDto;
import com.idear.user.center.service.param.TaskPlanListParam;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
public interface TaskPlanService {

    /**
     * 查询任务列表
     * @param param
     * @return
     */
    Response<TaskPlanListDto> queryTaskPlan(TaskPlanListParam param);

    /**
     * 新增任务计划
     * @param dto
     * @return
     */
    Response<Integer> addTaskPlan(TaskPlanDto dto);

    /**
     * 更新任务计划
     * @param dto
     * @return
     */
    Response updateTaskPlan(TaskPlanDto dto);
}
