/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/9/15
 */
public class TaskDetailDto implements Serializable{

    /**
     * 任务Id
     */
    private Integer taskId;

    /**
     * App名称
     */
    private String appName;

    /**
     * appLogo地址
     */
    private String appLogoUrl;

    /**
     * app包名
     */
    private String appPackageName;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务详细说明
     */
    private String taskDescription;

    /**
     * 奖励金额
     */
    private Integer amount;

    /**
     * 展示时间
     */
    private Date showTime;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 总数
     */
    private Integer totalNum;

    /**
     * 已用数量
     */
    private Integer receivedNum;

    /**
     * 剩余数量
     */
    private Integer remainNum;

    /**
     * 标签列表
     */
    private List labelList;

    /**
     * 使用教程列表
     */
    private List tutorialList;

    /**
     * 应用市场信息
     */
    private String marketName;

    /**
     * 应用市场Url
     */
    private String marketLogoUrl;

    /**
     * 应用市场包名
     */
    private String marketPackageName;

    /**
     * 任务状态
     */
    private Integer status;

    /**
     * 任务类型
     */
    private Integer type;

    /**
     * 任务类型相关信息
     */
    private String typeInfo;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getShowTime() {
        return showTime;
    }

    public void setShowTime(Date showTime) {
        this.showTime = showTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getReceivedNum() {
        return receivedNum;
    }

    public void setReceivedNum(Integer receivedNum) {
        this.receivedNum = receivedNum;
    }

    public Integer getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(Integer remainNum) {
        this.remainNum = remainNum;
    }

    public List getLabelList() {
        return labelList;
    }

    public void setLabelList(List labelList) {
        this.labelList = labelList;
    }

    public List getTutorialList() {
        return tutorialList;
    }

    public void setTutorialList(List tutorialList) {
        this.tutorialList = tutorialList;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeInfo() {
        return typeInfo;
    }

    public void setTypeInfo(String typeInfo) {
        this.typeInfo = typeInfo;
    }

    public String getAppLogoUrl() {
        return appLogoUrl;
    }

    public void setAppLogoUrl(String appLogoUrl) {
        this.appLogoUrl = appLogoUrl;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getAppPackageName() {
        return appPackageName;
    }

    public void setAppPackageName(String appPackageName) {
        this.appPackageName = appPackageName;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketLogoUrl() {
        return marketLogoUrl;
    }

    public void setMarketLogoUrl(String marketLogoUrl) {
        this.marketLogoUrl = marketLogoUrl;
    }

    public String getMarketPackageName() {
        return marketPackageName;
    }

    public void setMarketPackageName(String marketPackageName) {
        this.marketPackageName = marketPackageName;
    }
}
