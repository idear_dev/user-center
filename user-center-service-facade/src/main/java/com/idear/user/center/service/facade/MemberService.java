/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.MemberInfoDto;


/**
 * Created By CcShan on 2017/12/20.
 * @author Administrator
 */
public interface MemberService {

    /**
     * 检查Token有效性，每检查一次延长Token有效时间
     * @param token
     * @return
     */
    Response<Boolean> checkToken(String token);

    /**
     * 按照Token获取会员信息
     * @param token
     * @return
     */
    Response<MemberInfoDto> getMemberByToken(String token);

    /**
     * 按照会员Id获取会员信息
     * @param memberId
     * @return
     */
    Response<MemberInfoDto> getMemberById(Integer memberId);

    /**
     * 判断手机号是否已注册
     * @param mobileNo
     * @return
     */
    Response<Boolean> checkMobileNoIsRegistry(String mobileNo);

    /**
     * 根据手机号查询会员信息
     * @param mobileNo
     * @return
     */
    Response<MemberInfoDto> getMemberByMobileNo(String mobileNo);

    /**
     * 手机号码注册
     * @param mobileNo
     * @param password
     * @return
     */
    Response registry(String mobileNo,String password);

    /**
     * 重置密码
     * @param mobileNo
     * @param password
     * @return
     */
    Response resetLoginPassword(String mobileNo,String password);

    /**
     * 登出
     * @param token
     * @return
     */
    Response logout(String token);
}
