/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户端版本信息配置
 *
 * @author CcShan
 * @date 2018/8/28
 */
public class ClientVersionDto implements Serializable, Comparable<ClientVersionDto> {

    /**
     * 配置Id
     */
    private Integer id;

    /**
     * 版本信息
     */
    private Integer version;

    /**
     * 版本描述信息
     */
    private String description;

    /**
     * 下载地址
     */
    private String downloadUrl;

    /**
     * 是否需要强制升级
     */
    private Boolean forceUpdate;

    /**
     * 渠道IOS/ANDROID
     */
    private String channel;

    /**
     * 状态
     */
    private Integer status;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public int compareTo(ClientVersionDto o) {
        if (version > o.getVersion()) {
            return 1;
        } else if (version.equals(o.getVersion())) {
            return 0;
        } else {
            return -1;
        }
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
