/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;


import com.idear.framework.response.Response;

/**
 *
 * @author CcShan
 * @date 2017/12/20
 */
public interface PasswordService {

    /**
     * 验证登录密码
     * @param mobileNo
     * @param password
     * @return
     */
    Response validateLoginPassword(String mobileNo, String password);
}
