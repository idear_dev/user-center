/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.TaskPlanDetailDto;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/2
 */
public interface TaskDetailService {

    /**
     * 查询任务详情列表
     * @param planId
     * @return
     */
    Response<List<TaskPlanDetailDto>> queryTaskDetail(Integer planId);

    /**
     * 新增任务详情
     * @param dto
     * @return
     */
    Response<Integer> addTaskDetail(TaskPlanDetailDto dto);

    /**
     * 更新任务详情
     * @param dto
     * @return
     */
    Response updateTaskDetail(TaskPlanDetailDto dto);
}
