/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.ClientVersionDto;
import com.idear.user.center.service.dto.ClientVersionListDto;
import com.idear.user.center.service.dto.QuestionDto;

import java.util.List;

/**
 * @author CcShan
 * @date 2018/8/28
 */
public interface ConfigService {

    /**
     * 查询比当前版本还要新的客户端版本信息
     * @param channel
     * @param version
     * @return
     */
    Response<List<ClientVersionDto>> listNewClientVersion(String channel,Integer version);

    /**
     * 分页查询所有客户端版本信息
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<ClientVersionListDto> listClientVersion(Integer pageNo, Integer pageSize);

    /**
     * 添加版本信息
     * @param clientVersionDto
     * @return
     */
    Response<Integer> addClientVersion(ClientVersionDto clientVersionDto);

    /**
     * 更新版本信息
     * @param clientVersionDto
     * @return
     */
    Response updateClientVersion(ClientVersionDto clientVersionDto);

    /**
     * 更新版本信息
     * @param versionId
     * @return
     */
    Response deleteClientVersion(Integer versionId);

    /**
     * 查看所有的QA信息
     * @param status
     * @return
     */
    Response<List<QuestionDto>> listQuestion(Integer status);

    /**
     * 添加QA信息
     * @param questionDto
     * @return
     */
    Response<Integer> addQuestion(QuestionDto questionDto);

    /**
     * 更新QA信息
     * @param questionDto
     * @return
     */
    Response updateQuestion(QuestionDto questionDto);

    /**
     * 删除QA信息
     * @param questionId
     * @return
     */
    Response deleteQuestion(Integer questionId);
}
