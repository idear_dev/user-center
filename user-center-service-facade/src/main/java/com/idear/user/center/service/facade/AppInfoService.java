/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.AppInfoDto;
import com.idear.user.center.service.dto.AppInfoListDto;

/**
 * @author ShanCc
 * @date 2018/10/25
 */
public interface AppInfoService {

    /**
     * 模糊查询App信息
     * @param appInfoDto
     * @param pageNo
     * @param pageSize
     * @return
     */
    Response<AppInfoListDto> listAppInfo(AppInfoDto appInfoDto, Integer pageNo, Integer pageSize);

    /**
     * 添加App信息
     * @param appInfoDto
     * @return
     */
    Response<Integer> addAppInfo(AppInfoDto appInfoDto);

    /**
     * 更新banner信息
     * @param appInfoDto
     * @return
     */
    Response updateAppInfo(AppInfoDto appInfoDto);
}
