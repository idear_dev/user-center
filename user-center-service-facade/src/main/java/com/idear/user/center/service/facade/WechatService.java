/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.service.facade;


import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.WechatDto;

/**
 * @author ShanCc
 * @date 2018/11/6
 */
public interface WechatService {

    /**
     * 根据memberId获取微信信息
     * @param memberId
     * @return
     */
    Response<WechatDto> getWechatDtoByMemberId(Integer memberId);

    /**
     * 微信用户关注公众号
     * @param wechatDto
     * @return
     */
    Response subscribe(WechatDto wechatDto);

    /**
     * 绑定会员信息
     * @param memberId
     * @param openId
     * @return
     */
    Response bindMember(Integer memberId,String openId);
}
