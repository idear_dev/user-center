/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.user.center.service.facade;

import com.idear.framework.response.Response;
import com.idear.user.center.service.dto.BannerDto;

import java.util.List;
import java.util.Map;

/**
 * @author CcShan
 * @date 2018/9/2
 */
public interface BannerService {

    /**
     * 根据位置信息获取banner信息
     * @param location
     * @return
     */
    Response<List<BannerDto>> listBanner(Integer location);

    /**
     * 根据位置信息获取banner信息
     * @return
     */
    Response<List<BannerDto>> listBanner();

    /**
     * 获取Banner位置信息
     * @return
     */
    Response<List<Map<String,Object>>> listLocation();

    /**
     * 添加广告信息
     * @param bannerDto
     * @return
     */
    Response<Integer> addBanner(BannerDto bannerDto);

    /**
     * 删除Banner信息
     * @param bannerId
     * @return
     */
    Response deleteBanner(Integer bannerId);

    /**
     * 更新banner信息
     * @param bannerDto
     * @return
     */
    Response updateBanner(BannerDto bannerDto);
}
