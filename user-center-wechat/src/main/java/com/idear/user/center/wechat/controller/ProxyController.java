/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.controller;

import com.idear.common.util.JsonUtil;
import com.idear.framework.context.GlobalWebContext;
import com.idear.user.center.wechat.message.WechatMessage;
import com.idear.user.center.wechat.message.factory.HandlerFactory;
import com.idear.user.center.wechat.message.handler.MessageHandler;
import com.idear.user.center.wechat.util.CheckUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ShanCc
 * @date 2018/12/11
 */
@Controller
@RequestMapping(value = "proxy")
public class ProxyController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HandlerFactory handlerFactory;

    @Value("${wechatToken}")
    private String wechatToken;

    @RequestMapping(value = "message")
    @ResponseBody
    public String pushMessage(@RequestBody String body){
        HttpServletRequest request = GlobalWebContext.getRequest();
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        if(CheckUtil.checkSignature(wechatToken,signature, timestamp, nonce)){
            //如果校验成功，处理业务逻辑
            WechatMessage message = new WechatMessage(body);
            logger.info("messageMap is {}.", JsonUtil.toJsonString(message.getContent()));
            MessageHandler messageHandler = handlerFactory.getMessageHandler(message.getMessageType());
            if (messageHandler != null){
                messageHandler.handle(message);
            }
        }
        return request.getParameter("echostr");
    }

}
