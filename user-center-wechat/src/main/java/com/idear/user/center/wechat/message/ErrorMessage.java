/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.message;

import com.idear.common.message.Message;

/**
 * @author ShanCc
 * @date 2018/12/13
 */
public enum ErrorMessage implements Message {

    MOBILE_NO_IS_REGISTRY("3000","手机号未注册，请先进行注册"),
    BIND_SMS_VALID_CODE_HAS_SEND("3001","短信验证码已发送，请稍后再试"),
    VALIDATE_SMS_VALID_CODE_FAILED("3002","短信验证码错误，请重新获取"),
    BIND_WECHAT_FAILED("3003","绑定微信失败，请稍后重试");

    private String code;

    private String msg;

    ErrorMessage(String code,String msg){
        this.code = code;
        this.msg = msg;
    }

    /**
     * 错误码区间
     * 10000请求成功
     * 10001～90000业务异常
     * 90000～99999系统异常
     * <p>
     * 会员错误码区间
     * USER-CENTER(7000~8000]
     * 基础服务错误码区间
     * SMS(8000～8200]
     *
     * @return
     */
    @Override
    public String getRespCode() {
        return code;
    }

    /**
     * 响应信息内容
     *
     * @return
     */
    @Override
    public String getRespMsg() {
        return msg;
    }
}
