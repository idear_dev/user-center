/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.job;

import com.alibaba.fastjson.JSONObject;
import com.idear.common.util.HttpUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


/**
 * @author ShanCc
 * @date 2018/12/11
 */
@Component
public class AccessTokenManager {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public static final String WECHAT_TOKEN_INDEX = "WECHAT:TOKEN";

    public static final String WECHAT_JS_TICKET_INDEX = "WECHAT:JS_TICKET";

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${appId}")
    private String appId;

    @Value("${appSecret}")
    private String appSecret;

    @Value("${getTokenUrl}")
    private String getTokenUrl;

    @Value("${getTicketUrl}")
    private String getTicketUrl;

    /**
     * 每一小时执行一次
     */
    @Scheduled(cron = "0 0 */1 * * ?")
    public void cron() {
        updateWechatToken();
        updateWechatTicket();
    }

    public void updateWechatToken() {
        String requestUrl = getTokenUrl + "&appId=" + appId + "&secret=" + appSecret;
        logger.info("requestUrl is {}.", requestUrl);
        String response = HttpUtil.get(requestUrl, null);
        logger.info("response is {}.", response);
        JSONObject jsonObject = JsonUtil.parseJsonObject(response);
        String accessToken = jsonObject.getString("access_token");
        if (StringUtil.isNotEmpty(accessToken)) {
            redisTemplate.opsForValue().set(WECHAT_TOKEN_INDEX, accessToken, 60, TimeUnit.MINUTES);
        }
    }

    public void updateWechatTicket() {
        String accessToken = getToken();
        String requestUrl = getTicketUrl + "&access_token=" + accessToken;
        logger.info("requestUrl is {}.", requestUrl);
        String response = HttpUtil.get(requestUrl, null);
        logger.info("response is {}.", response);
        JSONObject jsonObject = JsonUtil.parseJsonObject(response);
        String ticket = jsonObject.getString("ticket");
        if (StringUtil.isNotEmpty(ticket)) {
            redisTemplate.opsForValue().set(WECHAT_JS_TICKET_INDEX, ticket, 60, TimeUnit.MINUTES);
        }
    }

    public String getToken() {
        String token = redisTemplate.opsForValue().get(WECHAT_TOKEN_INDEX);
        if (StringUtil.isEmpty(token)) {
            updateWechatToken();
            token = redisTemplate.opsForValue().get(WECHAT_TOKEN_INDEX);
        }
        return token;
    }

    public String getJsTicket() {
        String ticket = redisTemplate.opsForValue().get(WECHAT_JS_TICKET_INDEX);
        if (StringUtil.isEmpty(ticket)) {
            updateWechatTicket();
            ticket = redisTemplate.opsForValue().get(WECHAT_JS_TICKET_INDEX);
        }
        return ticket;
    }
}
