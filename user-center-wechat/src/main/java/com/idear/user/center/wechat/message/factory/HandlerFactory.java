/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.message.factory;

import com.idear.user.center.wechat.message.handler.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/12/11
 */
@Component
public class HandlerFactory {

    @Autowired
    private Map<String, MessageHandler> handlerMap;

    /**
     * 根据tag获取MessageHandler
     *
     * @param msgType
     * @return
     */
    public MessageHandler getMessageHandler(String msgType) {
        if (MsgType.EVENT.getType().equals(msgType)) {
            return handlerMap.get("eventHandler");
        } else {
            return null;
        }
    }


    private enum MsgType {

        EVENT("event","事件");

        private String type;

        private String desc;

        MsgType(String type, String desc) {
            this.type = type;
            this.desc = desc;
        }

        public String getType() {
            return type;
        }

        public String getDesc() {
            return desc;
        }
    }
}
