/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.util;

import java.security.MessageDigest;
import java.util.Arrays;

/**
 * @author ShanCc
 * @date 2018/12/11
 */
public class CheckUtil {

    public static boolean checkSignature(String token, String signature, String timestamp, String nonce) {
        //1.定义数组存放token，timestamp,nonce
        String[] arr = {token, timestamp, nonce};
        //2.对数组进行排序
        Arrays.sort(arr);
        //3.生成字符串
        StringBuffer sb = new StringBuffer();
        for (String s : arr) {
            sb.append(s);
        }
        //4.sha1加密
        String temp = sha1(sb.toString());
        //5.将加密后的字符串，与微信传来的加密签名比较，返回结果
        return temp.equals(signature);
    }

    public static String getJsApiSign(String nonceStr, String ticket, Long timestamp, String url) {
        //3.生成字符串
        StringBuffer sb = new StringBuffer();
        sb.append("jsapi_ticket=").append(ticket)
                .append("&noncestr=").append(nonceStr)
                .append("&timestamp=").append(timestamp)
                .append("&url=").append(url);
        //4.sha1加密
        String signature = sha1(sb.toString());
        return signature;
    }

    /**
     * sha1加密
     * @param str
     * @return
     */
    public static String sha1(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }

        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
            mdTemp.update(str.getBytes("UTF-8"));
            byte[] md = mdTemp.digest();
            int j = md.length;
            char buf[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf);
        } catch (Exception e) {
            return null;
        }
    }

    public static final void main(String[] args) {
        String token = "shanchenchen1991";
        boolean result = checkSignature(token, "4d178eb99ce24befd9d736732591c0bda67ac600", "1544519958", "779002929");
        System.out.println(result);
    }
}
