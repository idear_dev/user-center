/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.controller;

import com.alibaba.fastjson.JSONObject;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.CommonUtil;
import com.idear.common.util.HttpUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.framework.util.ResponseUtil;
import com.idear.sms.service.facade.SmsService;
import com.idear.sms.service.param.SendSingleMessageParam;
import com.idear.user.center.service.dto.MemberInfoDto;
import com.idear.user.center.service.facade.MemberService;
import com.idear.user.center.service.facade.WechatService;
import com.idear.user.center.wechat.form.BindMemberForm;
import com.idear.user.center.wechat.form.SendSmsForm;
import com.idear.user.center.wechat.message.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

/**
 * @author ShanCc
 * @date 2018/12/13
 */
@Controller
@RequestMapping(value = "bind")
public class BindController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private SmsService smsService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private WechatService wechatService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value(value = "${appId}")
    private String appId;

    @Value(value = "${appSecret}")
    private String appSecret;

    @Value(value = "${getOpenIdUrl}")
    private String getOpenIdUrl;

    @RequestMapping(value = "sms")
    @ResponseBody
    public String sendSms(@RequestBody @Valid final SendSmsForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String mobileNo = form.getMobileNo();
                sendValidSms(mobileNo);
                return Response.SUCCESS;
            }
        }, bindingResult);
    }

    @RequestMapping(value = "member")
    @ResponseBody
    public String bindMember(@RequestBody @Valid final BindMemberForm form, BindingResult bindingResult) {
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String mobileNo = form.getMobileNo();
                String validCode = form.getValidCode();
                String code = form.getCode();
                if (validateSmsCode(mobileNo,validCode)){
                    Response<MemberInfoDto> response = memberService.getMemberByMobileNo(mobileNo);
                    MemberInfoDto memberInfoDto = response.getRespBody();
                    if (memberInfoDto == null){
                        throw new BusinessException(ErrorMessage.MOBILE_NO_IS_REGISTRY);
                    }
                    Integer memberId = memberInfoDto.getId();
                    String openId = getOpenId(code);
                    Response bindResponse = wechatService.bindMember(memberId,openId);
                    return ResponseUtil.getSimpleResponse(bindResponse);
                }else {
                    throw new BusinessException(ErrorMessage.VALIDATE_SMS_VALID_CODE_FAILED);
                }
            }
        }, bindingResult);
    }

    private Boolean validateSmsCode(String mobileNo,String validCode){
        String value = redisTemplate.opsForValue().get(WECHAT_BIND_SMS_VALID_CODE + mobileNo);
        if (StringUtil.isEmpty(value)) {
            return Boolean.FALSE;
        }else {
            Boolean result =  value.equals(validCode);
            redisTemplate.delete(WECHAT_BIND_SMS_VALID_CODE + mobileNo);
            return result;
        }
    }

    /**
     * 网页授权获取用openId
     * @param code
     * @return
     */
    private String getOpenId(String code){
        String requestUrl = getOpenIdUrl + "&appid="+appId+"&secret="+appSecret+"&code="+code;
        logger.info("requestUrl is {}.", requestUrl);
        String response = HttpUtil.get(requestUrl, null);
        logger.info("response is {}.", response);
        JSONObject jsonObject = JsonUtil.parseJsonObject(response);
        String openid = jsonObject.getString("openid");
        if (StringUtil.isNotEmpty(openid)){
            return openid;
        }else {
            throw new BusinessException(ErrorMessage.BIND_WECHAT_FAILED);
        }
    }
    /**
     * 发送短信验证码
     * @param mobileNo
     */
    private void sendValidSms(String mobileNo) {
        Response<Boolean> response = memberService.checkMobileNoIsRegistry(mobileNo);
        if (response == null) {
            throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
        }
        if (response.getRespBody()) {
            throw new BusinessException(ErrorMessage.MOBILE_NO_IS_REGISTRY);
        } else {
            String value = redisTemplate.opsForValue().get(WECHAT_BIND_SMS_VALID_CODE + mobileNo);
            if (!StringUtil.isEmpty(value)) {
                throw new BusinessException(ErrorMessage.BIND_SMS_VALID_CODE_HAS_SEND);
            }
            String validCode = CommonUtil.getRandomNum(6);
            SendSingleMessageParam sendSingleMessageParam = new SendSingleMessageParam();
            sendSingleMessageParam.setMobileNo(mobileNo);
            sendSingleMessageParam.setTemplateCode(BIND_SMS_VALID_CODE_TEMPLATE_CODE);
            sendSingleMessageParam.setParams(new String[]{validCode});
            Response sendResponse = smsService.sendSingleMessage(sendSingleMessageParam);
            if (sendResponse == null) {
                throw new CommunicationException(BaseMessage.COMMUNICATION_EXCEPTION);
            }
            if (!sendResponse.success()) {
                throw new BusinessException(sendResponse.getRespCode(), sendResponse.getRespMsg());
            } else {
                redisTemplate.opsForValue().set(WECHAT_BIND_SMS_VALID_CODE + mobileNo, validCode, SMS_VALID_CODE_EXPIRED_TIME, TimeUnit.SECONDS);
            }
        }
    }

    private final static String WECHAT_BIND_SMS_VALID_CODE = "WECHAT:BIND_SMS_VALID_CODE";

    private static final String BIND_SMS_VALID_CODE_TEMPLATE_CODE = "0002";

    private static final Long SMS_VALID_CODE_EXPIRED_TIME = 600L;
}
