/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.controller;

import com.idear.common.util.CommonUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.ControllerTemplate;
import com.idear.user.center.wechat.form.ConfigForm;
import com.idear.user.center.wechat.job.AccessTokenManager;
import com.idear.user.center.wechat.util.CheckUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


/**
 * @author ShanCc
 * @date 2018/12/12
 */
@Controller
@RequestMapping(value = "common")
public class CommonController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ControllerTemplate controllerTemplate;

    @Autowired
    private AccessTokenManager accessTokenManager;

    @Value(value = "${appId}")
    private String appId;

    @Value(value = "${appSecret}")
    private String appSecret;

    @RequestMapping(value = "config", method = RequestMethod.POST)
    @ResponseBody
    public String config(@RequestBody @Valid final ConfigForm configForm, BindingResult bindingResult){
        return controllerTemplate.execute(new Action() {
            @Override
            public Response execute() {
                String ticket = accessTokenManager.getJsTicket();
                String url = configForm.getCurrentUrl();
                String nonceStr = CommonUtil.getRandomString(15);
                Long timestamp = System.currentTimeMillis();
                String signature = CheckUtil.getJsApiSign(nonceStr,ticket,timestamp,url);
                Map<String,Object> result = new HashMap<>(16);
                result.put("appId",appId);
                result.put("timestamp",timestamp);
                result.put("nonceStr",nonceStr);
                result.put("signature",signature);
                result.put("appSecret",appSecret);
                return new Response(result);
            }
        },bindingResult);
    }
}
