/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ShanCc
 * @date 2018/12/13
 */
public class BindMemberForm {

    @NotEmpty(message = "手机号不能为空")
    private String mobileNo;

    @NotEmpty(message = "请输入短信验证码")
    private String validCode;

    @NotEmpty(message = "请关注公众号")
    private String code;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getValidCode() {
        return validCode;
    }

    public void setValidCode(String validCode) {
        this.validCode = validCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
