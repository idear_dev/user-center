/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.message.handler.impl;

import com.alibaba.fastjson.JSONObject;
import com.idear.common.util.HttpUtil;
import com.idear.common.util.JsonUtil;
import com.idear.user.center.service.dto.WechatDto;
import com.idear.user.center.service.facade.WechatService;
import com.idear.user.center.wechat.job.AccessTokenManager;
import com.idear.user.center.wechat.message.handler.MessageHandler;
import com.idear.user.center.wechat.message.WechatMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author ShanCc
 * @date 2018/12/11
 */
@Component
public class EventHandler implements MessageHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AccessTokenManager accessTokenManager;

    @Autowired
    private WechatService wechatService;

    @Value(value = "${getUserInfoUrl}")
    private String getUserInfoUrl;

    @Override
    public void handle(WechatMessage message) {
        String openId = message.getContent().get("FromUserName");
        String event = message.getContent().get("Event");
        if (EVENT_TYPE_SUBSCRIBE.equals(event)){
            String requestUrl = getUserInfoUrl+"?access_token="+accessTokenManager.getToken()+"&openid="+openId+"&lang=zh_CN";
            logger.info("requestUrl is {}.",requestUrl);
            String response = HttpUtil.get(requestUrl,null);
            logger.info("response is {}.",response);
            WechatDto dto = convert2Dto(response);
            logger.info("dto is {}.",JsonUtil.toJsonString(dto));
            wechatService.subscribe(dto);
        }
    }

    private static final String EVENT_TYPE_SUBSCRIBE = "subscribe";

    /**
     * 转换成Dto
     * @param body
     * @return
     */
    private WechatDto convert2Dto(String body){
        JSONObject jsonObject = JsonUtil.parseJsonObject(body);
        WechatDto dto = new WechatDto();
        Integer sex = jsonObject.getInteger("sex");
        if (sex != null){
            dto.setSex(sex);
        }
        String openId = jsonObject.getString("openid");
        dto.setOpenid(openId);
        String nickname = jsonObject.getString("nickname");
        dto.setNickname(nickname);
        String country = jsonObject.getString("country");
        dto.setCountry(country);
        String province = jsonObject.getString("province");
        dto.setProvince(province);
        String city = jsonObject.getString("city");
        dto.setCity(city);
        String headImageUrl = jsonObject.getString("headimgurl");
        dto.setHeadImgUrl(headImageUrl);
        String language = jsonObject.getString("language");
        Date date = new Date();
        dto.setLanguage(language);
        dto.setSubscribeTime(date);
        dto.setCreateTime(date);
        dto.setUpdateTime(date);
        return dto;
    }
}
