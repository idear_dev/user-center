/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.message;

import com.idear.common.util.StringUtil;
import com.idear.user.center.wechat.util.XmlUtil;

import java.util.Map;

/**
 * @author ShanCc
 * @date 2018/12/11
 */
public class WechatMessage {

    private String messageType;

    private Map<String, String> content;

    public WechatMessage(String body) {
        if (StringUtil.isNotEmpty(body)) {
            Map<String, String> bodyMap = XmlUtil.xmlToMap(body);
            String msgType = bodyMap.get("MsgType");
            if (StringUtil.isNotEmpty(msgType)) {
                this.messageType = msgType;
                this.content = bodyMap;
            }
        }
    }

    public String getMessageType() {
        return messageType;
    }

    public Map<String, String> getContent() {
        return content;
    }

}
