/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.user.center.wechat.message.handler;

import com.idear.user.center.wechat.message.WechatMessage;

/**
 * @author ShanCc
 * @date 2018/12/11
 */
public interface MessageHandler {

    /**
     * 处理消息方法
     * @param message
     */
    void handle(WechatMessage message);
}
