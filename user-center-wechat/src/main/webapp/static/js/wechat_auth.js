

function getDomain() {
  var B = document.location.href;
  if ("https:" == document.location.protocol) {
    return B.substring(0, B.indexOf("/", 8))
  }
  return B.substring(0, B.indexOf("/", 7))
}

function getProgram() {
  var C = document.location.href;
  var B = getDomain();
  var pos = C.indexOf("/", B.length + 1);
  if (pos === -1) return '/';
  return C.substring(B.length, pos + 1);
}

function checkLogin(callback) {
  // if (config.basePath.indexOf('localhost') != -1 || config.basePath.indexOf('99.48') != -1) {
  //   if (callback) {
  //     callback((new Date()).getTime(), '');
  //   }
  //   return;
  // }
  if (config.debug) openVconsole();
  var loginCount = 0;
  if (localStorage) {
    localStorage.removeItem('loginCount');
    loginCount = localStorage.getItem("loginCount");
    if (typeof(loginCount) == "undefined") {
      loginCount = 0;
    }
  }
  if (loginCount > 3) {
    document.location.replace("error.html?errorCode=3004");
    return;
  }

  utils.callAjax({
    url: utils.getApiUrl('wechat-authorize'),
    data: {
      pageUrl: location.href,
      memberId: memberId
    },
    success: function(data) {
      var json = data;
      if (json.code == 0 || json.code == 3003) {
        if (null != json.accessToken) {
          config.accessToken = json.accessToken;
        }
        var content = utils.parseJSON(json.content);
        if (localStorage) {
          localStorage.setItem("webVer", content.version);
          localStorage.setItem("loginCount", 0);
        }
        if (localStorage) localStorage.setItem('wechatInfo', JSON.stringify(content));
        initWechatJs(content.appId, content.nonceStr, content.timestamp, content.signature);
        // 插入同盾js
        if (content.tdId && content.tdId != '') tongdunJs(content.tdId);
        if (callback) {
          callback(content.version, content.jsType);
        }
      } else if (json.code == 3005) {
        if (localStorage) {
          localStorage.setItem("loginCount", loginCount + 1);
        }
        var content = utils.parseJSON(json.content);
        document.location.replace(content.url);
      } else {
        if (localStorage) {
          localStorage.setItem("loginCount", loginCount + 1);
        }
        document.location.replace("error.html?errorCode=" + json.code);
      }
    },
    error: function() {
      document.location.replace("error.html?errorCode=1001");
    }
  });
}

function initWechatJs(appId, nonceStr, timestamp, signature) {
  wx.config({
    debug: false,
    appId: appId,
    timestamp: timestamp,
    nonceStr: nonceStr,
    signature: signature,
    jsApiList: [
      'checkJsApi',
      'chooseImage',
      'uploadImage',
      'scanQRCode',
      'getLocation',
      'closeWindow',
      'chooseWXPay',
      'scanQRCode'
    ]
  });
}

// function tongdunJs(token) {
//   _fmOpt = {
//     bd: true,
//     partner: TD_APPNAME,
//     appName: TD_APPNAME,
//     token: token
//   };
//   var cimg = new Image(1, 1);
//   cimg.onload = function() {
//     _fmOpt.imgLoaded = true;
//   };
//   cimg.src = 'https://fp.fraudmetrix.cn/fp/clear.png?partnerCode='+_fmOpt.partner+'&appName='+_fmOpt.appName+'&tokenId=' + _fmOpt.token;
//   var fp = document.createElement('script');
//   fp.type = 'text/javascript';
//   fp.async = true;
//   fp.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.fraudmetrix.cn/fm.js?ver=0.1';
//   var s = document.getElementsByTagName('script')[0];
//   s.parentNode.insertBefore(fp, s);
// }

function openVconsole () {
  var fp = document.createElement('script');
  fp.type = 'text/javascript';
  fp.async = true;
  fp.src = 'resources/js/eruda.min.js?t='+(new Date()).getTime();
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(fp, s);
}

var wx_authorize = 'https://open.weixin.qq.com/connect/oauth2/authorize?';
var wx_redirect_url = '/user-center-wechat/index.html';
var appId = '';
var url = '';

var login_config = {
    'appid': appId,
    'redirect_uri': encodeURIComponent(url + wx_redirect_url),
    'response_type': 'code',
    'scope': '',
    'state': 'STATE#wechat_redirect'
}

var goodsNo = '';

function wxOauth (type) {
    console.log("wxOauth-type:" + type);
    var scopeType = '';
    if (type == 'base') {
        scopeType = "snsapi_base";
        // login_config.redirect_uri = encodeURIComponent(url + '/user-center-wechat/index.html');
    } else {
        scopeType = "snsapi_userinfo";
    }
    window.location.href = wx_authorize + 'appid=' + login_config.appid + '&redirect_uri=' + login_config.redirect_uri + '&response_type=' + login_config.response_type + '&scope=' + scopeType + '&state=' + login_config.state;

}
