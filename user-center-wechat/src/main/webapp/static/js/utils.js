// var v = null;
// var memberId = -1;
// $(document).ready(function() {
//   v = getQueryString("v");
//   if (null == v && localStorage) {
//     v = localStorage.getItem("webVer");
//   }
//   if (null == v) {
//     v = format(new Date());
//   }
//   memberId = getQueryString("memberId");
// });

var h5WalletCUtils = h5WalletCUtils || (function($, window, document, undefined) {
  // var _host = 'https://sittest.memedai.cn';
  var _host = '';
  var _path = '/walletWechat';
  var _jumpUrl = _path + '/wcweb/merchantApply';
  var _prefix = _path + '/api';
  var _urls = {
    'wechat-authorize': '/wechat/authorize',
    'common-errorCode': '/common/errorCode'
  };


  function _apiUrl(key) {
    if (!_urls[key] || _urls[key] == '') return '';
    return _host + _prefix + _urls[key];
  }

  function _toggleLoading(status) {
    // status ? $('#page-loading').addClass('active') : $('#page-loading').removeClass('active');
    if (status) {
      $('#page-loading').find('.newloading-2').removeClass('ending').addClass('starting');
      $('#page-loading').addClass('active');
    } else {
      if (!$('#page-loading').hasClass('active')) return false;
      // setTimeout(function() {
      // 延迟保证前面的动画完成
      $('#page-loading').find('.newloading-2').removeClass('starting').addClass('ending');
      setTimeout(function() {
        $('#page-loading').removeClass('active');
      }, 1000);
      // }, 2000);
    }
  }

  function setHost(host) {
    if (!host || host == '') return this;
    _host = host;
    return this;
  }

  function setUrls(urls) {
    _urls = $.extend(_urls, urls);
    return this;
  }

  function setPrefix(prefix) {
    _prefix = prefix;
    return this;
  }

  function goUrl(options) {
    options = $.extend({
      step: 0
    }, options);
    var url = _host + _jumpUrl;
    var qsArr = [];
    for (var k in options) {
      qsArr.push(k + '=' + options[k]);
    }
    location.href = url + '?' + qsArr.join('&');
  }

  function parseJSON(str) {
    if (typeof str == 'object') {
      return str;
    }
    try {
      return JSON.parse(str);
    } catch (ex) {}
    try {
      return $.parseJSON(str);
    } catch (ex) {}
    return (new Function("", "return " + str))();
  }

  function clientPlatform() {
    var ua = navigator.userAgent;
    if (ua.indexOf("iPhone") > -1) {
      return "iphone";
    }
    if (ua.indexOf("iPad") > -1) {
      return "iPad";
    }
    if (ua.indexOf("iPod") > -1) {
      return "iPod";
    }
    if (ua.indexOf("Windows Phone") > -1) {
      return "WindowsPhone";
    }
    if (ua.indexOf("SymbianOS") > -1) {
      return "Symbian";
    }
    if (ua.indexOf("Linux") > -1) {
      if (ua.indexOf("Android") > -1) {
        return "Android";
      }
    }
    return "unknown";
  }





  return {
    goUrl: goUrl,
    setHost: setHost,
    setPrefix: setPrefix,
    setUrls: setUrls,
    parseJSON: parseJSON,
    getApiUrl: _apiUrl,
    showLoading: function() {
      _toggleLoading(true);
    },
    hideLoading: function() {
      _toggleLoading(false);
    }
  }

})(jQuery, window, document);

var h5WalletUI = h5WalletUI || (function($, window, document, undefined) {

  var _toptip = function(msg) {
    if (!$.toptip || typeof $.toptip != 'function') return false;
    if (!msg || msg === '') return false;
    var duration = arguments[1] || 3000;
    $.toptip(msg, duration, 'warning');
  };

  var _popup = function(target, options) {
    if ($('#popup-' + target).length > 0) $('#popup-' + target).remove();
    var current = 0,
      len = 0,
      title = '',
      pageData = {},
      winW = $(window).width();
    if ($.isArray(options)) {
      if (options.length <= 0) return false;
      len = options.length;
      title = options[0]['title'];
      subtitle = options[0]['subtitle'] || '';
      pageData = options[0];
    } else {
      title = options['title'];
      subtitle = options['subtitle'] || '';
      pageData = options;
    }
    var dataArray1 = [];
    var dataArray2 = [];
    var dataArray3 = [];
    var _popC_page = null;
    var selectedDate = [];

    jQuery.fn.extend({
      setColumnData: function (dataArray, idx) {
        var ul = this.find('.col'+ idx).find('ul');
        ul.html('');
        for (var k = 0; k < dataArray.length; k++) {
          var data = dataArray[k];
          ul.append($('<li><a href="javascript:;" data-col="' + idx + '" data-idx="' + k + '" data-value="' + data['val'] + '" data-name="' + data['name'] + '">' + data['name'] + '</a></li>'));
        }
      }
    });

    var leapYear = function(year) {
      return !(year % (year % 100 ? 4 : 400));
    }

    var getMonthDateList = function() {
      var mDate = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      var maxDate = 31;
      var y = 0, m = (new Date()).getMonth() + 1;
      var arr = [];
      if (arguments.length > 0) {
        if (arguments.length > 1) {
          y = arguments[0] || 0;
          m = arguments[1] || m;
        }
        maxDate = mDate[m - 1];
        if (m === 2 && y > 0 && leapYear(y)) maxDate = 29;
      }
      for (var d = 1; d <= maxDate; d++) {
        // if (d < 10) d = '0' + d;
        arr.push({ val: d, name: d + '日' });
      }
      return arr;
    }

    var appendPage = function(pageData) {
      var idx = arguments[1] || 0;
      if ($('#popup-' + target).find('.popup-page#page_' + idx).length > 0) {
        $('#popup-' + target).find('.popup-page#page_' + idx).remove();
      }

      _popC_page = $('<div class="popup-page"></div>');
      _popC_page
        .attr('id', 'page_' + idx)
        .attr('data-page', ~~(pageData['nextPage']))
        .css({
          'width': winW,
          'left': (idx * winW) + 'px'
        });

      if (pageData['column'] && pageData['column'] === 3) {
        // 三栏模式
        var _cols = $('<div class="cols"></div>')
        _cols.append('<div class="col col1"><ul></ul></div>');
        _cols.append('<div class="col col2"><ul></ul></div>');
        _cols.append('<div class="col col3"><ul></ul></div>');
        _popC_page.append(_cols);

        dataArray1 = pageData['data'];
        dataArray2 = dataArray1[0]['second'];
        dataArray3 = dataArray2[0]['third'];
        _popC_page.setColumnData(dataArray1, 1);
        _popC_page.setColumnData(dataArray2, 2);
        _popC_page.setColumnData(dataArray3, 3);
      } else if (pageData['type'] && pageData['type'] === 'date') {
        // 日期模式
        var _cols = $('<div class="cols"></div>')
        _cols.append('<div class="col col1"><ul></ul></div>');
        _cols.append('<div class="col col2"><ul></ul></div>');
        _cols.append('<div class="col col3"><ul></ul></div>');
        _popC_page.append(_cols);

        if (pageData['tip'] && pageData['tip'] !== '') _popC_page.append('<div class="date-tip">' + pageData['tip'] + '</div>');

        var date = new Date();
        var yyyy = pageData['startYear'] || date.getFullYear() - 10;
        var yRange = pageData['yearRange'] || 10;
        for (var y = yyyy; y < yyyy + yRange; y++) dataArray1.push({ val: y, name: y + '年' });
        for (var m = 1; m <= 12; m++) {
          // if (m < 10) m = '0' + m;
          dataArray2.push({ val: m, name: m + '月' });
        }
        dataArray3 = getMonthDateList();
        _popC_page.setColumnData(dataArray1, 1);
        _popC_page.setColumnData(dataArray2, 2);
        _popC_page.setColumnData(dataArray3, 3);

      } else {
        _popC_page.append($('<ul></ul>'));
        for (var k = 0; k < pageData['data'].length; k++) {
          var data = pageData['data'][k];
          var nameStr = '';
          if (typeof data['name'] == 'string') {
            nameStr = data['name'];
          } else {
            for (var j = 0; j < data['name'].length; j++) {
              nameStr += '<span>' + data['name'][j] + '</span>';
            }
          }
          _popC_page.find('ul').append($('<li><a href="javascript:;" data-value="' + data['value'] + '" data-name="' + data['name'] + '">' + nameStr + '</a></li>'));
        }
      }

      if (_popCContent) {
        var _pageC = _popCContent.find('.popup-page-wrap');
        _pageC.append(_popC_page);
        _pageC.width(_pageC.find('.popup-page').length * winW);
      }
    };

    // 生成弹出层
    var _popBox = $('<div id="popup-' + target + '" class="weui-popup-container popup-bottom"></div>');
    var _popOverlay = $('<div class="weui-popup-overlay"></div>');
    var _popContainer = $('<div class="weui-popup-modal popup-container"></div>');

    // title section
    var _popCTitle = $('<div class="popup-title"><a href="javascript:;" class="btn-back"><i class="icon icon-pageback"></i></a><a href="javascript:;" class="close-popup btn-close"><i class="icon icon-dismiss"></i></a><h1 id="popup-title">' + title + '</h1><p id="subtitle">' + subtitle + '</p></div>');

    // content section
    var _popCContent = $('<div class="popup-wrap"><div class="popup-content"><div class="popup-page-wrap"></div></div></div>');
    appendPage(pageData);
    _popContainer.append(_popCTitle).append(_popCContent);
    _popBox.append(_popOverlay).append(_popContainer);
    $('body').append(_popBox);

    var popup = $('#popup-' + target);
    var setBackBtn = function() {
      var btnBack = popup.find('.btn-back');
      (current > 0) ? btnBack.addClass('active'): btnBack.removeClass('active');
    };
    var setPageTitle = function() {
      title = options[current]['title'] || '';
      console.log(current);
      console.log(title+'2');
      (title != '') && popup.find('#popup-title').text(title);
    };
    var nextPage = function(data) {
      current += 1;
      // pageData = options[current];
      pageData = $.extend(options[current], data);
      appendPage(pageData, current);
      setTimeout(function() {
        _popCContent.find('.popup-page-wrap').css('left', -(current * winW));
        setPageTitle();
        setBackBtn();
      }, (1000 / 60) + 10);
    };

    popup.on('click', '.popup-page a', function(evt) {
      var $this = $(this);
      var val = $(this).data('value');
      var name = $(this).data('name');
      var callback = ($.isArray(options)) ? options[current]['callback'] : options['callback'];

      if (pageData['column'] && pageData['column'] === 3) {
        var dataset = $(this).data();
        var col = +dataset['col'];
        _popC_page.find('.col' + col).find('a').removeClass('selected');
        $(this).addClass('selected');
        if (col === 1) {
          dataArray2 = dataArray1[dataset['idx']]['second'];
          dataArray3 = dataArray2[0]['third'];
          _popC_page.setColumnData(dataArray2, 2);
          _popC_page.setColumnData(dataArray3, 3);
        }
        if (col === 2) {
          dataArray3 = dataArray2[dataset['idx']]['third'];
          _popC_page.setColumnData(dataArray3, 3);
        }
        if (col === 3) {
          if (typeof callback == 'function') callback.call(this, val, name);
          $.closePopup();
        }
      } else if (pageData['type'] && pageData['type'] === 'date') {
        var dataset = $(this).data();
        var col = +dataset['col'];
        _popC_page.find('.col' + col).find('a').removeClass('selected');
        $(this).addClass('selected');
        selectedDate[col - 1] = val;
        var setCallBack = function () {
          var dateVal = selectedDate.join('-');
          var dateStr = selectedDate[0] + '年' + selectedDate[1] + '月' + selectedDate[2] + '日';
          if (typeof callback == 'function') callback.call(this, dateVal, dateStr);
          $.closePopup();
        }
        if (selectedDate.length === 3) {
          var verifyFunc = pageData['verify'];
          if (!selectedDate[0] || selectedDate[0] === '' || !selectedDate[1] || selectedDate[1] === '') return false;
          if (typeof verifyFunc === 'function') {
            if (verifyFunc(selectedDate)) setCallBack();
          } else {
            setCallBack();
          }
        } else {
          var dateList = getMonthDateList(+selectedDate[0], +selectedDate[1]);
          _popC_page.setColumnData(dateList, 3);
        }
      } else {
        var pageBox = popup.find('.popup-page#page_' + current);
        var page = pageBox.data('page') || 0;
        if (page > 0) {
          // 这里通过两次callback来将外部的数据注入进来，调用内部callback
          if (typeof callback == 'function') callback.call(this, {
            name: name,
            value: val
          }, nextPage);
          else nextPage();
        } else {
          if (typeof callback == 'function') callback.call(this, val, name);
          $.closePopup();
          current = 0;
        }
      }
    }).on('click', '.btn-back', function(evt) {
      current -= 1;
      _popCContent.find('.popup-page-wrap').css('left', -(current * winW));
      setPageTitle();
      setBackBtn();
    });

    $(window).on('resize', function() {
      // $.closePopup();
      setTimeout(function() {
        winW = $(window).width();
        var _pageP = popup.find('.popup-page');
        popup.find('.popup-page').width(winW);
        popup.find('.popup-page-wrap').width(_pageP.length * winW);
      }, 1000 / 60);
    });

    popup.popup();
  };

  var _alert = function(msg) {
    if (!msg || msg == '') return false;
    var title = arguments[1] || '提示';
    $.alert(msg, title);
  };

  var _confirm = function(options) {
    options = $.extend({
      title: '提示',
      btnCancel: '取消',
      btnOk: '确认'
    }, options);
    if (!options['text'] || options['text'] == '') return false;
    $.modal(options);
  };

  return {
    toptip: _toptip,
    popup: _popup,
    alert: _alert,
    confirm: _confirm
  };

})(jQuery, window, document);

var utils = utils || h5WalletCUtils;
var ui = ui || h5WalletUI;

function format(time) {
  var o = {
    "M+": time.getMonth() + 1,
    "d+": time.getDate(),
    "H+": time.getHours(),
    "m+": time.getMinutes(),
    "s+": time.getSeconds()
  };
  var fmt = "yyyyMMddHHmm";
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
  }
  return fmt;
}

function getQueryString(name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) {
    return decodeURIComponent(r[2]);
  }
  return null;
}

function loadCssRes(F) {
  var E = document.getElementsByTagName("head")[0];
  var D = document.createElement("link");
  D.href = F;
  D.rel = "stylesheet";
  D.type = "text/css";
  E.appendChild(D);
}

function loadScriptRes(url, callback) {
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.async = true;
  script.charset = "utf-8";
  script.src = url;
  if (script.readyState) {
    script.onreadystatechange = function() {
      if (script.readyState == "loaded" || script.readyState == "complete") {
        script.onreadystatechange = null;
        if (callback) {
          callback();
        }
      }
    };
  } else {
    script.onload = function() {
      if (callback) {
        callback();
      }
    };
  }
  head.appendChild(script);
}

function isExitsFunction(funcName) {
  try {
    if (typeof(eval(funcName)) == "function") {
      return true;
    }
  } catch (e) {}
  return false
}

function isExitsVariable(D) {
  try {
    if (typeof(D) == "undefined") {
      return false;
    } else {
      return true;
    }
  } catch (C) {}
  return false;
};

var _utils = {
    sending : false,
    showTips : function(data){
        console.log(data);
        var $tipsWrap = $("<div class='tipsWrap'>" + data + "</div>");
        $("body").append($tipsWrap);
        setTimeout(function(){
            $tipsWrap.remove();
        },2000);
    },
    showOldTips : function(data){
        ui.toptip(data);
        setTimeout(function(){
            $(".weui_toptips").remove();
        },9999900);
    },
    formatMoney:function(cent,withPoint){
        if(cent == null) return cent;
        var cent = (cent + "").replace(/\D/g,"");
        var _length = cent.length;
        if(_length == 2){
            cent = "0." + cent;
        }else if(_length == 1){
            cent = "0.0" + cent;
        }else if(_length == 0){
            cent = "0.00"
        }
        else{
            intCent = cent.substr(0,_length-2);
            pointCent = cent.substr(_length-2,2);
            if(withPoint){
                cent = intCent.split('').reverse().join('').replace(/(\d{3}(?=\d)(?!\d+\.|$))/g, '$1,').split('').reverse().join('') + "." + pointCent;
            }else{
                cent = intCent.split('').reverse().join('').replace(/(\d{3}(?=\d)(?!\d+\.|$))/g, '$1,').split('').reverse().join('');
            }
        }
        return cent;
    },
    fgetQueryString:function(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }
}

//ajax
var debug = false;
var host = "http://106.12.35.98/user-center-wechat";
function ajaxRun(url,method,async,contentType,params,callback){
    params.timestamp = new Date().getTime();
    $.ajax({
        url: url,
        type: method,
        async: async,
        contentType: contentType?contentType:"application/json;charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        // beforeSend: function (XMLHttpRequest) {
        //     XMLHttpRequest.setRequestHeader("token", $.cookie('token'));
        // },
        success: function (res) {
            if(debug) {
                console.log('ajax res',res);
            }
            if(callback && typeof callback == 'function') callback(res);
        },
        complete: function(res){
            if(debug) {
                console.log('ajax complete', res);
                console.log('ajax url', url);
                console.log('ajax data', params);
                console.log('ajax async', async);
            }
        },
        error: function (res) {
            if(debug) {
                console.log('ajax-error',res);
            }else {
                alert("系统异常，请稍后重试");
            }
        }
    })

}

/*金额转换*/
function currency(amount, fixed) {
    /* 千分位方法，支持小数*/
    var toThousands = function (num) {
        num = (num || 0).toString()
        var arr = []
        var result = ''
        if (num.indexOf('.') != -1) {
            arr = num.split('.')
            num = arr[0]
        }
        while (num.length > 3) {
            result = ',' + num.slice(-3) + result
            num = num.slice(0, num.length - 3)
        }
        if (num) {
            result = num + result
        }
        if (arr.length > 0) result += '.' + arr[1]
        return result
    }

    if (~~(amount) === 0) return '0.00'
    fixed = fixed || 0
    var tmpNumber = amount
    if (fixed == 1) tmpNumber = (tmpNumber / 100).toFixed(2)
    else tmpNumber = parseInt(tmpNumber / 100, 10)
    if (tmpNumber >= 1000) return toThousands(tmpNumber)
    else return tmpNumber
}

function checkMobile(mobile) {
    if (mobile.length === 0) {
        // _utils.showOldTips("请输入手机号");
        return false;
        // } else if (/^(?:13\d|15\d|18\d|147|173|176|177|178)\d{5}(\d{3}|\*{3})$/.test(mobile)) {
    } else if (/^1\d{10}$/.test(mobile)) {
        return true;
    } else {
        // _utils.showOldTips("手机号有误，请重新输入");
        return false;
    }
}

function checkVerifyCode(verifyCode) {
    if (verifyCode.length == 4) {
        return true;
    } else {
        return false;
    }
}

var phoneNumberReg = /^1[345678]\d{9}$/,
    isNum = /^\d$/;