# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.40)
# Database: rumiker
# Generation Time: 2018-10-11 11:12:05 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table app_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_info`;

CREATE TABLE `app_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `app_name` varchar(40) NOT NULL DEFAULT '' COMMENT 'App名称',
  `app_description` varchar(100) NOT NULL DEFAULT '' COMMENT 'App描述信息',
  `app_logo_url` varchar(250) NOT NULL DEFAULT '' COMMENT 'AppLog',
  `company` varchar(11) DEFAULT '' COMMENT '公司名称',
  `phone_number` varchar(11) DEFAULT '' COMMENT '联系人电话',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_app_name` (`app_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='App应用信息';

LOCK TABLES `app_info` WRITE;
/*!40000 ALTER TABLE `app_info` DISABLE KEYS */;

INSERT INTO `app_info` (`id`, `app_name`, `app_description`, `app_logo_url`, `company`, `phone_number`, `create_time`, `update_time`)
VALUES
	(1,'么么钱包','专门为女性解决金融问题','http://memedai.com','米么金服','12345678911','2018-09-14 15:34:30','2018-09-14 15:35:28'),
	(2,'掌上生活','招商银行信用卡','http://cmbcc.com','招商银行','23456789111','2018-09-14 15:35:32','2018-09-14 15:36:17');

/*!40000 ALTER TABLE `app_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_market
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_market`;

CREATE TABLE `app_market` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '市场名称',
  `logo_url` varchar(250) NOT NULL DEFAULT '' COMMENT '应用市场logo',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用市场信息';

LOCK TABLES `app_market` WRITE;
/*!40000 ALTER TABLE `app_market` DISABLE KEYS */;

INSERT INTO `app_market` (`id`, `name`, `logo_url`, `create_time`, `update_time`)
VALUES
	(1,'360市场','http://360','2018-09-14 14:38:27','2018-09-14 14:39:32'),
	(2,'百度市场','http://baidu','2018-09-14 15:33:13','2018-09-14 15:34:09'),
	(3,'华为市场','http://huawei','2018-09-14 15:34:03','2018-09-14 15:34:18');

/*!40000 ALTER TABLE `app_market` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table banner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banner`;

CREATE TABLE `banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` tinyint(4) NOT NULL,
  `location` tinyint(4) NOT NULL,
  `link_url` varchar(250) DEFAULT NULL,
  `image_url` varchar(250) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `sort` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='banner广告';

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;

INSERT INTO `banner` (`id`, `category`, `location`, `link_url`, `image_url`, `title`, `sort`, `status`, `create_time`, `update_time`)
VALUES
	(1,1,1,'www.baidu.com','www.baidu.com','测试百度',3,1,'2019-01-01 00:00:00','2019-01-01 00:00:00'),
	(2,1,1,'www.aliyun.com','www.aliyun.com','测试阿里',1,1,'2019-01-01 00:00:00','2019-01-01 00:00:00');

/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table client_version_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `client_version_config`;

CREATE TABLE `client_version_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL COMMENT '版本号',
  `download_url` varchar(250) NOT NULL DEFAULT '' COMMENT '下载地址',
  `description` varchar(1000) NOT NULL DEFAULT '' COMMENT '描述信息',
  `is_force_update` tinyint(4) NOT NULL COMMENT '是否强制升级',
  `channel` varchar(10) NOT NULL DEFAULT '' COMMENT 'IOS/ANDRPOID',
  `status` tinyint(4) NOT NULL COMMENT '状态',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端版本信息';

LOCK TABLES `client_version_config` WRITE;
/*!40000 ALTER TABLE `client_version_config` DISABLE KEYS */;

INSERT INTO `client_version_config` (`id`, `version`, `download_url`, `description`, `is_force_update`, `channel`, `status`, `create_time`, `update_time`)
VALUES
	(1,100,'https://www.baidu.com','测试100',0,'IOS',1,'2018-08-19 10:15:55','2018-08-19 10:18:12'),
	(2,101,'https://www.baidu.com','测试101',0,'IOS',1,'2018-08-19 10:17:12','2018-08-19 10:18:13'),
	(3,100,'https://www.baidu.com','测试100',0,'ANDROID',1,'2018-08-19 10:17:49','2018-08-19 10:18:11'),
	(4,101,'https://www.baidu.com','测试101',1,'ANDROID',1,'2018-08-19 10:18:36','2018-08-19 10:18:51');

/*!40000 ALTER TABLE `client_version_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table manager
# ------------------------------------------------------------

DROP TABLE IF EXISTS `manager`;

CREATE TABLE `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `email` varchar(30) NOT NULL DEFAULT '' COMMENT '邮箱',
  `name` varchar(40) NOT NULL COMMENT '用户名称',
  `mobile_no` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `role_ids` text,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户状态 0：关闭1：开启',
  `password` varchar(100) NOT NULL DEFAULT '1' COMMENT '用户密码',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '账号创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_manager_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;

INSERT INTO `manager` (`id`, `email`, `name`, `mobile_no`, `role_ids`, `status`, `password`, `del_flag`, `create_time`, `update_time`)
VALUES
	(1,'chenchen.shan@mi-me.com','chenchen.shan@mi-me.com','','1',1,'96e79218965eb72c92a549dd5a330112',0,'2017-09-28 11:05:03','2018-09-26 10:38:50');

/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table member_task_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `member_task_record`;

CREATE TABLE `member_task_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `app_info` text NOT NULL,
  `plan_id` int(11) NOT NULL COMMENT '计划Id',
  `plan_info` text NOT NULL,
  `task_id` int(11) NOT NULL COMMENT '任务Id',
  `task_info` text NOT NULL,
  `audit_info` text,
  `status` tinyint(4) NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户任务记录';

LOCK TABLES `member_task_record` WRITE;
/*!40000 ALTER TABLE `member_task_record` DISABLE KEYS */;

INSERT INTO `member_task_record` (`id`, `member_id`, `app_id`, `app_info`, `plan_id`, `plan_info`, `task_id`, `task_info`, `audit_info`, `status`, `create_time`, `update_time`)
VALUES
	(4,1,1,'{\"appDescription\":\"专门为女性解决金融问题\",\"appLogoUrl\":\"http://memedai.com\",\"appName\":\"么么钱包\",\"company\":\"米么金服\",\"createTime\":\"2018-09-14 15:34:30\",\"id\":1,\"phoneNumber\":\"12345678911\",\"updateTime\":\"2018-09-14 15:35:28\"}',1,'{\"appId\":1,\"createTime\":\"2018-09-14 15:40:46\",\"endTime\":\"2018-09-20 15:40:46\",\"id\":1,\"label\":\"[\\\"新户\\\",\\\"简单\\\",\\\"钱多\\\"]\",\"name\":\"掌上生活下载体验\",\"startTime\":\"2018-09-14 15:40:46\",\"status\":0,\"totalNum\":0,\"tutorial\":\"[{\\\"name\\\":\\\"第一步\\\",\\\"textExplain\\\":\\\"打开APP\\\",\\\"imageExplain\\\":\\\"http://www.baidu.com/image\\\"},{\\\"name\\\":\\\"第二步\\\",\\\"textExplain\\\":\\\"查找APP\\\",\\\"imageExplain\\\":\\\"http://www.baidu.com/image\\\"},{\\\"name\\\":\\\"第三步\\\",\\\"textExplain\\\":\\\"关闭APP\\\",\\\"imageExplain\\\":\\\"http://www.baidu.com/image\\\"}]\",\"type\":1,\"updateTime\":\"2018-09-15 16:33:41\"}',1,'{\"amount\":100,\"channel\":\"IOS\",\"createTime\":\"2018-09-14 15:52:46\",\"endTime\":\"2018-09-20 15:52:46\",\"id\":1,\"planId\":1,\"receivedNum\":6,\"showTime\":\"2018-09-14 15:52:46\",\"startTime\":\"2018-09-14 15:52:46\",\"status\":1,\"totalNum\":1000,\"updateTime\":\"2018-09-18 18:06:30\"}',NULL,1,'2018-09-19 18:36:09','2018-09-19 18:36:09');

/*!40000 ALTER TABLE `member_task_record` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父节点ID',
  `name` varchar(40) NOT NULL COMMENT '菜单名称',
  `href` varchar(200) NOT NULL COMMENT '资源指向',
  `resource_ids` text COMMENT '菜单下的资源集',
  `sort` int(11) NOT NULL COMMENT '资源排序序号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近更新时间',
  `del_flag` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志0-未删除1-已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_menu_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;

INSERT INTO `menu` (`id`, `pid`, `name`, `href`, `resource_ids`, `sort`, `create_time`, `update_time`, `del_flag`)
VALUES
	(1,0,'系统配置','system-seeting','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28',0,'2017-09-28 10:03:31','2017-11-21 15:33:29',b'0');

/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;

INSERT INTO `question` (`id`, `question`, `answer`, `status`, `create_time`, `update_time`)
VALUES
	(1,'问题:如何提现','想怎么提就怎么提',1,'2018-09-19 22:11:49','2018-09-19 22:12:21'),
	(2,'问题:如何退款','想怎么退就怎么退',1,'2018-09-19 22:12:01','2018-09-19 22:12:24');

/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `resource`;

CREATE TABLE `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(40) NOT NULL COMMENT '资源名称',
  `url_pattern` varchar(200) NOT NULL COMMENT '资源对应的接口URL',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '资源创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近更新时间',
  `del_flag` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志0-未删除1-已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_resource_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源表';

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;

INSERT INTO `resource` (`id`, `name`, `url_pattern`, `create_time`, `update_time`, `del_flag`)
VALUES
	(1,'创建分组信息','/role/group/create','2017-09-28 09:24:32','2017-09-28 09:24:36',b'0'),
	(2,'创建用户信息','/user/create','2017-09-28 09:24:34','2017-09-28 09:24:45',b'0'),
	(3,'删除分组','/role/group/delete','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(4,'新建角色','/role/create','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(5,'查询用户列表','/user/list','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(6,'编辑分组信息','/role/group/edit','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(7,'编辑用户信息','/user/edit','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(8,'编辑角色信息','/role/edit','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(9,'获取分组列表','/role/group/list','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(10,'获取用户信息','/user/get','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(11,'获取菜单列表','/role/menu/list','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(12,'获取角色列表','/role/list','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(13,'查看角色信息','/role/get','2017-09-28 09:24:58','2017-09-28 09:24:58',b'0'),
	(14,'用户登出','/user/logout','2017-10-13 13:55:55','2017-10-13 13:55:57',b'0'),
	(15,'上传图片','/file/image/upload','2017-11-17 11:38:49','2017-11-17 11:38:51',b'0'),
	(16,'新建菜单','/menu/create','2017-11-21 15:33:28','2017-11-21 15:33:28',b'0'),
	(17,'编辑菜单','/menu/edit','2017-11-21 15:33:28','2017-11-21 15:37:18',b'0'),
	(18,'删除菜单','/menu/delete','2017-11-21 15:33:28','2017-11-21 15:33:28',b'0'),
	(19,'新建资源','/resource/create','2017-11-21 15:33:28','2017-11-21 15:33:28',b'0'),
	(20,'编辑资源','/resource/edit','2017-11-21 15:33:28','2017-11-21 15:33:28',b'0'),
	(21,'删除资源','/resource/delete','2017-11-21 15:33:29','2017-11-21 15:33:29',b'0'),
	(22,'资源列表','/resource/list','2017-11-21 15:33:29','2017-11-21 15:33:29',b'0'),
	(23,'资源所有列表','/resource/all/list','2017-11-21 15:33:29','2017-11-21 15:33:29',b'0'),
	(24,'删除用户信息','/user/delete','2017-11-21 15:33:29','2017-11-21 15:33:29',b'0'),
	(25,'关闭用户','/user/close','2018-01-30 15:09:22','2018-01-30 15:09:22',b'0'),
	(26,'开启用户','/user/open','2018-01-30 15:09:22','2018-01-30 15:09:22',b'0'),
	(27,'获取所有角色列表','/role/all/list','2018-02-01 09:48:27','2018-02-01 09:48:27',b'0'),
	(28,'获取Banner位置列表','/banner/location/list','2018-10-11 18:08:01','2018-10-11 18:08:13',b'0');

/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `group_id` int(11) NOT NULL COMMENT '角色组ID',
  `name` varchar(40) NOT NULL COMMENT '角色名称',
  `menu_ids` text COMMENT '角色对应的菜单权限集',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '最近更新时间',
  `del_flag` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志0-未删除1-已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;

INSERT INTO `role` (`id`, `group_id`, `name`, `menu_ids`, `create_time`, `update_time`, `del_flag`)
VALUES
	(1,1,'管理员','1','2017-09-28 11:13:19','2017-09-28 11:13:22',b'0');

/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_group`;

CREATE TABLE `role_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(40) NOT NULL COMMENT '组名称',
  `code` varchar(40) NOT NULL COMMENT '组编码',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态1-正常0-关闭',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '最近更新时间',
  `del_flag` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志0-未删除1-已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_group_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色组表';

LOCK TABLES `role_group` WRITE;
/*!40000 ALTER TABLE `role_group` DISABLE KEYS */;

INSERT INTO `role_group` (`id`, `name`, `code`, `status`, `create_time`, `update_time`, `del_flag`)
VALUES
	(1,'管理员','1',1,'2017-09-29 11:35:08','2017-09-29 12:29:00',b'0');

/*!40000 ALTER TABLE `role_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sms_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_history`;

CREATE TABLE `sms_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(32) NOT NULL DEFAULT '' COMMENT '流水号',
  `mobile_no` varchar(11) NOT NULL DEFAULT '' COMMENT '发送手机号',
  `template_code` varchar(4) NOT NULL DEFAULT '' COMMENT '模板编号',
  `content` text NOT NULL COMMENT '消息内容',
  `thd_serial_no` varchar(50) DEFAULT NULL COMMENT '流水号',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `in_mobile_no` (`mobile_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送记录';



# Dump of table sms_template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_template`;

CREATE TABLE `sms_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(4) NOT NULL DEFAULT '' COMMENT '短信模版编码',
  `content` text NOT NULL COMMENT '短信模板',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sms_template` WRITE;
/*!40000 ALTER TABLE `sms_template` DISABLE KEYS */;

INSERT INTO `sms_template` (`id`, `code`, `content`, `status`, `create_time`, `update_time`)
VALUES
	(1,'test','同事您好，感谢您对此次测试的配合。{0}',1,'2018-01-29 20:21:30','2018-01-29 20:21:31');

/*!40000 ALTER TABLE `sms_template` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table task_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task_detail`;

CREATE TABLE `task_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL COMMENT '计划Id',
  `channel` varchar(10) NOT NULL DEFAULT '',
  `amount` int(11) NOT NULL COMMENT '奖励单位分',
  `total_num` int(11) NOT NULL COMMENT '总数量',
  `received_num` int(11) NOT NULL COMMENT '已领取数量',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `show_time` datetime NOT NULL COMMENT '露出时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `status` tinyint(4) NOT NULL COMMENT '子任务状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `task_detail` WRITE;
/*!40000 ALTER TABLE `task_detail` DISABLE KEYS */;

INSERT INTO `task_detail` (`id`, `plan_id`, `channel`, `amount`, `total_num`, `received_num`, `start_time`, `show_time`, `end_time`, `status`, `create_time`, `update_time`)
VALUES
	(1,1,'ANDROID',100,1000,7,'2018-09-14 15:52:46','2018-09-14 15:52:46','2018-11-20 15:52:46',1,'2018-09-14 15:52:46','2018-10-11 18:16:37');

/*!40000 ALTER TABLE `task_detail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table task_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task_plan`;

CREATE TABLE `task_plan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '任务名称',
  `description` varchar(500) DEFAULT '' COMMENT '任务详细说明',
  `tutorial` varchar(500) DEFAULT NULL COMMENT '任务教程',
  `label` varchar(50) DEFAULT '' COMMENT '标签',
  `type` tinyint(4) NOT NULL COMMENT '任务类型',
  `type_info` varchar(100) DEFAULT NULL COMMENT '任务类型信息',
  `start_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `end_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '结束时间',
  `total_num` int(11) NOT NULL DEFAULT '0' COMMENT '总数量',
  `status` tinyint(4) NOT NULL COMMENT '计划状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `task_plan` WRITE;
/*!40000 ALTER TABLE `task_plan` DISABLE KEYS */;

INSERT INTO `task_plan` (`id`, `app_id`, `name`, `description`, `tutorial`, `label`, `type`, `type_info`, `start_time`, `end_time`, `total_num`, `status`, `create_time`, `update_time`)
VALUES
	(1,1,'掌上生活下载体验','掌上生活下载体验然后去使用，截图说明可以得到2元','[{\"name\":\"第一步\",\"textExplain\":\"打开APP\",\"imageExplain\":\"http://www.baidu.com/image\",\"needCapture\":false},{\"name\":\"第二步\",\"textExplain\":\"查找APP\",\"imageExplain\":\"http://www.baidu.com/image\",\"needCapture\":true},{\"name\":\"第三步\",\"textExplain\":\"关闭APP\",\"imageExplain\":\"http://www.baidu.com/image\",\"needCapture\":true}]','[\"新户\",\"简单\",\"钱多\"]',1,NULL,'2018-09-14 15:40:46','2018-11-20 15:40:46',1000,1,'2018-09-14 15:40:46','2018-10-11 19:08:51');

/*!40000 ALTER TABLE `task_plan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_center_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_center_account`;

CREATE TABLE `user_center_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '账户Id',
  `member_id` int(11) NOT NULL COMMENT '会员编号',
  `status` tinyint(2) NOT NULL COMMENT '账户状态  1正常 2冻结 9销户',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_center_account_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_center_account_record`;

CREATE TABLE `user_center_account_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '交易Id',
  `account_id` int(11) NOT NULL COMMENT '账户Id',
  `member_id` int(11) NOT NULL COMMENT '会员Id',
  `serial_no` varchar(45) NOT NULL COMMENT '流水号',
  `record_type` tinyint(2) NOT NULL COMMENT '类型 1返佣 2提现',
  `amount` int(11) NOT NULL COMMENT '金额（分）',
  `remark` text COMMENT '备注',
  `in_out_flag` bit(1) NOT NULL COMMENT '出入账标记：1入账9出账',
  `record_time` datetime NOT NULL COMMENT '记录时间',
  `status` tinyint(2) NOT NULL COMMENT ' 记录状态：1入账中 2出账中 3交易成功 4交易失败',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `IX_serial_no` (`serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_center_account_withdraw_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_center_account_withdraw_record`;

CREATE TABLE `user_center_account_withdraw_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `serial_no` varchar(45) NOT NULL DEFAULT '' COMMENT '申请流水号',
  `mobile_no` varchar(11) NOT NULL COMMENT '手机号码',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户姓名',
  `amount` int(11) NOT NULL COMMENT '提现金额',
  `withdraw_type` tinyint(2) NOT NULL COMMENT '提现方式 1微信',
  `account_info` text NOT NULL COMMENT '账户信息',
  `remark` text COMMENT '复核结果',
  `status` tinyint(2) NOT NULL COMMENT '提现状态信息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_center_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_center_member`;

CREATE TABLE `user_center_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_no` varchar(20) NOT NULL DEFAULT '' COMMENT '会员编号',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别',
  `mobile_no` varchar(11) NOT NULL DEFAULT '' COMMENT '注册手机号',
  `nickname` varchar(16) DEFAULT NULL COMMENT '昵称',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `head_img_url` text COMMENT '头像地址',
  `login_password` varchar(32) DEFAULT NULL COMMENT '用户登录密码',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_center_member` WRITE;
/*!40000 ALTER TABLE `user_center_member` DISABLE KEYS */;

INSERT INTO `user_center_member` (`id`, `member_no`, `sex`, `mobile_no`, `nickname`, `birthday`, `email`, `head_img_url`, `login_password`, `status`, `create_time`, `update_time`)
VALUES
	(1,'5864302169',NULL,'13002169280',NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',1,'2018-07-29 11:41:55','2018-07-29 11:41:55');

/*!40000 ALTER TABLE `user_center_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_center_wechat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_center_wechat`;

CREATE TABLE `user_center_wechat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL COMMENT '会员Id',
  `openid` varchar(40) NOT NULL DEFAULT '' COMMENT 'openid',
  `nickname` varchar(40) NOT NULL DEFAULT '' COMMENT '昵称',
  `sex` tinyint(1) NOT NULL COMMENT '性别1男性2女性0未知',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  `country` varchar(20) DEFAULT NULL COMMENT '国家',
  `province` varchar(20) DEFAULT NULL COMMENT '省份',
  `language` varchar(10) DEFAULT NULL COMMENT '语言',
  `head_img_url` text COMMENT '头像地址',
  `subscribe_time` datetime DEFAULT NULL COMMENT '关注时间',
  `union_id` varchar(40) DEFAULT '',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
